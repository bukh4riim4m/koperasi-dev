<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendidikan extends Model
{
  protected $fillable = [
      'id','name','aktif','admin','created_at','updated_at'
  ];
}
