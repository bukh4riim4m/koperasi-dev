<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
  protected $fillable = [
      'id','name','aktif','created_at','updated_at'
  ];
}
