<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSimpanan extends Model
{
  protected $fillable = [
      'id','name','aktif','admin','nominal','created_at','updated_at'
  ];
}
