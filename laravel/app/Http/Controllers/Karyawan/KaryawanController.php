<?php

namespace App\Http\Controllers\Karyawan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Akumulasi;
use Excel;
use DB;
use Log;

class KaryawanController extends Controller
{
    public function __construct()
    {
        $this->middleware('karyawan');
    }
    public function homekaryawan()
    {
        $dashboard ="dashboard";
        return view('karyawan.index', compact('dashboard'));
    }
    public function profil(Request $request)
    {
        $dashboard ="profil";
        if ($request->fotodiri) {
            $this->validate($request, [
            'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
            $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
        }

        if ($request->tgl_lahir =="") {
            $tanggal_lahir = "";
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }
        if ($request->action =='edit') {
            DB::beginTransaction();
            try {
                $user = User::find($request->user()->id);
                $user->nik=$request->nik;
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                if ($request->fotodiri) {
                    if ($user->fotodiri =='null' || $user->fotodiri =='') {
                        $user->fotodiri=$diri;
                        $destination_foto =public_path('foto');
                        $request->fotodiri->move($destination_foto, $diri);
                    } else {
                        $destination_foto =public_path('foto/'.$user->fotodiri);
                        if (file_exists($destination_foto)) {
                            unlink($destination_foto);
                        }
                        $user->fotodiri=$diri;
                        $destination_foto =public_path('foto');
                        $request->fotodiri->move($destination_foto, $diri);
                    }
                }
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Profil:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Profil.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data Profil berhasil di Edit.', 'INFO');
            return redirect()->back();
        }
        return view('karyawan.profil', compact('dashboard'));
    }
    public function anggota(Request $request)
    {
        $dashboard ="dataAnggota";
        $no_anggota = $request->no_anggota;
        $sequence = substr($no_anggota, -6);
        $nama = $request->name;
        if ($request->tgl_lahir =="") {
            $tanggal_lahir = date('1900-01-01');
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }
        if ($request->action == 'tambah') {
            DB::beginTransaction();
            // return $request->all();
            try {
                $user = User::create([
                'tgl_daftar'=>date('Y-m-d'),
                'name'=>$request->name,
                'tpt_lahir'=>$request->tmp_lahir,
                'tgl_lahir'=>$tanggal_lahir,
                'nik'=>$request->nik,
                'no_anggota'=>$no_anggota,
                'sequence'=>$sequence,
                'npwp'=>$request->npwp,
                'email'=>$request->email,
                'agama'=>$request->agama,
                'pendidikan'=>$request->pendidikan,
                'statuskeluarga'=>$request->statuskeluarga,
                'nama_ibu'=>$request->nama_ibu,
                'nama_ayah'=>$request->nama_ayah,
                'telp'=>$request->telp,
                'jenkel'=>$request->jenkel,
                'alamat'=>$request->alamat,
                'kelurahan'=>$request->kelurahan,
                'kecamatan'=>$request->kecamatan,
                'kabupaten'=>$request->kabupaten,
                'komunitas'=>$request->komunitas,
                'propinsi'=>$request->propinsi,
                'pendapatan'=>$request->pendapatan,
                'pekerjaan'=>$request->pekerjaan,
                'password'=>Hash::make($sequence),
                'type'=>'anggota',
                'fotoktp'=>null,
                'fotodiri'=>null,
                'admin'=>$request->user()->id,
                'aktif'=>1,
                'saldo'=>0
              ]);
            } catch (\Exception $e) {
                Log::info('Gagal input Anggota:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Data gagal di tambahkan.', 'INFO');
                $user = User::where('aktif', 1)->where('type', '<>', 'karyawan')->paginate(50);
                return view('karyawan.dataAnggota', compact('dashboard', 'user', 'nama', 'no_anggota'));
            }
            DB::commit();
            flash()->overlay('Data berhasil di Tambahkan.', 'INFO');
            $user = User::where('aktif', 1)->where('type', '<>', 'karyawan')->paginate(50);
            return view('karyawan.dataAnggota', compact('dashboard', 'user', 'nama', 'no_anggota'));
        } elseif ($request->action == 'edit') {
            DB::beginTransaction();
            try {
                $user = User::find($request->ids);
                $user->name = $request->name;
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->nik=$request->nik;
                $user->no_ks212 = $request->no_ks212;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Data Anggota:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Data Anggota.', 'INFO');
                return redirect()->route('data-anggota');
            }
            DB::commit();
            flash()->overlay('Data Anggota berhasil di Edit.', 'INFO');
            return redirect()->route('data-anggota');
        }
        // $users = User::where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('name', 'LIKE', '%'.$nama.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->orderBy('no_anggota', 'ASC')->get();
        $user = User::where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('name', 'LIKE', '%'.$nama.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->orderBy('no_anggota', 'ASC')->paginate(50);
        return view('karyawan.dataAnggota', compact('dashboard', 'user', 'nama', 'no_anggota', 'users'));
    }
    public function password()
    {
        $dashboard ="gantiPassword";
        return view('karyawan.gantiPassword', compact('dashboard'));
    }
    public function gantipassword(Request $request)
    {
        // Log::info(' ALERt :'.$request);

        if (\Hash::check($request->input('password_lm'), $request->user()->password)) {
            $user = User::find($request->user()->id);
            $user->password = bcrypt($request->input('password_br'));
            $user->update();
            flash()->overlay('Password Berhasil di rubah.', 'INFO');
            return redirect()->back();
        } else {
            flash()->overlay('Password Gagal di rubah<br>Password Lama Salah', 'INFO');
            return redirect()->back();
        }
    }
    public function akumulasikaryawan(Request $request)
    {
        $dashboard ="akumulasi";
        $from = date('01-m-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $no_kssd = $request->no_kssd;
        $type = $request->type;
        if ($request->action =='cari') {
            $type = $request->type;
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        } elseif ($request->action =='export') {
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
            $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->orderBy('id', 'ASC')->get();
            if ($request->export =='1' && count($akumulasis)>1) {
                $totalQuery = count($akumulasis);
                $while = ceil($totalQuery / 500);
                $collections = collect($akumulasis);
                return Excel::create($from.' - '.$to, function ($excel) use ($while, $collections) {
                    for ($i = 1; $i <= $while; $i++) {
                        $items = $collections->forPage($i, 500);
                        $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                            $sheet->loadView('administrator.report._report_excel_akumulasi', ['akumulasis' => $items]);
                        });
                    }
                })->export('xls');
            }
        } elseif ($request->action =='csv') {
            $validatedData = $request->validate([
      'upload' => 'required'
      ]);
            $ktp = '12345.'.$request->upload->getClientOriginalExtension();
            $sequence = substr($ktp, -3);
            if ($sequence !=='csv') {
                flash()->overlay('Gagal, Data Upload harus CSV File.', 'INFO');
                return redirect()->back();
            }
            $upload = $request->file('upload');
            $filePath = $upload->getRealPath();
            $file = fopen($filePath, 'r');
            $header = fgetcsv($file);
            $escapedHeader=[];
            //validate
            foreach ($header as $key => $value) {
                $lheader=strtolower($value);
                $escapedItem=preg_replace('/[^a-z]/', '', $lheader);
                array_push($escapedHeader, $escapedItem);
            }
            //looping through othe columns
            while ($columns=fgetcsv($file)) {
                if ($columns[0]=="") {
                    continue;
                }
                //trim data
                foreach ($columns as $key => &$value) {
                    $value=$value;
                }
                // return $columns;
                $data = array_combine($escapedHeader, $columns);
                // setting type
                foreach ($data as $key => &$value) {
                    $value=($key=="nokssd" || $key=="tanggal" || $key=="notransaksi")?(string)$value: (string)$value;
                }
                // return $data;
                // Table update
                $nokssds=$data['nokssd'];
                if ($nokssds == 201701000000) {
                    $type = 'Non';
                } else {
                    $type = 'Anggota';
                }
                $notrxs=$data['notransaksi'];
                $tanggals=$data['tanggal'];
                $nominals=$data['nominal'];
                $gelais = $data['gerai'];
                $simpanan = Akumulasi::firstOrNew(['no_kssd'=>$nokssds,'no_trx'=>$notrxs]);
                $simpanan->no_kssd=$nokssds;
                $simpanan->tgl_trx = $tanggals;
                $simpanan->no_trx = $notrxs;
                $simpanan->nominal=$nominals;
                $simpanan->gerai=$gelais;
                $simpanan->aktif=1;
                $simpanan->type = $type;
                $simpanan->admin = $request->user()->id;
                $simpanan->save();
            }
            flash()->overlay('CSV Berhasil di Upload.', 'INFO');
            return redirect()->back();
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->where('type', 'LIKE', '%'.$type.'%')->orderBy('id', 'ASC')->get();
        return view('karyawan.dataAkumulasi', compact('dashboard', 'akumulasis', 'from', 'to', 'no_kssd', 'type'));
    }
}
