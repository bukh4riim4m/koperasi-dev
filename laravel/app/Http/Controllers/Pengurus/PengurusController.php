<?php

namespace App\Http\Controllers\Pengurus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Komunitas;
use App\Propinsi;
use App\Pendapatan;
use App\Pekerjaan;
use App\JenisSimpanan;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\User;
use App\Simpanan;
use App\Berita;
use App\Simpananadmin;
use App\Akumulasi;
use App\Product;
use App\Datatransaksippobs;
use App\Toko;
use App\Bukusaldotransaksi;
use App\Deposit;
use App\Pesanan;
use App\Status;
use App\Website;
use Response;
use Image;
use Excel;
use DB;
use Log;
use PDF;

class PengurusController extends Controller
{
    public function __construct()
    {
        $this->middleware('pengurus');
    }
    public function homepengurus()
    {
        $dashboard ="dashboard";
        return view('pengurus.index', compact('dashboard'));
    }
    public function datagroup()
    {
        $dashboard ="dataPengguna";
        $nomor ="";
        $name ="";
        $type="";
        $user = User::where('aktif', 1)->where('type', '<>', 'karyawan')->orderBy('no_anggota', 'ASC')->get();
        return view('pengurus.dataPengurus', compact('dashboard', 'user', 'nomor', 'name', 'type'));
    }
    public function carigroup(Request $request)
    {
        $dashboard ="dataPengurus";
        $nomor =$request->no_anggota;
        $name =$request->name;
        $type=$request->type;
        $user = User::where('no_anggota', 'LIKE', '%'.$request->no_anggota.'%')->where('type', '<>', 'karyawan')->where('name', 'LIKE', '%'.$request->name.'%')->where('type', 'LIKE', '%'.$request->type.'%')->get();
        return view('pengurus.dataPengurus', compact('dashboard', 'user', 'nomor', 'name', 'type'));
    }
    public function index(Request $request)
    {
        $dashboard ="dataPengguna";
        $no_anggota = $request->no_anggota;
        $nama = $request->name;
        $user = User::where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('name', 'LIKE', '%'.$nama.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->orderBy('no_anggota', 'ASC')->get();
        return view('pengurus.dataAnggota', compact('dashboard', 'user', 'nama', 'no_anggota'));
    }
    public function mutasi(Request $request)
    {
        $dashboard ="bukuSaldo";
        $anggota = $request->no_anggota;
        $cari = $request->action;
        $jns = 1;
        $from = date('Y');
        $until = date('Y');
        // $dari = date('Y-m-d', strtotime($from));
        // $ke = date('Y-m-d', strtotime($until));
        $users = User::where('no_anggota', $anggota)->where('type', '<>', 'karyawan')->first();
        if ($request->action =='cari') {
            $anggota = $request->no_anggota;
            $from =$request->dari;
            $until =$request->sampai;
            // $dari = date('Y-m-d', strtotime($from));
            // $ke = date('Y-m-d', strtotime($until));
            if (!User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor Anggota Tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('type', '<>', 'karyawan')->first();
        }
        $simpananpokok = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwakaf = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 5)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinfaq = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 6)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananshu = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 7)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananlain = Simpananadmin::where('no_anggota', 'LIKE', '%'.$anggota.'%')->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 8)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('pengurus.bukuSaldo', compact('dashboard', 'anggota', 'from', 'until', 'users', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'simpananwakaf', 'simpananinfaq', 'simpananshu', 'simpananlain', 'cari', 'jns'));
    }

    public function simpananadmin(Request $request)
    {
        $dashboard ="dataSimpanan";
        $nomor = $request->no_anggota;
        $jenissim = $request->jenis_simpanan;
        $from = $request->dari;
        $to = $request->sampai;
        $mutasis = $request->mutasi;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$nomor.'%')->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('mutasi', 'LIKE', '%'.$mutasis.'%')->where('aktif', 1)->orderBy('tgl_setor', 'ASC')->get();
            return view('pengurus.dataSimpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
        }
        $from = date('01-m-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d');
        $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('aktif', 1)->where('mutasi', 'LIKE', '%'.$mutasis.'%')->orderBy('tgl_setor', 'ASC')->get();
        return view('pengurus.dataSimpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to', 'mutasis'));
    }

    public function akumulasiadmin(Request $request)
    {
        $dashboard ="akumulasi";
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $no_kssd = $request->no_kssd;
        if ($request->action =='cari') {
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        } elseif ($request->action =='tambah') {
            if ($users=User::where('no_anggota', 'LIKE', '%'.$request->no_kssd.'%')->where('type', '<>', 'karyawan')->where('aktif', 1)->first()) {
                $tgl_trx = date('Y-m-d', strtotime($request->tgl_trx));
                $akumulasis = Akumulasi::create([
          'no_kssd'=>$users->no_anggota,
          'no_trx'=>date('ymdhis'),
          'tgl_trx'=>$tgl_trx,
          'nominal'=>$request->nominal,
          'aktif'=>1,
          'admin'=>$request->user()->id
        ]);
                if ($akumulasis) {
                    flash()->overlay('Data berhasil di simpan.', 'INFO');
                    return redirect()->back();
                }
                flash()->overlay('Data gagal di simpan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Nomor Anggota Tidak Terdaftar.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='export') {
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
            $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->get();
            if ($request->export =='1') {
                $totalQuery = count($akumulasis);
                $while = ceil($totalQuery / 500);
                $collections = collect($akumulasis);
                return Excel::create($from.' - '.$to, function ($excel) use ($while, $collections) {
                    for ($i = 1; $i <= $while; $i++) {
                        $items = $collections->forPage($i, 500);
                        $excel->sheet('page-' . $i, function ($sheet) use ($items) {
                            $sheet->loadView('pengurus.report._report_excel_akumulasi', ['akumulasis' => $items]);
                        });
                    }
                })->export('xls');
            }
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', 'LIKE', '%'.$no_kssd.'%')->get();
        return view('pengurus.dataAkumulasi', compact('dashboard', 'akumulasis', 'from', 'to', 'no_kssd'));
    }
    public function kartu(Request $request)
    {
        $dashboard ="kartuAnggota";
        $anggota = $request->no_anggota;
        $nama = $request->name;
        $users = User::where('no_anggota', 'LIKE', '%'.$anggota.'%')->where('name', 'LIKE', '%'.$nama.'%')->where('aktif', 1)->where('type', '<>', 'karyawan')->get();
        return view('pengurus.kartuAnggota', compact('dashboard', 'users', 'anggota', 'nama'));
    }
    public function alert(Request $request)
    {
        $dashboard ="Simpananwajib";
        $alertbulan = date('Y');
        $thn = date('Y');
        $bln = date('m');
        $no_anggota = $request->no_anggota;
        $stts = 'Semua';
        $cari ='tidak';
        if ($request->action =='cari') {
            // return $request->all();
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        } elseif ($request->action =='export') {
            $thn = $request->tahun;
            $no_anggota = $request->no_anggota;
            $alertbulan = $request->tahun;
            $bln = $request->bulan;
            $stts = $request->status;
            $cari ='cari';
        }
        $wajib = JenisSimpanan::find(2);
        $simpanan = Simpananadmin::where('no_anggota', 'LIKE', '%'.$no_anggota.'%')->where('jenis_simpanan', 2)->where('mutasi', 'Kredit')->where('aktif', 1)->where('awal', 1)->orderBy('jth_tempo', 'ASC')->get();
        if ($request->action =='export' && $request->excel ==1) {
            return Excel::create($bln.'-'.$alertbulan, function ($excel) use ($thn,$bln,$stts,$no_anggota,$alertbulan,$simpanan) {
                $excel->sheet('Excel sheet', function ($sheet) use ($thn,$bln,$stts,$no_anggota,$alertbulan,$simpanan) {
                    $sheet->loadView('administrator.report._report_status_simpanan_wajib', compact('alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari', 'simpanan'));
                });
            })->export('xls');
        }
        return view('pengurus.alert_simpanan_wajib', compact('dashboard', 'simpanan', 'alertbulan', 'bln', 'thn', 'no_anggota', 'stts', 'wajib', 'cari'));
    }
    public function karyawan(Request $request)
    {
        $dashboard ="dataPengguna";
        $nomor =$request->no_karyawan;
        $name =$request->name;
        $type="";
        if ($request->action =='tambah') {
            $validatedData = $request->validate([
      'name' => 'required|string|max:255',
      'no_karyawan' => 'required|min:6',
      'id_login' => 'required|min:6',
      'password_login' => 'required|min:6'
      ]);
            if ($idlog = User::where('sequence', $request->id_login)->first()) {
                flash()->overlay('GAGAL, ID Login Sudah di gunakan.', 'INFO');
                return redirect()->back();
            }
            if ($idemail = User::where('email', $request->email)->first()) {
                flash()->overlay('GAGAL, Email Sudah di gunakan.', 'INFO');
                return redirect()->back();
            }
            $user = User::create([
        'name' => $request->name,
        'tgl_lahir' => date('Y-m-d', strtotime($request->tgl_lahir)),
        'no_anggota' => $request->no_karyawan,
        'sequence' => $request->id_login,
        'email' => $request->email,
        'jenkel' => $request->jenkel,
        'password' => Hash::make($request->password_login),
        'saldo' => 0,
        'admin' => $request->user()->id,
        'aktif' => 1,
        'type' => 'karyawan'
      ]);
            if ($user) {
                flash()->overlay('Karyawan berhasil di tambahkan.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Karyawan gagal di tambahkan.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='edit') {
            if ($request->fotodiri) {
                $this->validate($request, [
              'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
                $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
            }
            DB::beginTransaction();
            try {
                $user = User::find($request->ids);
                $user->name=$request->name;
                $user->no_anggota=$request->no_karyawan;
                $user->tgl_lahir=date('Y-m-d', strtotime($request->tgl_lahir));
                $user->email=$request->email;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;

                if ($request->fotodiri) {
                    if ($user->fotodiri =='null' || $user->fotodiri =='') {
                        $user->fotodiri=$diri;
                        $destination_foto =public_path('foto');
                        $request->fotodiri->move($destination_foto, $diri);
                    } else {
                        $destination_foto =public_path('foto/'.$user->fotodiri);
                        if (file_exists($destination_foto)) {
                            unlink($destination_foto);
                        }
                        $user->fotodiri=$diri;
                        $destination_foto =public_path('foto');
                        $request->fotodiri->move($destination_foto, $diri);
                    }
                }
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Profil:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Profil.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data Profil berhasil di Edit.', 'INFO');
            return redirect()->back();
        } elseif ($request->action =='hapus') {
            $delete = User::find($request->ids);
            $delete->aktif = 0;
            if ($delete->update()) {
                flash()->overlay('Data Profil berhasil di Hapus.', 'INFO');
                return redirect()->back();
            }
            flash()->overlay('Data Profil Gagal di Hapus.', 'INFO');
            return redirect()->back();
        }
        $user = User::where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('name', 'LIKE', '%'.$name.'%')->where('aktif', 1)->where('type', 'karyawan')->orderBy('no_anggota', 'ASC')->get();
        return view('pengurus.dataKaryawan', compact('dashboard', 'user', 'nomor', 'name', 'type'));
    }
    public function saldoakumulasipengurus(Request $request)
    {
        $dashboard ="akumulasi";
        $nomor = $request->no_anggota;
        $tahun = date('Y');
        if ($request->action =='cari') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
        } elseif ($request->action =='edit') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $data = Akumulasi::find($request->ids);
            $data->no_kssd = $nomor;
            $data->tgl_trx = date('Y-m-m', strtotime($request->tgl_trx));
            $data->nominal = $request->nominal;
            $data->gerai = $request->gerai;
            if ($data->update()) {
                flash()->overlay('Data berhasil diEdit.', 'INFO');
                $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
                return view('pengurus.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
            }
            flash()->overlay('Data gagal diEdit.', 'INFO');
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
            return view('pengurus.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
        } elseif ($request->action =='hapus') {
            $tahun = $request->tahun;
            if (!$users = User::where('aktif', 1)->where('no_anggota', 'LIKE', '%'.$nomor.'%')->where('type', '<>', 'karyawan')->first()) {
                flash()->overlay('Nomor tidak Terdaftar.', 'INFO');
                return redirect()->back();
            }
            $data = Akumulasi::find($request->ids);
            $data->aktif = 0;
            if ($data->update()) {
                flash()->overlay('Data berhasil diHapus.', 'INFO');
                $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
                return view('pengurus.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
            }
            flash()->overlay('Data gagal diHapus.', 'INFO');
            $datas = Akumulasi::where('no_kssd', 'LIKE', '%'.$nomor.'%')->where('aktif', 1)->where('tgl_trx', 'LIKE', '%'.$tahun.'%')->orderBy('id', 'ASC')->get();
            return view('pengurus.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
        }
        return view('pengurus.saldo_akumulasi', compact('dashboard', 'users', 'nomor', 'datas', 'tahun'));
    }
    public function statistik(Request $request)
    {
        $dashboard ="statistik";
        $dari = date("Y-m-d");
        $sampai = date("Y-m-d");
        if ($request->dari && $request->sampai) {
            $dari = date('Y-m-d', strtotime($request->dari));
            $sampai = date('Y-m-d', strtotime($request->sampai));
        }
        $totalramanuju = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Anggota'");
        $totalgrogol = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Anggota'");
        $totalpci = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Anggota'");
        $totalramanujunon = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Non'");
        $totalgrogolnon = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Non'");
        $totalpcinon = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Non'");
        if ($request->action =='excel' && $request->excel ==1) {
            return Excel::create($dari.'-'.$sampai, function ($excel) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
                $excel->sheet('Excel sheet', function ($sheet) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
                    $sheet->cells('A1:C1', function ($cells) {
                        $cells->setBackground('#ffff00');
                    });
                    $sheet->cells('A6:C6', function ($cells) {
                        $cells->setBackground('#ffff00');
                    });
                    $sheet->cells('A8:C8', function ($cells) {
                        $cells->setBackground('#66ff66');
                    });
                    $sheet->cells('A13:C13', function ($cells) {
                        $cells->setBackground('#66ff66');
                    });
                    $sheet->cells('A15:C15', function ($cells) {
                        $cells->setBackground('#66ffcc');
                    });
                    $sheet->cells('A20:C20', function ($cells) {
                        $cells->setBackground('#66ffcc');
                    });
                    $sheet->loadView('administrator.report._report_statistik_penjualan', compact('totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
                });
            })->export('xls');
        } elseif ($request->action =='pdf' && $request->excel ==1) {
            $pdf = PDF::loadView('administrator.report.pdf_statistik_penjualan', compact('totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
            return $pdf->download($dari.'_'.$sampai.'_Statistik.pdf');
        }
        return view('pengurus.statistik', compact('dashboard', 'totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
    }
    public function statistiksimpanan(Request $request)
    {
        $dashboard = "statistik";
        $pokoks = DB::select("SELECT SUM(simpananadmins.nominal) as totalpokok FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '1' AND simpananadmins.mutasi = 'Kredit'");
        $wajins = DB::select("SELECT SUM(simpananadmins.nominal) as totalwajib FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '2' AND simpananadmins.mutasi = 'Kredit'");
        $sukarelas = DB::select("SELECT SUM(simpananadmins.nominal) as totalsukarela FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '3' AND simpananadmins.mutasi = 'Kredit'");
        $investasis = DB::select("SELECT SUM(simpananadmins.nominal) as totalinvestasi FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '4' AND simpananadmins.mutasi = 'Kredit'");
        $wakafs = DB::select("SELECT SUM(simpananadmins.nominal) as totalwakaf FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '5' AND simpananadmins.mutasi = 'Kredit'");
        $infaqs = DB::select("SELECT SUM(simpananadmins.nominal) as totalinfak FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '6' AND simpananadmins.mutasi = 'Kredit'");
        $shus = DB::select("SELECT SUM(simpananadmins.nominal) as totalshu FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '7' AND simpananadmins.mutasi = 'Kredit'");
        $lains = DB::select("SELECT SUM(simpananadmins.nominal) as totallain FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '8' AND simpananadmins.mutasi = 'Kredit'");
        return view('pengurus.statistik_simpanan', compact('dashboard', 'pokoks', 'wajins', 'sukarelas', 'investasis', 'wakafs', 'infaqs', 'shus', 'lains'));
    }
    public function statistikkelurahan()
    {
        $dashboard = "statistik";
        $kelurahans = Kelurahan::where('aktif', 1)->get();
        return view('pengurus.statistikKelurahan', compact('dashboard', 'kelurahans'));
    }
    public function statistikkecamatan()
    {
        $dashboard = "statistik";
        $kecamatans = Kecamatan::where('aktif', 1)->get();
        return view('pengurus.statistik_kecamatan', compact('dashboard', 'kecamatans'));
    }
    public function statistikkabupaten()
    {
        $dashboard = "statistik";
        $kabupatens = Kabupaten::where('aktif', 1)->get();
        return view('pengurus.statistk_kabupaten', compact('dashboard', 'kabupatens'));
    }
    public function chart(Request $request)
    {
        $dashboard = "statistik";
        $lakis = DB::select("SELECT SUM(users.aktif) as totalLaki FROM users WHERE users.aktif ='1' AND users.jenkel = 'Laki-Laki'");
        $perempuan = DB::select("SELECT SUM(users.aktif) as totalPerempuan FROM users WHERE users.aktif ='1' AND users.jenkel = 'Perempuan'");
        return view('pengurus.statistikJenkel', compact('dashboard', 'lakis', 'perempuan'));
    }
    public function cekharga(Request $request)
    {
        $dashboard ="tokoOnline";
        $access_token = Website::find(1);
        $url = $access_token->url.'hargaagent/PLN';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', $url, [
                        'headers'=>[
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json',
                            'Authorization' => 'Bearer '.$access_token->token
                        ]
                    ]);
        if ($res->getStatusCode() == 200) {
          $pln =  json_decode($res->getBody()->getContents(), true);
        }
        foreach ($pln as $value) {
            if ($produks = Product::where('code', $value['code'])->first()) {
                if ($produks->jual < $value['price']) {
                  $produks->jual = $value['price']+300;
                }
                $produks->price = $value['price'];
                $produks->status = $value['status'];
                $produks->update();
            }
            // $produks = Product::create([
      //   'code'=>$value['code'],
      //   'description'=>$value['description'],
      //   'operator'=>$value['operator'],
      //   'price'=>$value['price'],
      //   'jual'=>$value['price'],
      //   'status'=>$value['status'],
      //   'provider_sub'=>$value['provider_sub']
      // ]);
        }
        $stts = $request->status;
        $deskropsi = $request->description;
        $providers = $request->provider;
        $nominal= str_replace(".", "", $request->jual);
        $pulsa = Product::orderBy('id', 'ASC')->where('operator', 'LIKE', '%'.$providers.'%')->where('description', 'LIKE', '%'.$deskropsi.'%')->where('status', 'LIKE', '%'.$stts.'%')->get();

        return view('pengurus.data_harga_pulsa', compact('dashboard', 'pulsa', 'stts', 'deskropsi', 'providers'));
    }
    public function produk(Request $request)
    {
        $dashboard ="tokoOnline";
        $kode = $request->kode;
        $name = $request->name;
        $produks = Toko::where('aktif', 1)->where('kode', 'LIKE', '%'.$kode.'%')->where('name', 'LIKE', '%'.$name.'%')->orderBy('id', 'DESC')->get();
        return view('pengurus.produk', compact('dashboard', 'produks', 'kode', 'name'));
    }
    public function bukusaldoanggota(Request $request)
    {
        $dashboard = 'bukuSaldo';
        $cari = $request->action;
        if (!$users = User::where('no_anggota', 'LIKE', '%'.$request->no_anggota.'%')->first()) {
            flash()->overlay('Nomor Anggot Salah.', 'INFO');
            return redirect()->back();
        };
        $no_anggota = $users->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        $datasppob = Bukusaldotransaksi::whereBetWeen('tgl_trx', [$dari,$ke])->where('no_anggota', $no_anggota)->where('aktif', 1)->orderBy('created_at', 'ASC')->get();
        // $users = User::find($request->user()->id);
        return view('pengurus.bukusaldoTransaksi', compact('dashboard', 'from', 'until', 'users', 'datasppob', 'no_anggota', 'cari'));
    }
    public function laporan(Request $request)
    {
        $dashboard = 'deposit';
        $dari = date('Y-m-d');
        $sampai = date('Y-m-d');
        $stts = "";
        $deposits = Deposit::whereBetWeen('tgl_trx', [$dari,$sampai])->where('aktif', 1)->where('status', '<>', 1)->orderBy('id', 'DESC')->get();
        if ($request->action == 'cari') {
            $stts = $request->status;
            $dari = date('Y-m-d', strtotime($request->dari));
            $sampai = date('Y-m-d', strtotime($request->sampai));
            $deposits = Deposit::whereBetWeen('tgl_trx', [$dari,$sampai])->where('aktif', 1)->where('status', '<>', 1)->where('status', 'LIKE', '%'.$stts.'%')->orderBy('id', 'DESC')->get();
            if ($request->export =='download') {
                return Excel::create($request->dari.'-'.$request->sampai, function ($excel) use ($stts,$dari,$sampai,$deposits) {
                    $excel->sheet('Excel sheet', function ($sheet) use ($stts,$dari,$sampai,$deposits) {
                        $sheet->loadView('administrator.report._report_deposit_anggota', compact('deposits', 'dari', 'sampai', 'stts', 'stts', 'wajib', 'cari', 'simpanan'));
                    });
                })->export('xls');
            }
        }

        return view('pengurus.laporan_deposit', compact('dashboard', 'deposits', 'stts', 'dari', 'sampai'));
    }
    public function topupsuplayer(Request $request)
    {
        $dashboard = 'deposit';
        if ($request->action =='topup') {
            $trxid = date('YmdHis').$request->user()->id;
            $nominal= str_replace(".", "", $request->nominal);

            // ??ppppppppppppppppppppppppppppp
            $params['trxid']=$trxid;
            $params['bank']=$request->bank;
            $params['nominal']=$nominal;
            $access_token = Website::find(1);
            $url = $access_token->url.'topup-suplayer';
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                                    'headers'=>[
                                        'Accept' => 'application/json',
                                        'Content-Type' => 'application/json',
                                        'Authorization' => 'Bearer '.$access_token->token
                                    ],
                                          'body'=>json_encode($params)
                                ]);
            if ($res->getStatusCode() == 200) {
                $hasil =  json_decode($res->getBody()->getContents(), true);
                if ($hasil['result'] =='success') {
                    flash()->overlay($hasil['message'], 'INFO');
                    return redirect()->back();
                } else {
                    // Silakan transfer sebesar Rp 500.507,- Ke Rekening: BCA, no. 0770441111, a.n. BENY ARIF L. Batas waktu transfer 1x24jam
                    flash()->overlay($hasil['message'], 'GAGAL');
                    return redirect()->back();
                }
            } else {
                flash()->overlay('Gagal menampilkan ddata', 'INFO');
                return redirect()->back();
            }
        }
        $from = date('Y-m-01');
        $until = date('Y-m-d');
        $dari = date('01-m-Y', strtotime($from));
        $sampai = date('d-m-Y', strtotime($until));
        if ($request->action == 'cari') {
            $from = date('Y-m-d', strtotime($request->from));
            $until = date('Y-m-d', strtotime($request->until));
            $dari = date('d-m-Y', strtotime($from));
            $sampai = date('d-m-Y', strtotime($until));
        }

        $status = $request->status;
        // ??ppppppppppppppppppppppppppppp
        $params['status']=$status;
        $params['from']=$from;
        $params['until']=$until;
        $access_token = Website::find(1);
        $url = $access_token->url.'data-suplayer';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
                                'headers'=>[
                                    'Accept' => 'application/json',
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer '.$access_token->token
                                ],
                                      'body'=>json_encode($params)
                            ]);
        if ($res->getStatusCode() == 200) {
            $hasil =  json_decode($res->getBody()->getContents(), true);
        } else {
            flash()->overlay('Gagal menampilkan ddata', 'INFO');
            return redirect()->back();
        }
        return view('pengurus.topup_deposit', compact('dashboard', 'hasil', 'dari', 'sampai', 'status'));
    }
    public function bukusaldosuplayer(Request $request)
    {
        $dashboard = 'deposit';
        $from = date('Y-m-01');
        $until = date('Y-m-d');
        if ($request->dari) {
            $from = date('Y-m-d', strtotime($request->dari));
            $until = date('Y-m-d', strtotime($request->sampai));
        }
        $dari = date('d-m-Y', strtotime($from));
        $sampai = date('d-m-Y', strtotime($until));
        // ??ppppppppppppppppppppppppppppp
        $params['from']=$from;
        $params['until']=$until;
        $access_token = Website::find(1);
        $url = $access_token->url.'buku-saldo-suplayer';
        $client = new \GuzzleHttp\Client();
        $res = $client->request('POST', $url, [
                            'headers'=>[
                                'Accept' => 'application/json',
                                'Content-Type' => 'application/json',
                                'Authorization' => 'Bearer '.$access_token->token
                            ],
                                  'body'=>json_encode($params)
                        ]);
        if ($res->getStatusCode() == 200) {
            $hasil =  json_decode($res->getBody()->getContents(), true);
            $hasiluser = $hasil['users'];
            $hasilbukusaldo = $hasil['bukusaldo'];
            $dashboard = "datasuplayer";
        } else {
            flash()->overlay('Gagal menampilkan ddata', 'INFO');
            return redirect()->back();
        }

        // ppppppppppppppppppppppppppppp?
        return view('pengurus.buku_saldo', compact('dashboard', 'hasiluser', 'hasilbukusaldo', 'dari', 'sampai'));
    }
    public function laporanppob(Request $request)
    {
        $dashboard = 'laporan';
        $from = date('Y-m-d');
        $until = $from;
        $stts = $request->status;
        if ($request->has('from') && $request->has('until')) {
          $from = date('Y-m-d',strtotime($request->from));
          $until = date('Y-m-d',strtotime($request->until));
        }
        $datas = Datatransaksippobs::whereBetWeen('tgl_trx',[$from,$until])->where('status','LIKE','%'.$stts.'%')->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $from = date('d-m-Y',strtotime($from));
        $until = date('d-m-Y',strtotime($until));
        return view('pengurus.laporan_ppob', compact('dashboard', 'datas','from','until','stts'));
    }
    public function foto($id)
    {
        $user = User::find($id);
        $filepath = public_path('/foto/').$user->fotodiri;
        return Response::download($filepath, $user->no_anggota.".jpg");
    }
    public function terimabarang(Request $request){
      $dashboard = 'laporan';
      $stts = $request->status;
      $today = date('Y-m-d');
      $from = $today;
      $until= $today;
      if ($request->from && $request->until) {
        $from = date('Y-m-d', strtotime($request->from));
        $until= date('Y-m-d', strtotime($request->until));
      }
      $datas = Pesanan::whereBetWeen('tanggal',[$from,$until])->where('status','LIKE','%'.$request->status.'%')->get();
      $from = date('d-m-Y', strtotime($from));
      $until= date('d-m-Y', strtotime($until));
      $statuses = Status::get();
      return view('pengurus.laporan_barang',compact('dashboard','datas','stts','from','until','statuses'));
    }

}
