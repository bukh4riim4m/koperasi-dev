<?php

namespace App\Http\Controllers\Anggota;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Simpananadmin;
use App\User;
use App\Website;
use Auth;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Propinsi;
use App\Pesanan;
use App\Akumulasi;
use App\Bukusaldotransaksi;
use App\Menutransaksi;
use App\Deposit;
use App\Bank;
use App\Product;
use App\Toko;
use App\Datatransaksippobs;
use App\City;
use App\Status;
use Response;
use Image;
use DB;
use Log;
use PDF;

class ProfilController extends Controller
{
    public function __construct()
    {
        $this->middleware('anggota');
    }
    public function home()
    {
        $dashboard ="dashboard";
        return view('anggota.index', compact('dashboard'));
    }
    public function profil(Request $request)
    {
        $dashboard ="profil";
        if ($request->fotodiri) {
            $this->validate($request, [
            'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
            // $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
        }

        if ($request->tgl_lahir =="") {
            $tanggal_lahir = "";
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }
        if ($request->action =='edit') {
            DB::beginTransaction();
            try {
                $user = User::find($request->user()->id);
                if ($request->file('fotodiri')) {
                    // return 'OK';
                    if ($user->fotodiri =='null' || $user->fotodiri =='') {
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');
                        // return $directory;
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    } else {
                        $destination_foto =public_path('foto/'.$user->fotodiri);
                        if (file_exists($destination_foto)) {
                            unlink($destination_foto);
                        }
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');

                        $imageUrl = $directory.$fileName;
                        // return $imageUrl;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    }
                }
                $user->nik=$request->nik;
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Profil:'.$e->getMessage());
                DB::rollback();
                flash()->overlay("<p class='alert alert-danger'>Gagal Edit Profil.</p>", "INFO");
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay("<p class='alert alert-success'>Data Profil berhasil di Edit.</p>", "INFO");
            return redirect()->back();
        }
        return view('anggota.profil', compact('dashboard'));
    }
    public function simpanananggota(Request $request)
    {
        $dashboard ="simpanan";
        $nomor = $request->user()->no_anggota;
        $jenissim = $request->jenis_simpanan;
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $simpanan = Simpananadmin::where('no_anggota', $nomor)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('aktif', 1)->get();
            return view('anggota.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to'));
        }
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('no_anggota', $request->user()->no_anggota)->where('aktif', 1)->get();
        return view('anggota.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to'));
    }
    public function bukusaldo(Request $request)
    {
        $dashboard ="bukuSaldo";
        $anggota = $request->user()->no_anggota;
        $from = date('Y');
        $until = date('Y');
        // $dari = date('Y-m-d', strtotime($from));
        // $ke = date('Y-m-d', strtotime($until));
        if ($request->action =='cari') {
            $from =$request->dari;
            $until =$request->sampai;
            // $dari = date('Y-m-d', strtotime($from));
      // $ke = date('Y-m-d', strtotime($until));
        }
        $users = User::where('no_anggota', $anggota)->first();
        $simpananpokok = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwakaf = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 5)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinfaq = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 6)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananshu = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 7)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananlain = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$from.'-01-01',$until.'-12-31'])->where('jenis_simpanan', 8)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.bukusaldo', compact('dashboard', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'simpananwakaf', 'simpananinfaq', 'simpananshu', 'simpananlain', 'anggota', 'from', 'until', 'users'));
    }
    public function saldotransaksi(Request $request)
    {
        $dashboard = 'bukuSaldo';
        $nomor = Auth::user()->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        if ($request->dari && $request->sampai) {
            $from = $request->dari;
            $until = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
        }
        $datasppob = Bukusaldotransaksi::whereBetWeen('tgl_trx', [$dari,$ke])->where('no_anggota', $nomor)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $users = User::find($request->user()->id);
        return view('anggota.bukusaldoTransaksi', compact('dashboard', 'from', 'until', 'users', 'datasppob'));
    }
    public function password()
    {
        $dashboard ="gantiPassword";
        return view('anggota.gantiPassword', compact('dashboard'));
    }
    public function gantipassword(Request $request)
    {
        // Log::info(' ALERt :'.$request);

        if (\Hash::check($request->input('password_lm'), $request->user()->password)) {
            $user = User::find($request->user()->id);
            $user->password = bcrypt($request->input('password_br'));
            $user->update();
            flash()->overlay("<p class='alert alert-success'>Password Berhasil di rubah.</p>", "INFO");
            return redirect()->back();
        } else {
            flash()->overlay("<p class='alert alert-danger'>Password Gagal di rubah<br>Password Lama Salah</p>", "INFO");
            return redirect()->back();
        }
    }

    public function tambahpropinsi(Request $request)
    {
        $dashboard ="dataMaster";
        if (Propinsi::where('name', $request->propinsi)->where('aktif', 1)->first()) {
            flash()->overlay("<p class='alert alert-danger'>Data Gagal input.</p>", "INFO");
            return redirect()->back();
        }
        $komunitas = Propinsi::create([
      'name'=>$request->propinsi,
      'aktif'=>1
    ]);
        flash()->overlay("<p class='alert alert-success'>Data berhasil input.</p>", "INFO");
        return redirect()->back();
    }
    public function tambahkabupaten(Request $request)
    {
        $dashboard ="dataMaster";
        if (Kabupaten::where('kabupaten', $request->kabupaten)->where('aktif', 1)->first()) {
            flash()->overlay("<p class='alert alert-danger'>Data Gagal input.</p>", "INFO");
            return redirect()->back();
        }
        $komunitas = Kabupaten::create([
      'kabupaten'=>$request->kabupaten,
      'aktif'=>1,
      'admin'=>$request->user()->id
    ]);
        flash()->overlay("<p class='alert alert-success'>Data berhasil input.</p>", "INFO");
        return redirect()->back();
    }
    public function tambahkecamatan(Request $request)
    {
        $dashboard ="dataMaster";
        if (Kecamatan::where('name', $request->kecamatan)->where('aktif', 1)->first()) {
            flash()->overlay("<p class='alert alert-danger'>Data Gagal input.</p>", "INFO");
            return redirect()->back();
        }
        $komunitas = Kecamatan::create([
      'name'=>$request->kecamatan,
      'aktif'=>1,
      'admin'=>$request->user()->id
    ]);
        flash()->overlay("<p class='alert alert-success'>Data berhasil input.</p>", "INFO");
        return redirect()->back();
    }
    public function tambahkelurahan(Request $request)
    {
        $dashboard ="dataMaster";
        if (Kelurahan::where('name', $request->kelurahan)->where('aktif', 1)->first()) {
            flash()->overlay("<p class='alert alert-danger'>Data Gagal input.</p>", "INFO");
            return redirect()->back();
        }
        $komunitas = Kelurahan::create([
      'name'=>$request->kelurahan,
      'aktif'=>1,
      'admin'=>$request->user()->id
    ]);
        flash()->overlay("<p class='alert alert-success'>Data berhasil input.</p>", "INFO");
        return redirect()->back();
    }
    public function kartu()
    {
        $dashboard ="kartu";
        return view('anggota.kartuAnggota', compact('dashboard'));
    }
    public function download($id)
    {
        $user = User::where('sequence', $id)->first();
        $pdf = PDF::loadView('administrator.report.pdf', compact('user'));
        return $pdf->download('kartu.pdf');
    }
    public function akumulasi(Request $request)
    {
        $dashboard ="akumulasi";
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $from = $request->dari;
            $to = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', $request->user()->no_anggota)->get();
        return view('anggota.akumulasi', compact('dashboard', 'akumulasis', 'from', 'to'));
    }
    public function statistik(Request $request)
    {
        $dashboard = "statistik";
        $nomor = $request->user()->no_anggota;
        $pokoks = DB::select("SELECT SUM(simpananadmins.nominal) as totalpokok FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '1' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $wajins = DB::select("SELECT SUM(simpananadmins.nominal) as totalwajib FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '2' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $sukarelas = DB::select("SELECT SUM(simpananadmins.nominal) as totalsukarela FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '3' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $investasis = DB::select("SELECT SUM(simpananadmins.nominal) as totalinvestasi FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '4' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $wakafs = DB::select("SELECT SUM(simpananadmins.nominal) as totalwakaf FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '5' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $infaqs = DB::select("SELECT SUM(simpananadmins.nominal) as totalinfak FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '6' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $shus = DB::select("SELECT SUM(simpananadmins.nominal) as totalshu FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '7' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        $lains = DB::select("SELECT SUM(simpananadmins.nominal) as totallain FROM simpananadmins WHERE simpananadmins.aktif ='1' AND simpananadmins.jenis_simpanan = '8' AND simpananadmins.mutasi = 'Kredit' AND simpananadmins.no_anggota = '{$nomor}'");
        return view('anggota.statistik_simpanan', compact('dashboard', 'pokoks', 'wajins', 'sukarelas', 'investasis', 'wakafs', 'infaqs', 'shus', 'lains'));
    }
    public function penjualan(Request $request)
    {
        $dashboard = "statistik";
        $dashboard ="statistik";
        $dari = date("Y-m-d");
        $sampai = date("Y-m-d");
        if ($request->dari && $request->sampai) {
            $dari = date('Y-m-d', strtotime($request->dari));
            $sampai = date('Y-m-d', strtotime($request->sampai));
        }
        $totalramanuju = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Anggota' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        $totalgrogol = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Anggota' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        $totalpci = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Anggota' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        $totalramanujunon = DB::select("SELECT SUM(akumulasis.nominal) as totalRamanujuAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Ramanuju' AND akumulasis.type = 'Non' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        $totalgrogolnon = DB::select("SELECT SUM(akumulasis.nominal) as totalGrogolAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'Grogol' AND akumulasis.type = 'Non' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        $totalpcinon = DB::select("SELECT SUM(akumulasis.nominal) as totalPciAnggota FROM akumulasis WHERE akumulasis.tgl_trx BETWEEN '{$dari}' AND '{$sampai}' AND akumulasis.aktif ='1' AND akumulasis.gerai = 'PCI' AND akumulasis.type = 'Non' AND akumulasis.no_kssd = '{$request->user()->no_anggota}'");
        if ($request->action =='excel' && $request->excel ==1) {
            return Excel::create($dari.'-'.$sampai, function ($excel) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
                $excel->sheet('Excel sheet', function ($sheet) use ($totalramanuju,$totalgrogol,$totalpci,$totalramanujunon,$totalgrogolnon,$totalpcinon,$dari,$sampai) {
                    $sheet->cells('A1:C1', function ($cells) {
                        $cells->setBackground('#ffff00');
                    });
                    $sheet->cells('A6:C6', function ($cells) {
                        $cells->setBackground('#ffff00');
                    });
                    $sheet->cells('A8:C8', function ($cells) {
                        $cells->setBackground('#66ff66');
                    });
                    $sheet->cells('A13:C13', function ($cells) {
                        $cells->setBackground('#66ff66');
                    });
                    $sheet->cells('A15:C15', function ($cells) {
                        $cells->setBackground('#66ffcc');
                    });
                    $sheet->cells('A20:C20', function ($cells) {
                        $cells->setBackground('#66ffcc');
                    });
                    $sheet->loadView('administrator.report._report_statistik_penjualan', compact('totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
                });
            })->export('xls');
        } elseif ($request->action =='pdf' && $request->excel ==1) {
            $pdf = PDF::loadView('administrator.report.pdf_statistik_penjualan', compact('totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
            return $pdf->download($dari.'_'.$sampai.'_Statistik.pdf');
        }
        return view('anggota.statistik_akumulasi', compact('dashboard', 'totalramanuju', 'totalgrogol', 'totalpci', 'totalramanujunon', 'totalgrogolnon', 'totalpcinon', 'dari', 'sampai'));
    }
    public function kelamin(Request $request)
    {
        $dashboard = "statistik";
        $lakis = DB::select("SELECT SUM(users.aktif) as totalLaki FROM users WHERE users.aktif ='1' AND users.jenkel = 'Laki-Laki'");
        $perempuan = DB::select("SELECT SUM(users.aktif) as totalPerempuan FROM users WHERE users.aktif ='1' AND users.jenkel = 'Perempuan'");
        return view('anggota.statistikJenkel', compact('dashboard', 'lakis', 'perempuan'));
    }
    public function kelurahan(Request $request)
    {
        $dashboard = "statistik";
        $kelurahans = Kelurahan::where('aktif', 1)->get();
        return view('anggota.statistikKelurahan', compact('dashboard', 'kelurahans'));
    }
    public function kecamatan(Request $request)
    {
        $dashboard = "statistik";
        $kecamatans = Kecamatan::where('aktif', 1)->get();
        return view('anggota.statistik_kecamatan', compact('dashboard', 'kecamatans'));
    }
    public function kabupaten(Request $request)
    {
        $dashboard = "statistik";
        $kabupatens = Kabupaten::where('aktif', 1)->get();
        return view('anggota.statistk_kabupaten', compact('dashboard', 'kabupatens'));
    }
    public function topup(Request $request)
    {
        $dashboard = 'deposit';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        $nominal= str_replace(".", "", $request->nominal);
        $uniks = substr($nominal, -4);
        $digit = substr($request->user()->no_anggota, -4);
        $total = 50000+$digit;
        // return $uniks.' '.$digit;

        if ($request->action =='topup') {
            if ($nominal < $total) {
              $totals = number_format($total);
                flash()->overlay("<p class='alert alert-danger'>Minimal deposit Rp 50,000 + $digit = Rp $totals </p>", "INFO");
                return redirect()->back();
            }
            if ($uniks !== $digit) {
                flash()->overlay("<p class='alert alert-danger'>Kode Unik tidak sesuai, Kode unik anda : $digit </p>", "INFO");
                return redirect()->back();
            }
            if (Deposit::where('nominal', $nominal)->where('bank', 'LIKE', '%'.$request->bank.'%')->where('status', 1)->where('user_id', $request->user()->id)->first()) {
                flash()->overlay("<p class='alert alert-danger'>Deposit sebelumnya masih dalam pengecekan.</p>", "INFO");
                return redirect()->back();
            }
            $banks = Bank::where('bank', $request->bank)->first();
            $create = Deposit::create([
        'user_id'=>$request->user()->id,
        'no_anggota'=>$request->user()->no_anggota,
        'no_trx'=>date('Ymd').rand(0, 100),
        'tgl_trx'=>date('Y-m-d'),
        'bank'=>$banks->bank.' : '.$banks->no_rekening,
        'nominal'=> $nominal,
        'status'=>1,
        'aktif'=>1,
        'created_by'=>$request->user()->id
      ]);
            if ($create) {
                flash()->overlay("<p class='alert alert-success'>Konfirmasi berhasil di kirim.</p>", "INFO");
                return redirect()->route('data.topup');
            }
            flash()->overlay("<p class='alert alert-danger'>Konfirmasi Gagal di kirim.</p>", "INFO");
            return redirect()->back();
        }
        $datas = Deposit::where('aktif', 1)->where('no_anggota', $request->user()->no_anggota)->orderBy('id', 'DESC')->get();
        return view('anggota.topup_deposit', compact('dashboard', 'menus', 'datas'));
    }
    public function bankselect(Request $request)
    {
        if ($request->ajax()) {
            Log::info('BANK = '.$request->bank);
            $bankss = Bank::where('bank', $request->bank)->first();
            Log::info('$detail = '.$bankss);
            $option = "";
            $option.="<p>No. Rek : ".$bankss->no_rekening."  <br> Atas Nama : ".$bankss->atas_nama."</p>";
            return $option;
        }
    }
    public function pulsa(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.pulsa', compact('dashboard', 'menus'));
    }
    public function paketdata(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.paket_data', compact('dashboard', 'menus'));
    }
    public function voucher(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.voucher_listrik', compact('dashboard', 'menus'));
    }
    public function saldoojek(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.saldo_ojek', compact('dashboard', 'menus'));
    }
    public function paketsms(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.paket_sms', compact('dashboard', 'menus'));
    }
    public function transferpulsa(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.transfer_pulsa', compact('dashboard', 'menus'));
    }
    public function selectprovider(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->provider);
            $pakets = Product::where('operator', $request->provider)->orderBy('jual', 'ASC')->get();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<option value=''>Pilih Nominal Pulsa</option>";
            foreach ($pakets as $key => $paket) {
                $option.="<option value='".$paket->code."'>".$paket->description."</option>";
            }
            return $option;
        }
    }
    public function selectpaket(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->paket);
            $pakets = Product::where('code', $request->paket)->first();
            Log::info('$detail = '.$pakets);
            $option = "";
            $option.="<label>Rp. ".number_format($pakets->jual).",-</label>";
            return $option;
        }
    }
    public function belanja(Request $request)
    {
        $dashboard ="transaksi";
        $kode = "";
        $name = "";
        if ($request->action == 'cari') {
            $kode = $request->kode;
            $name = $request->name;
        } elseif ($request->action == 'beli') {
            $produk = Toko::find($request->ids);
            // return dd($produk);
            //PROPINSI
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "key: da08d414a043e0001fd33b5b3ad29ad0"
              ),
            ));

            $response = curl_exec($curl);
            // return dd($response);
            $resarr =json_decode($response, true);
            $detail =  $resarr['rajaongkir']['results'];
            // return dd($detail);
            // $err = curl_error($curl);
            //
            // curl_close($curl);


            return view('anggota.pemesanan', compact('produk', 'dashboard', 'kode', 'name', 'detail'));
        }
        $produks = Toko::where('aktif', 1)->where('kode', 'LIKE', '%'.$kode.'%')->where('name', 'LIKE', '%'.$name.'%')->orderBy('id', 'DESC')->get();
        return view('anggota.belanja', compact('dashboard', 'produks', 'kode', 'name'));
    }
    public function prosesppob(Request $request)
    {
        if ($request->nomorhp =="" || $request->paket =="") {
            $params['code'] = '400';
            $params['messages'] = "<p class='alert alert-danger'>Isi form dengan lengkap</p>";
            return $params;
        }
        $this->validate($request, [
            'nomorhp' => 'required',
            'paket' => 'required'
        ]);
        $harga = Product::where('code', $request->paket)->first();
        if (Auth::user()->saldotransaksi > $harga->jual) {
            if (date('H') > 23 || date('H') < 1) {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Saat ini sedang maintenance jam 23:00 s/d 01:00 WIB</p>";
                return $params;
            }
            $saldo = Auth::user()->saldotransaksi - $harga->jual;
            $trxid = date('YmdHis').$request->user()->id;
            $nomorhp = $request->nomorhp;
            // if (Datatransaksippobs::where("tgl_trx",date('Y-m-d'))->where("paket",$harga->description)->where("nomorhp",$nomorhp)->first()) {
            //   $params['code'] = '400';
            //   $params['messages'] = "<p class='alert alert-danger'>Transaksi dengan Nomor dan nominal yang sama maksimal 1x perhari</p>";
            //   return $params;
            // }
            DB::beginTransaction();
            try {
                Datatransaksippobs::create([
                  'user_id'=>$request->user()->id,
                  'no_anggota'=>$request->user()->no_anggota,
                  'no_trx'=>$trxid,
                  'tgl_trx'=>date('Y-m-d'),
                  'mutasi'=>'Kredit',
                  'paket'=>$harga->description,
                  'nominal'=>$harga->jual,
                  'nta'=>$harga->price,
                  'saldo'=>$saldo,
                  'status'=>'Proses',
                  'nomorhp'=>$nomorhp,
                  'aktif'=>1,
                  'keterangan'=>'Transaksi '.$harga->description,
                  'created_by'=>$request->user()->id
                ]);
                User::find($request->user()->id)->update([
                  'saldotransaksi'=>$saldo
                ]);
            } catch (\Exception $e) {
                Log::info('Gagal Transaksi PPOB:'.$e->getMessage());
                DB::rollback();
                flash()->overlay("<p class='alert alert-danger'>Transaksi GAGAL.</p>", "INFO");
                return redirect()->back();
            }
            DB::commit();
            // ????????
            $params['trxid']=$trxid;
            $params['nomor']=$request->nomorhp;
            $params['code']=$request->paket;
            $access_token = Website::find(1);
            $url = $access_token->url.'proses-agent';
            $client = new \GuzzleHttp\Client();
            $res = $client->request('POST', $url, [
                            'headers'=>[
                                'Accept' => 'application/json',
                                'Content-Type' => 'application/json',
                                'Authorization' => 'Bearer '.$access_token->token
                            ],
                                  'body'=>json_encode($params)
                        ]);
            if ($res->getStatusCode() == 200) {
                $hasil =  json_decode($res->getBody()->getContents(), true);
                if ($hasil['result'] =='success') {
                    Bukusaldotransaksi::create([
                    'user_id'=>$request->user()->id,
                    'no_anggota'=>$request->user()->no_anggota,
                    'no_trx'=>$trxid,
                    'tgl_trx'=>date('Y-m-d'),
                    'nominal'=>$harga->jual,
                    'saldo'=>$saldo,
                    'mutasi'=>'Debet',
                    'keterangan'=>'Transaksi '.$harga->description.' '.$request->nomorhp,
                    'aktif'=>1,
                    'created_by'=>$request->user()->id,
                  ]);
                    $url = route('anggota-data-transaksi-ppob');
                    $nominal = number_format($harga->jual, 0, ",", ".");
                    $params['code'] = '200';
                    $params['messages'] = "<table class='table' id='table'>
                    <tr>
                      <td>Produk</td><td>: $harga->description </td>
                    </tr>
                    <tr>
                      <td>Nominal</td><td>: Rp $nominal </td>
                    </tr>
                    <tr>
                      <td>Nomor Pelanggan</td><td>: $request->nomorhp </td>
                    </tr>
                    <tr>
                      <td>Status</td><td>: Proses </td>
                    </tr>
                    <tr>
                      <td colspan='2'> Mohon di tunggu ...</td>
                    </tr>
                    <tr>
                      <td align='center' colspan='2'> <a href='$url'><button class='btn btn-primary'>Data Transaksi</button></a> </td>
                    </tr>
                  </table>";
                    return $params;
                // flash()->overlay($harga->description.' '.$request->nomorhp.' Mohon di tunggu', 'Berhasil');
                  // return redirect()->route('data-transaksi-ppob');
                } else {
                    $hapus = Datatransaksippobs::where('no_trx', $trxid)->first();
                    $hapus->delete();
                    $users = User::find($request->user()->id);
                    $users->saldotransaksi = $users->saldotransaksi + $harga->jual;
                    $users->update();
                    $params['code'] = '400';
                    $params['messages'] = "<p class='alert alert-danger'>Transaksi Gagal, Silahkan di ulangi kembali</p>";
                    return $params;
                }
            } else {
                $params['code'] = '400';
                $params['messages'] = "<p class='alert alert-danger'>Gagalmenampilkan data</p>";
                return $params;
            }
        }
        $params['code'] = '400';
        $params['messages'] = "<p class='alert alert-danger'>SALDO anda Tidak Cukup</p>";
        return $params;
    }
    public function datappob(Request $request)
    {
        $dashboard = 'transaksi';
        $datas = Datatransaksippobs::where('no_anggota', $request->user()->no_anggota)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('anggota.dataPpob', compact('dashboard', 'datas'));
    }
    public function databelanja(Request $request)
    {
        $dashboard = 'transaksi';
        $stts = $request->status;
        $today = date('Y-m-d');
        $from = $today;
        $until= $today;
        if ($request->from && $request->until) {
          $from = date('Y-m-d', strtotime($request->from));
          $until= date('Y-m-d', strtotime($request->until));
        }
        $datas = Pesanan::whereBetWeen('tanggal',[$from,$until])->where('status','LIKE','%'.$request->status.'%')->where('user_id',$request->user()->id)->orderBy('id','DESC')->get();
        $from = date('d-m-Y', strtotime($from));
        $until= date('d-m-Y', strtotime($until));
        $statuses = Status::whereBetween('id',[2,5])->get();
        return view('anggota.laporan_belanja', compact('dashboard', 'datas','from','until','stts','statuses'));
    }
    public function bayar(Request $request)
    {
        $dashboard = 'transaksi';
        if ($request->ids =="" || $request->berat =="" || $request->ongkir =="" || $request->jumlah =="" || $request->province =="" || $request->city =="" || $request->kirim =="" || $request->alamat =="") {
            flash()->overlay("<p class='alert alert-danger'>Data Harus Lengkap.</p>", "INFO");
            return redirect()->back();
        }
        if ($request->setuju =="") {
            flash()->overlay("<p class='alert alert-danger'>Anda belum Setujui debet saldo</p>", "INFO");
            return redirect()->back();
        }
        $kode = Toko::find($request->ids);
        $no_pesanan = date('ymdHis').$request->user()->id;
        $totalharga = (int)($kode->harga * $request->jumlah) + $request->ongkir;
        if ($request->user()->saldotransaksi < $totalharga) {
          $isisaldo = route('topup-deposit-anggota');
          flash()->overlay("<p class='alert alert-danger'>Saldo tidak cukup untuk transaksi.</p> <br><br> <a href=$isisaldo class='btn btn-primary'>Isi Saldo</a>", "INFO");
          return redirect()->back();
        }
        $saldo_akhir = $request->user()->saldotransaksi - $totalharga;
        $status = 2;
        $sisa = $kode->stok - $request->jumlah;
        if ($sisa < 0) {
          flash()->overlay("<p class='alert alert-danger'>Maaf Barang tersisa $kode->stok </p>", "INFO");
          return redirect()->back();
        }
        DB::beginTransaction();
        try {
            Toko::find($request->ids)->update([
              'stok'=>$sisa
            ]);
            User::find($request->user()->id)->update([
              'saldotransaksi'=>$saldo_akhir
            ]);
            Bukusaldotransaksi::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>date('ymdhis'),
              'tgl_trx'=>date('Y-m-d'),
              'nominal'=>$totalharga,
              'mutasi'=>'Debet',
              'keterangan'=>'Bayar '.$kode->kode.', no transaksi : '.$no_pesanan,
              'saldo'=>$saldo_akhir,
              'aktif'=>1,
            ]);
            Pesanan::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=> $request->user()->no_anggota,
              'no_pemesanan'=> $no_pesanan,
              'tanggal'=> date("Y-m-d"),
              'kode'=>$kode->kode,
              'toko_id'=>$request->ids,
              'total_harga'=>$totalharga,
              'modal'=>$kode->modal,
              'berat'=>$request->berat,
              'ongkir'=>$request->ongkir,
              'jumlah'=>$request->jumlah,
              'propinsi'=>$request->province,
              'city'=>$request->city,
              'melalui'=>$request->kirim,
              'alamat'=>$request->alamat.', '.$request->city.', '.$request->province,
              'status'=>$status,
              'aktif'=>1,
              'created_by'=>$request->user()->id
            ]);
        } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            flash()->overlay("<p class='alert alert-danger'>Gagal Transaksi.</p>", "INFO");
            return redirect()->back();
        }
        DB::commit();
        flash()->overlay("<p class='alert alert-success'>Transaksi berhasil.</p>", "INFO");
        return redirect()->route('anggota-data-transaksi-belanja');
    }
    public function datatopup(Request $request)
    {
        $dashboard = 'deposit';
        $from = date('d-m-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $status = $request->status;
        if ($request->action =='cari') {
            $from = $request->from;
            $to = $request->to;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        }
        $datas = Deposit::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_anggota', $request->user()->no_anggota)->where('status', 'LIKE', '%'.$status.'%')->orderBy('id', 'DESC')->get();
        return view('anggota.dataTopup', compact('dashboard', 'datas', 'from', 'to', 'status'));
    }
    public function cekstatus(Request $request)
    {
        $web = Website::find(1);
        $cek = Datatransaksippobs::where('id', $request->ids)->where('user_id', $request->user()->id)->first();
        if ($cek->status !== 'Proses') {
            flash()->overlay("<p class='alert alert-danger'>Hanya status proses </p>", "INFO");
            return redirect()->route('anggota-data-transaksi-ppob');
        }
        $data = array("no_trx" => $cek->no_trx,"status" => $cek->status);
        $data_string = json_encode($data);
        $ch = curl_init($web->url.'cek-status');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
        'Authorization :Bearer '.$web->token,
        'Accept : application/json',
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

        $result = curl_exec($ch);
        $hasil = json_decode($result, true);
        $status = '';
        if ($hasil['status'] == 4) {
            $status = 'Berhasil';
            Datatransaksippobs::where('no_trx', $hasil['trxid_api'])->first()->update([
        'status'=>$status,
        'sn'=>$hasil['sn']
      ]);
            flash()->overlay("<p class='alert alert-success'>Transaksi $status </p>", "INFO");
        } elseif ($hasil['status'] == 1) {
            flash()->overlay("<p class='alert alert-danger'>Transaksi masih dalam proses</p>", "INFO");
        } elseif ($hasil['status'] == 2) {
            flash()->overlay("<p class='alert alert-danger'>Transaksi gagal dan saldo telah di kembalikan</p>", "INFO");
        } elseif ($hasil['status'] == 3) {
            flash()->overlay("<p class='alert alert-danger'>Transaksi gagal dan saldo telah di refund</p>", "INFO");
        }
        return redirect()->route('anggota-data-transaksi-ppob');
    }
    public function tagihan(Request $request)
    {
        $dashboard = 'transaksi';
        $menus = Menutransaksi::where('aktif', 1)->orderBy('id', 'ASC')->get();
        if ($request->ajax()) {
            if ($request['nomor']) {
                if ($request['nomor'] =="") {
                    $params['code'] = '400';
                    $params['messages'] = "<p class='alert alert-danger'>Isi form dengan lengkap</p>";
                    return $params;
                }
                $trxid = date('YmdHis').$request->user()->id;
                $params['trxid']=$trxid;
                $params['meteran']=$request['nomor'];
                $access_token = Website::find(1);
                $url = $access_token->url.'cek-pln-pascabayar';
                $client = new \GuzzleHttp\Client();
                $res = $client->request('POST', $url, [
                            'headers'=>[
                                'Accept' => 'application/json',
                                'Content-Type' => 'application/json',
                                'Authorization' => 'Bearer '.$access_token->token
                            ],
                                  'body'=>json_encode($params)
                        ]);
                if ($res->getStatusCode() == 200) {
                    // return 'OK';
                    $hasil =  json_decode($res->getBody()->getContents(), true);
                    // return $hasil['meteran'];
                    if ($hasil['result'] =='success') {
                        $meteran = $hasil['meteran'];
                        $tagihan = $hasil['tagihan'];
                        $periode = $hasil['periode'];
                        $atas_nama = $hasil['atas_nama'];

                        $param['code'] = '200';
                        $param['messages'] = "<table class='table'>
                        <tr>
                          <td>Transaksi</td><td>:</td><td> PLN PASCABAYAR </td>
                        </tr>
                          <tr>
                            <td>ID Pelanggan</td><td>:</td><td> $meteran </td>
                          </tr>
                          <tr>
                            <td>Tagihan</td><td>:</td><td> $tagihan </td>
                          </tr>
                          <tr>
                            <td>Periode</td><td>:</td><td> $periode </td>
                          </tr>
                          <tr>
                            <td>Atas Nama</td><td>:</td><td> $atas_nama </td>
                          </tr>
                          <tr>
                            <td colspan='3' align='center'><br><a href=''><button class='btn'>History Pulsa</button></a></td>
                          </tr>
                      </table>";
                        return $param;
                    } else {
                        $param['code'] = '400';
                        $param['messages'] = "<p class='alert alert-danger'>". $hasil['message'] ."</p>";
                        return $param;
                    }
                }
            }
        }
        return view('anggota.pln_pascabayar', compact('dashboard', 'menus'));
    }
    public function jumlah(Request $request)
    {
        if ($request->ajax()) {
            Log::info('Jumlah = '.$request->jumlah);
            $hargas = Toko::find($request->ids);
            $total_harga = $hargas->harga * $request->jumlah;
            Log::info('Data harga : '.$hargas->harga);
            $label ="";
            $label.="<input type='hidden' name='total_harga' value='".$total_harga."'/>";
            $label.="<label>Rp. ".number_format($hargas->harga * $request->jumlah).",-</label>";
            Log::info('TOTAL harga : '.$total_harga);
            return $label;
        }
    }
    public function province(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->province);
            $citises = City::where('province_id', $request->province)->get();
            Log::info('$detail = '.$citises);
            $option = "";
            foreach ($citises as $key => $citys) {
                if ($key==0) {
                    $option.="<option value=''>Pilih Kota</option>";
                } else {
                    $option.="<option value='".$citys->city_id."'>".$citys->city_name."</option>";
                }
            }
            return $option;
        }
    }
    public function city(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->city);
            $hargas = City::where('city_id', $request->city)->first();
            Log::info('alamat = '.$hargas);
            $total = $request['berat'] * $request['jumlah'];
            $kg = $total/1000;
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=jne",
              CURLOPT_HTTPHEADER => array(
                "content-type: application/x-www-form-urlencoded",
                "key:da08d414a043e0001fd33b5b3ad29ad0"
              ),
            ));
            $response = curl_exec($curl);
            Log::info('$response = '.$response);
            $resarr =json_decode($response, true);
            $jne =json_decode($response, true);
            $label ="";
            $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
            $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
            $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
            $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
            $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
            $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
            $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
            $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
            return $label;
        }
    }

    public function kirim(Request $request)
    {
        if ($request->ajax()) {
            Log::info('ID province = '.$request->city);
            $hargas = City::where('city_id', $request->city)->first();
            Log::info('alamat = '.$hargas);
            $total = $request['berat'] * $request['jumlah'];
            $kg = $total/1000;
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "origin=106&destination=".$hargas->city_id."&weight=".$total."&courier=".$request['kirim'],
                CURLOPT_HTTPHEADER => array(
                  "content-type: application/x-www-form-urlencoded",
                  "key:da08d414a043e0001fd33b5b3ad29ad0"
                ),
              ));
            $response = curl_exec($curl);
            Log::info('$response = '.$response);
            $resarr =json_decode($response, true);
            $jne =json_decode($response, true);
            $label ="";
            $label.="<input type='hidden' name='province' value='".$resarr['rajaongkir']['destination_details']['province']."'/>";
            $label.="<input type='hidden' name='city' value='".$resarr['rajaongkir']['destination_details']['city_name']."'/>";
            $label.="<input type='hidden' name='total_berat' value='".$total."'/>";
            $label.="<input type='hidden' name='ongkir' value='".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value']."'/>";
            $label.= "<p>".$resarr['rajaongkir']['results'][0]['name']."</p>";
            $label.= "<p>Total Berat : ".$total." Gram / ".$kg." Kg</p>";
            $label.= "<p>Ongkos Kirim Rp. ".number_format($resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['value'])."</p>";
            $label.= "<p>Estimasi : ".$resarr['rajaongkir']['results'][0]['costs'][0]['cost'][0]['etd']." Hari </p>";
            return $label;
        }
    }

    public function bayardgnsaldo(Request $request)
    {
      // $param['code'] = 400;
      // $param['messages'] = "<p class='alert alert-danger'>Maaf, Belum  dapat berfungsi</p>";
      // return $param;

        $produks = Pesanan::where('no_pemesanan',$request->no_pesanan)->where('no_anggota',$request->user()->no_anggota)->where('status',1)->first();
        if ($request->user()->saldotransaksi < $produks->total_harga) {
          $param['code'] = 400;
          $param['messages'] = "<p class='alert alert-danger'>Maaf, Saldo anda kurang</p>";
          return $param;
        }else {
          $saldo_akhir = $request->user()->saldotransaksi - $produks->total_harga;
          DB::beginTransaction();
          try {
            $bayar = User::find($request->user()->id)->update([
              'saldotransaksi'=>$saldo_akhir
            ]);
            $debet = Bukusaldotransaksi::create([
              'user_id'=>$request->user()->id,
              'no_anggota'=>$request->user()->no_anggota,
              'no_trx'=>date('ymdhis'),
              'tgl_trx'=>date('Y-m-d'),
              'nominal'=>$produks->total_harga,
              'mutasi'=>'Kredit',
              'keterangan'=>'Bayar '.$produks->tokoId->kode.', no pesanan : '.$produks->no_pemesanan,
              'saldo'=>$saldo_akhir,
              'aktif'=>1,
            ]);
            $status = Pesanan::find($produks->id)->update([
              'status'=>2
            ]);
          } catch (\Exception $e) {
            Log::info('Gagal Transaksi:'.$e->getMessage());
            DB::rollback();
            $param['code'] = 400;
            $param['messages'] = "<p class='alert alert-danger'> $e </p>";
            return $param;
          }
          DB::commit();
          $param['code'] = 200;
          $param['messages'] = "<p class='alert alert-success'>Transaksi Berhasil</p>";
          return $param;
        }
    }
}
