<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Simpananadmin;
use App\User;
use Auth;
use App\Kelurahan;
use App\Kecamatan;
use App\Kabupaten;
use App\Propinsi;
use App\Akumulasi;
use Storage;
use Artisan;
use Image;
use Response;
use App\Datatransaksippobs;
use App\Bukusaldotransaksi;
use DB;
use Log;
use PDF;

class DataDiriController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function profil(Request $request)
    {
        $dashboard ="dataSaya";
        if ($request->fotodiri) {
            $this->validate($request, [
            'fotodiri' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);
            // $diri = date('YmdHis').'.'.$request->fotodiri->getClientOriginalExtension();
        }

        if ($request->tgl_lahir =="") {
            $tanggal_lahir = "";
        } else {
            $tanggal_lahir = date('Y-m-d', strtotime($request->tgl_lahir));
        }
        if ($request->action =='edit') {
            DB::beginTransaction();
            try {
                $user = User::find($request->user()->id);
                if ($request->file('fotodiri')) {
                    // return 'OK';
                    if ($user->fotodiri =='null' || $user->fotodiri =='') {
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');
                        // return $directory;
                        $imageUrl = $directory.$fileName;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    } else {
                        $destination_foto =public_path('foto/'.$user->fotodiri);
                        if (file_exists($destination_foto)) {
                            unlink($destination_foto);
                        }
                        $image = $request->file('fotodiri');
                        $imageName = $image->getClientOriginalName();
                        $fileName = date('YmdHis')."_".$imageName;
                        $directory = public_path('/foto/');

                        $imageUrl = $directory.$fileName;
                        // return $imageUrl;
                        Image::make($image)->resize(400, 500)->save($imageUrl);
                        $user->fotodiri = $fileName;
                    }
                }
                $user->tpt_lahir=$request->tpt_lahir;
                $user->tgl_lahir=$tanggal_lahir;
                $user->npwp=$request->npwp;
                $user->email=$request->email;
                $user->agama=$request->agama;
                $user->pendidikan=$request->pendidikan;
                $user->statuskeluarga=$request->statuskeluarga;
                $user->nama_ibu=$request->nama_ibu;
                $user->nama_ayah=$request->nama_ayah;
                $user->telp=$request->telp;
                $user->jenkel=$request->jenkel;
                $user->alamat=$request->alamat;
                $user->kelurahan=$request->kelurahan;
                $user->kecamatan=$request->kecamatan;
                $user->kabupaten=$request->kabupaten;
                $user->komunitas=$request->komunitas;
                $user->propinsi=$request->propinsi;
                $user->pendapatan=$request->pendapatan;
                $user->pekerjaan=$request->pekerjaan;
                $user->update();
            } catch (\Exception $e) {
                Log::info('Gagal Edit Profil:'.$e->getMessage());
                DB::rollback();
                flash()->overlay('Gagal Edit Profil.', 'INFO');
                return redirect()->back();
            }
            DB::commit();
            flash()->overlay('Data Profil berhasil di Edit.', 'INFO');
            return redirect()->back();
        }
        return view('administrator.datadiri.profil', compact('dashboard'));
    }
    public function simpanananggota(Request $request)
    {
        $dashboard ="dataSaya";
        $nomor = $request->user()->no_anggota;
        $jenissim = $request->jenis_simpanan;
        $from = $request->dari;
        $to = $request->sampai;
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $simpanan = Simpananadmin::where('no_anggota', $nomor)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 'LIKE', '%'.$jenissim.'%')->where('aktif', 1)->get();
            return view('administrator.datadiri.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to'));
        }
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        $simpanan = Simpananadmin::whereBetWeen('tgl_setor', [$dari,$ke])->where('no_anggota', $request->user()->no_anggota)->where('aktif', 1)->get();
        return view('administrator.datadiri.simpanan', compact('dashboard', 'nomor', 'simpanan', 'jenissim', 'from', 'to'));
    }
    public function bukusaldo(Request $request)
    {
        $dashboard ="deposit";
        $anggota = $request->user()->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        if ($request->action =='cari') {
            $from =$request->dari;
            $until =$request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
        }
        $users = User::where('no_anggota', $anggota)->first();
        // $image = $users->fotodiri;
        // $destination_foto =public_path('foto/'.$users->fotodiri);
        // if(file_exists($destination_foto)){
        //             unlink($destination_foto);
        // }
        $simpananpokok = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 1)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwajib = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 2)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpanansukarela = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 3)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinvestasi = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 4)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananwakaf = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 5)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananinfaq = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 6)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananshu = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 7)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $simpananlain = Simpananadmin::where('no_anggota', $anggota)->whereBetWeen('tgl_setor', [$dari,$ke])->where('jenis_simpanan', 8)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        return view('administrator.datadiri.bukusaldo', compact('dashboard', 'simpananpokok', 'simpananwajib', 'simpanansukarela', 'simpananinvestasi', 'simpananwakaf', 'simpananinfaq', 'simpananshu', 'simpananlain', 'anggota', 'from', 'until', 'users'));
    }

    public function password()
    {
        $dashboard ="dataSaya";
        return view('administrator.datadiri.gantiPassword', compact('dashboard'));
    }
    public function gantipassword(Request $request)
    {
        // Log::info(' ALERt :'.$request);
        $this->validate($request, [
          'password_br' => 'required|min:6'
    ]);
        if (\Hash::check($request->input('password_lm'), $request->user()->password)) {
            $user = User::find($request->user()->id);
            $user->password = bcrypt($request->input('password_br'));
            $user->update();
            flash()->overlay('Password Berhasil di rubah.', 'INFO');
            return redirect()->back();
        } else {
            flash()->overlay('Password Gagal di rubah<br>Password Lama Salah', 'INFO');
            return redirect()->back();
        }
    }
    public function kartu()
    {
        $dashboard ="dataSaya";
        return view('administrator.datadiri.kartuAnggota', compact('dashboard'));
    }
    public function download($id)
    {
        $user = User::where('sequence', $id)->first();
        $pdf = PDF::loadView('administrator.report.pdf', compact('user'));
        return $pdf->download('kartu.pdf');
    }
    public function akumulasi(Request $request)
    {
        $dashboard ="dataSaya";
        $from = date('01-01-Y');
        $to = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($to));
        if ($request->action =='cari') {
            $from = $request->dari;
            $to = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($to));
        }
        $akumulasis = Akumulasi::whereBetWeen('tgl_trx', [$dari,$ke])->where('aktif', 1)->where('no_kssd', $request->user()->no_anggota)->get();
        return view('administrator.datadiri.akumulasi', compact('dashboard', 'akumulasis', 'from', 'to'));
    }
    public function saldotransaksi(Request $request)
    {
        $dashboard = 'deposit';
        $nomor = Auth::user()->no_anggota;
        $from = date('01-01-Y');
        $until = date('d-m-Y');
        $dari = date('Y-m-d', strtotime($from));
        $ke = date('Y-m-d', strtotime($until));
        if ($request->dari && $request->sampai) {
            $from = $request->dari;
            $until = $request->sampai;
            $dari = date('Y-m-d', strtotime($from));
            $ke = date('Y-m-d', strtotime($until));
        }
        $datasppob = Bukusaldotransaksi::whereBetWeen('tgl_trx', [$dari,$ke])->where('no_anggota', $nomor)->where('aktif', 1)->orderBy('created_at', 'ASC')->get();
        $users = User::find($request->user()->id);
        return view('administrator.datadiri.bukusaldoTransaksi', compact('dashboard', 'from', 'until', 'users', 'datasppob'));
    }
    public function foto($id)
    {
        $user = User::find($id);
        $filepath = public_path('/foto/').$user->fotodiri;
        return Response::download($filepath, $user->no_anggota.".jpg");
    }
}
