<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PengaturanController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }
    public function index(Request $request){
      $dashboard ="pengaturan";
      return view('administrator.pengaturan.index',compact('dashboard'));
    }
}
