@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Profil</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#edit_profil"><i class="fa fa-edit"></i> Edit Profil</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="card-box m-b-0">
<div class="row">
  <div class="col-md-12">
    <div class="profile-view">
      <div class="profile-img-wrap">
        <div class="profile-img">
          <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.Auth::user()->fotodiri) }}" alt=""></a>
        </div>
      </div>
      <div class="profile-basic">
        <div class="row">
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>Nomor Anggota</td>
                  <td>: {{ Auth::user()->no_anggota }}</td>
                </tr>
                <tr>
                  <td>Nomor KS212</td>
                  <td>: {{ Auth::user()->no_ks212 }}</td>
                </tr>
                <tr>
                  <td>Nama Lengkap</td>
                  <td>: {{ Auth::user()->name }}</td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>: <a href="">{{ Auth::user()->email }}</a></td>
                </tr>
                <tr>
                  <td>Nomor Telpon</td>
                  <td>: {{ Auth::user()->telp }}</td>
                </tr>
                <tr>
                  <td>No.NPWP</td>
                  <td>: {{ Auth::user()->npwp }}</td>
                </tr>
                <tr>
                  <td>Pendapatan </td>
                  <td>: @if(Auth::user()->pendapatan > 0){{ Auth::user()->pendapatan_id->name }} @else @endif </td>
                </tr>
                <tr>
                  <td>Tempat Lahir</td>
                  <td>: {{ Auth::user()->tpt_lahir }}</td>
                </tr>
                <tr>
                  <td>Tanggal Lahir</td>
                  <td>: @if(Auth::user()->tgl_lahir =='') @else{{ date('d-m-Y', strtotime(Auth::user()->tgl_lahir)) }}@endif</td>
                </tr>
                <tr>
                  <td>Pekerjaan</td>
                  <td>: @if(Auth::user()->pekerjaan > 0){{ Auth::user()->pekerjaan_id->name }} @else @endif </td>
                </tr>
                <tr>
                  <td>Nama Ibu</td>
                  <td>: {{ Auth::user()->nama_ibu }}</td>
                </tr>
                <tr>
                  <td>Nama Ayah</td>
                  <td>: {{ Auth::user()->nama_ayah }}</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table">


                <tr>
                  <td>Jenis Kelamin</td>
                  <td>: {{ Auth::user()->jenkel }}</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>: {{ Auth::user()->agama }}</td>
                </tr>
                <tr>
                  <td>Pendidikan</td>
                  <td>: @if(Auth::user()->pendidikan > 0){{ Auth::user()->pendidikan_id->name }} @else  @endif</td>
                </tr>
                <tr>
                  <td>Status dalam keluarga</td>
                  <td>: {{ Auth::user()->statuskeluarga }}</td>
                </tr>
                <tr>
                  <td>Komunitas</td>
                  <td>: @if(Auth::user()->komunitas > 0){{ Auth::user()->komunitas_id->name }} @else  @endif</td>
                </tr>
                <tr>
                  <td>Alamat</td>
                  <td>: {{ Auth::user()->alamat }}</td>
                </tr>
                <tr>
                  <td>Kelurahan</td>
                  <td>: @if(Auth::user()->kelurahan > 0){{ Auth::user()->kelurahan_id->name }} @else  @endif</td>
                </tr>
                <tr>
                  <td>Kecamatan </td>
                  <td>: @if(Auth::user()->kecamatan > 0){{ Auth::user()->kecamatan_id->name }} @else  @endif</td>
                </tr>
                <tr>
                  <td>Kabupaten/Kota</td>
                  <td>: @if(Auth::user()->kabupaten > 0){{ Auth::user()->kabupaten_id->name }} @else  @endif</td>
                </tr>
                <tr>
                  <td>Propinsi </td>
                  <td>: @if(Auth::user()->propinsi > 0){{ Auth::user()->propinsi_id->name }} @else @endif </td>
                </tr>
                <tr>
                  <td><strong>SALDO SIMPANAN</strong> </td>
                  <td><strong>: Rp. {{ number_format(Auth::user()->saldo) }},-</strong></td>
                </tr>
                <tr>
                  <td><strong>SALDO TRANSAKSI</strong> </td>
                  <td><strong>: Rp. {{ number_format(Auth::user()->saldotransaksi) }},-</strong></td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="card-box tab-box">
<!-- <div class="row user-tabs">
  <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
    <ul class="nav nav-tabs tabs nav-tabs-bottom"> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#myprojects">My Detail</a></li> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#tasks">Riwayat</a></li>
    </ul>
  </div>
</div> -->
</div>
<div id="tambah_kelurahan" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Kelurahan</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/anggota/data-kelurahan')}}" method="post">
            @csrf
          <div class="form-group">
            <label>Nama Kelurahan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="kelurahan">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="tambah_kecamatan" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Kecamatan</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/anggota/data-kecamatan')}}" method="post">
            @csrf
          <div class="form-group">
            <label>Nama Kecamatan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="kecamatan">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="tambah_kabupaten" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Kabupaten</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/anggota/data-kabupaten')}}" method="post">
            @csrf
          <div class="form-group">
            <label>Nama Kabupaten <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="kabupaten">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="tambah_propinsi" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Propinsi</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/anggota/data-propinsi')}}" method="post">
            @csrf
          <div class="form-group">
            <label>Nama Propinsi <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="propinsi">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="edit_profil" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Edit Profil</h4>
      </div>
      <div class="modal-body">
        <form class="m-b-30" action="{{url('/anggota/profil')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="action" value="edit">
          <div class="row">
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">No.Anggota<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="no_anggota" value="{{Auth::user()->no_anggota}}" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                <input class="form-control" type="text" name="name" value="{{Auth::user()->name}}" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">No. KS212</label>
                <input class="form-control" type="text" name="no_ks212" value="{{Auth::user()->no_ks212}}" disabled>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">No. KTP</label>
                <input class="form-control" type="text" name="nik" value="{{Auth::user()->nik}}" />
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Email </label>
                <input class="form-control" type="email" name="email" value="{{Auth::user()->email}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nomor Telpon </label>
                <input class="form-control" type="text" name="telp" value="{{Auth::user()->telp}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Tempat Lahir</label>
                <input class="form-control" type="text" name="tpt_lahir" value="{{Auth::user()->tpt_lahir}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Tanggal Lahir </label>
                <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{date('d-m-Y', strtotime(Auth::user()->tgl_lahir))}}"></div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">NPWP</label>
                <input class="form-control" type="text" name="npwp" value="{{Auth::user()->npwp}}">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Agama </label>
                <select class="select" name="agama">
                  <option value="islam">Islam</option>
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pendidikan Terakhir </label>
                <select class="select" name="pendidikan">
                  <?php $pendidikans = App\Pendidikan::where('aktif',1)->get();?>
                  <option value="">Pilih Pendidikan Terakhir</option>
                  @foreach($pendidikans as $pendidikan)
                  @if(Auth::user()->pendidikan == $pendidikan->id)
                  <option value="{{$pendidikan->id}}" selected>{{$pendidikan->name}}</option>
                  @else
                  <option value="{{$pendidikan->id}}">{{$pendidikan->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Status Dalam Keluarga</label>
                <input type="text" class="form-control" name="statuskeluarga" value="{{Auth::user()->statuskeluarga}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nama Ibu</label>
                <input type="text" class="form-control" name="nama_ibu" value="{{Auth::user()->nama_ibu}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Nama Ayah</label>
                <input type="text" class="form-control" name="nama_ayah" value="{{Auth::user()->nama_ayah}}">
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Jenis Kelamin </label>
                <select class="select" name="jenkel">
                  <?php $jenkel = ['Laki-laki','Perempuan'] ;?>
                  <option value="">Pilih Jenis Kelamin</option>
                  @foreach($jenkel as $jen)
                  @if(Auth::user()->jenkel == $jen)
                  <option value="{{$jen}}" selected>{{$jen}}</option>
                  @else
                  <option value="{{$jen}}">{{$jen}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Alamat Anggota </label>
                <input class="form-control" type="text" name="alamat" value="{{Auth::user()->alamat}}">
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Kelurahan <!--<a href="#" data-toggle="modal" data-target="#tambah_kelurahan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a>--> </label>
                <select class="select" name="kelurahan">
                  <option value="">Pilih Kelurahan</option>
                  <?php $kelurahans = App\Kelurahan::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($kelurahans as $kelurah)
                  @if($kelurah->id==Auth::user()->kelurahan)
                  <option value="{{$kelurah->id}}" selected>{{$kelurah->name}}</option>
                  @else
                  <option value="{{$kelurah->id}}">{{$kelurah->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Kecamatan <!--<a href="#" data-toggle="modal" data-target="#tambah_kecamatan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> --></label>
                <select class="select" name="kecamatan">
                  <option value="">Pilih Kecamatan</option>
                  <?php $kecamatans = App\Kecamatan::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($kecamatans as $kecama)
                  @if($kecama->id==Auth::user()->kecamatan)
                  <option value="{{$kecama->id}}" selected>{{$kecama->name}}</option>
                  @else
                  <option value="{{$kecama->id}}">{{$kecama->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Kabupaten <!--<a href="#" data-toggle="modal" data-target="#tambah_kabupaten"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> --></label>
                <select class="select" name="kabupaten">
                  <option value="">Pilih Kabupaten</option>
                  <?php $kabupatens = App\Kabupaten::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($kabupatens as $kabu)
                  @if($kabu->id==Auth::user()->kabupaten)
                  <option value="{{$kabu->id}}" selected>{{$kabu->name}}</option>
                  @else
                  <option value="{{$kabu->id}}">{{$kabu->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Propinsi <!--<a href="#" data-toggle="modal" data-target="#tambah_propinsi"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> --></label>
                <select class="select" name="propinsi">
                  <option value="">Pilih Propinsi</option>
                  <?php $propinsi = App\Propinsi::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($propinsi as $pro)
                  @if($pro->id==Auth::user()->propinsi)
                  <option value="{{$pro->id}}" selected>{{$pro->name}}</option>
                  @else
                  <option value="{{$pro->id}}">{{$pro->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Komunitas </label>
                <select class="select" name="komunitas">
                  <option value="">Pilih Komunitas</option>
                  <?php $komunitas = App\Komunitas::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($komunitas as $komu)
                  @if($komu->id==Auth::user()->komunitas)
                  <option value="{{$komu->id}}" selected>{{$komu->name}}</option>
                  @else
                  <option value="{{$komu->id}}">{{$komu->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pendapatan </label>
                <select class="select" name="pendapatan">
                  <option value="">Pilih Pendapatan</option>
                  <?php $pendapatan = App\Pendapatan::where('aktif',1)->get(); ?>
                  @foreach($pendapatan as $pend)
                  @if($pend->id == Auth::user()->pendapatan)
                  <option value="{{$pend->id}}" selected>{{$pend->name}}</option>
                  @else
                  <option value="{{$pend->id}}">{{$pend->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Pekerjaan </label>
                <select class="select" name="pekerjaan">
                  <option value="">Pilih Pekerjaan</option>
                  <?php $pekerjaan = App\Pekerjaan::where('aktif',1)->orderBy('name','ASC')->get(); ?>
                  @foreach($pekerjaan as $pek)
                  @if($pek->id == Auth::user()->pekerjaan)
                  <option value="{{$pek->id}}" selected>{{$pek->name}}</option>
                  @else
                  <option value="{{$pek->id}}">{{$pek->name}}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label class="control-label">Foto Diri 400x500 Pixels<span class="text-danger">*</span></label>
                <input class="form-control" type="file" name="fotodiri">
              </div>
            </div>
          </div>

          <div class="m-t-20 text-center">
            <button class="btn btn-primary">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  </div>
</div>


</div>
</div>
@endsection
