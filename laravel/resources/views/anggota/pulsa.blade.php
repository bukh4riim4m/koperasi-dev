@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">PULSA HP</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-md-6 col-xs-12" id="respon">
          <div class="" id="message"></div>
        </div>
        <script>
          $("#respon").hide();
        </script>
        <div class="col-sm-8 col-md-6 col-xs-12" id="form">
          <div class="" id="errors"></div>
          <script>
            $("#errors").hide();
          </script>
          <form class="" action="{{url('/anggota/proses-ppob')}}" method="post">
            @csrf
          <div class="form-group">
            <select class="form-control" name="provider" id="provider" required>
              <option value="">Pilih Provider</option>
              <option value="BOLT">BOLT</option>
              <option value="TELKOMSEL">TELKOMSEL</option>
              <option value="SMARTFREN">SMARTFREN</option>
              <option value="INDOSAT">INDOSAT</option>
              <option value="TRI">TRI</option>
              <option value="XL">XL</option>
              <option value="AXIS">AXIS</option>
            </select>
          </div>
          <script type="text/javascript">
            $("select[name='provider']").change(function(){
                $('.modal').modal('show');
                var provider = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('anggota-select-provider') ?>",
                    method: 'POST',
                    data: {provider:provider, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#paket").html("");
                      $("#paket").append(data);
                      $("#divharga").hide();
                      $("#buttom").hide();
                      $("#nomorhp").hide();
                      $('.modal').modal('hide');
                    }
                });
            });
          </script>
          <div class="form-group">
            <select class="form-control" name="paket" id="paket" required>
              <option value="">Pilih Nominal Pulsa</option>
            </select>
          </div>
          <script type="text/javascript">
            $("select[name='paket']").change(function(){
                $('.modal').modal('show');
                var paket = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('anggota-select-paket') ?>",
                    method: 'POST',
                    data: {paket:paket, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#harga").html("");
                      $("#harga").append(data);
                      $("#divharga").show();
                      $("#buttom").show();
                      $("#nomorhp").show();
                      $('.modal').modal('hide');
                    }
                });
            });
          </script>
          <div class="form-group" id="divharga">
            <label id="harga" class="form-control" name="harga">Rp.</label>
          </div>
          <script>
            $("#divharga").hide();
          </script>
          <div class="form-group" id="nomorhp">
            <input type="text" name="nomorhp" class="form-control" value="" placeholder="Nomor HP" required>
          </div>
          <script>
            $("#nomorhp").hide();
          </script>
          <div class="form-group" id="buttom">
            <input type="button" class="form-control btn btn-primary" id="btn-proses" onclick="modal();" name="btn" value="P R O S E S">
          </div>
          <script>
            $("#buttom").hide();
          </script>
          <script type="text/javascript">
            $("#btn-proses").on("click", function(){
              $('.modal').modal('show');
                var paket = $("#paket").val();
                var nomorhp = $("input[name='nomorhp']").val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('anggota-proses-ppob') ?>",
                    method: 'POST',
                    data: {paket:paket, _token:token,nomorhp:nomorhp},
                    success: function(data) {
                      console.log(data);
                      $('.modal').modal('hide');
                      if (data.code == '200') {
                        $("#message").html("");
                        $("#form").html("");
                        $("#form").hide();
                        $("#respon").show();
                        $("#message").html(data.messages);

                      }else {
                        $("#message").html("");
                        $("#errors").show();
                        $("#errors").html(data.messages);
                        setTimeout(function () {
                         	$('#errors').modal('hide');
                          $('#errors').html('');
                        }, 5000);
                      }
                    }
                });
            });
          </script>
        </form>
        </div>
        <div class="col-sm-8 col-md-6 col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <table width="100%" class="table table-striped custom-table">
                <tr>
                  <td>
                    <h4 class="page-title text-center"> MENU LAIN</h4>
                  </div>
                    @foreach($menus as $menu)
                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <a href="{{url('anggota/'.$menu->route)}}"><button type="button" name="button"  class="btn btn-success btn-sm m-t-10 form-control">{{$menu->menu}}</button></a>
                    </div>
                    @endforeach
                  </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <!-- MODAL LOADING -->
          <style>
          .bd-example-modal-lg .modal-dialog{
            display: table;
            position: relative;
            margin: 0 auto;
            text-align:center;
            top: calc(50% - 24px);
          }
          .bd-example-modal-lg .modal-dialog .modal-content{
            background-color: #ffffff;
            border: none;
          }
          </style>
          <div class="modal fade bd-example-modal-lg" data-backdrop="static" data-keyboard="false" tabindex="-1">
              <div class="modal-dialog modal-sm">
                  <div class="modal-content">
                      <span class="fa fa-spinner fa-spin fa-5x"></span><br><br>
                      Mohon Menunggu...
                  </div>

              </div>
          </div>
          <!-- MODAL -->
    </div>
</div>
@endsection
