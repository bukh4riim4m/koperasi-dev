@extends('layouts.anggota.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data Simpanan</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <div class="view-icons">
  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/anggota/simpanan')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Jenis Simpanan</label>
        <?php $jenis = App\JenisSimpanan::where('aktif',1)->get(); ?>
        <select class="select floating" name="jenis_simpanan">
          <option value=""> -- Semua -- </option>
          @foreach($jenis as $jen)
              <option value="{{$jen->id}}"> {{$jen->name}} </option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>Jenis Simpanan</th>
          <th>Mutasi</th>
          <th>Nominal</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $kredit = 0;
        $debet = 0;?>
        @foreach($simpanan as $us)
        <?php if ($us->mutasi =='Kredit') {
          $kredit+=$us->nominal;
        }else {
          $debet+=$us->nominal;
        } ?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$us->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($us->tgl_setor))}}</td>
          <td>{{$us->jenisSimpanan->name}}</td>
          <td>{{$us->mutasi}}</td>
          <td>Rp. {{number_format($us->nominal)}},-</td>
        </tr>
        @endforeach
        @if(count($simpanan) < 1)
        <tr>
          <td colspan="9" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Mutasi Debet</td>
    <td>: Rp. {{number_format($debet)}},-</td>
  </tr>
  <tr>
    <td>Mutasi Kredit</td>
    <td>: Rp. {{number_format($kredit)}},-</td>
  </tr>
</table>
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
