<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>KSSD-CILEGON</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="author" content="" />

  <!-- css -->
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700|Open+Sans:300,400,600,700" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet" />
  <link href="assets/css/bootstrap-responsive.css" rel="stylesheet" />
  <link href="assets/css/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="assets/css/jcarousel.css" rel="stylesheet" />
  <link href="assets/css/flexslider.css" rel="stylesheet" />
  <link href="assets/css/style.css" rel="stylesheet" />
  <!-- <link href="css/style.css" rel="stylesheet" /> -->
  <!-- Theme skin -->
  <link href="assets/skins/default.css" rel="stylesheet" />
  <!-- Fav and touch icons -->
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png" />
  <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png" />
  <link rel="shortcut icon" href="{{url('images/Logo-Syariah-Transparan.png')}}" />
  <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-datetimepicker.min.css')}}">


  <!-- =======================================================
    Theme Name: Flattern
    Theme URL: https://bootstrapmade.com/flattern-multipurpose-bootstrap-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <div id="wrapper">
    @include('flash::message')
    @include('includes.umum.header')
    <section id="inner-headline">
      <div class="container">
        <div class="row">
          <div class="span12">
            <ul class="breadcrumb">
              <li><a href="{{url("/")}}"><i class="icon-home"></i></a><i class="icon-angle-right"></i></li>
              <li class="active">{{$title}}</li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    @yield('content')
    @include('includes.umum.footer')
  </div>
  <script type="text/javascript" src="{{url('js/jquery-3.2.1.min.js')}}"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  <script>
      $('#flash-overlay-modal').modal();
  </script>
  <a href="#" class="scrollup"><i class="icon-chevron-up icon-square icon-32 active"></i></a>
  <!-- javascript
    ================================================== -->
    <script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/app.js')}}"></script>
  <!-- Placed at the end of the document so the pages load faster -->
  <script src="assets/js/jquery.js"></script>
  <script src="assets/js/jquery.easing.1.3.js"></script>
  <script src="assets/js/bootstrap.js"></script>
  <script src="assets/js/jcarousel/jquery.jcarousel.min.js"></script>
  <script src="assets/js/jquery.fancybox.pack.js"></script>
  <script src="assets/js/jquery.fancybox-media.js"></script>
  <script src="assets/js/google-code-prettify/prettify.js"></script>
  <script src="assets/js/portfolio/jquery.quicksand.js"></script>
  <script src="assets/js/portfolio/setting.js"></script>
  <script src="assets/js/jquery.flexslider.js"></script>
  <script src="assets/js/jquery.nivo.slider.js"></script>
  <script src="assets/js/modernizr.custom.js"></script>
  <script src="assets/js/jquery.ba-cond.min.js"></script>
  <script src="assets/js/jquery.slitslider.js"></script>
  <script src="assets/js/animate.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Custom JavaScript File -->
  <script src="assets/js/custom.js"></script>

</body>

</html>
