<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <?php $web = App\Website::find(1); ?>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('images/'.$web->logo)}}">
		     <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/font-awesome.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{url('css/select2.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{url('css/bootstrap-datetimepicker.min.css')}}">
    		<link rel="stylesheet" type="text/css" href="{{url('plugins/summernote/dist/summernote.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/dataTables.bootstrap.min.css')}}">
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    </head>
    <body>
        <div class="main-wrapper">
          @include('flash::message')
          <!-- HEADER -->
            @include('includes.admin.header')
      <!-- ENDHEADER -->
      <!-- SIDEBAR -->
      @include('includes.pengurus.sidebar')
      <!-- ENDSIDEBAR -->
      @yield('content')
      @include('includes.admin.footer')
