@extends('layouts.umum.pages')
@section('content')
<section id="content">
  <div class="container">
    <div class="row">
      <div class="span6">
        <p style="text-align:justify;">
          Koperasi Syari'ah Super Damai Cilegon (KSSD Cilegon) merupakan Koperasi Syari'ah yang berdiri sendiri secara otonom dengan tujuan mendirikan pusat pengembangan perekonomian ummat Muslim di wilayah Cilegon dan sekitarnya dengan melibatkan masyarakat secara keseluruhan bersama-sama yang berbasiskan Masjid.
        </p>
        <p style="text-align:justify;"><strong>Latar Belakang</strong>
          <ol style="text-align:justify;">
            <li>Masih rendahnya kontribusi kinerja umat Islam terhadap Ekonomi Indonesia, baik terhadap penguasaan aset-aset produktif maupun aset usaha (87% penduduk memperoleh kepemilikan 16% aset bisnis).</li>
            <li>Masih jauhnya perekonomian umat dari prinsip-prinsip syariah yang tercermin dalam Al-Quran dan Sunnah.</li>
            <li>Kesenjangan ekonomi antara si Kaya dan si Miskin yang terus melebar.</li>
            <li>Terpecah-pecahnya perjuangan Umat Islam, baik secara politis maupun Ekonimis</li>
            <li>Besarnya potensi pasar atau daya beli umat islam yang belum dikelola secara sistemamtis oleh pengusaha-pengusaha muslim</li>
            <li>Diperlukannya gerakan ekonomi umat islam secara berjamaah, dengan pengelolaan yang amanah dan profesional, serta tumbuh secara berkesinambungan</li>
          </ol>
        </p>
      </div>
      <div class="span6">
        <!-- start flexslider -->
        <div class="flexslider">
          <ul class="slides">
            <li>
              <img src="images/12345.jpeg" alt="" />
            </li>
            <!-- <li>
              <img src="assets/img/works/full/image-02-full.jpg" alt="" />
            </li>
            <li>
              <img src="assets/img/works/full/image-03-full.jpg" alt="" />
            </li> -->
          </ul>
        </div>
        <!-- end flexslider -->
      </div>

    </div>
    <!-- divider -->
    <div class="row">
      <div class="span12">
        <div class="solidline">
          <p style="text-align:justify;">Untuk memenuhi kebutuhan tersebut, maka KSSD Cilegon dibentuk dengan mengemban Visi, Misi dan Prinsip-prinsip sebagai berikut :</p>
          <p style="text-align:justify;"><strong>Visi</strong>   : Memperkuat Ekonomi Umat</p>
          <p style="text-align:justify;"><strong>Misi</strong>   : <ol style="text-align:justify;">
            <li>Menciptakan pengusaha-pengusaha muslim yang tangguh</li>
            <li>Memperkuat jaringan masjid</li>
            <li>Menggali potensi ekonomi umat</li>
          </ol></p>
          <p style="text-align:justify;"><strong>Prinsip - Prinsip</strong>   : <ol style="text-align:justify;">
            <li>Sesuai dengan Al-Quran dan Sunnah serta Fatwa Ulama</li>
            <li>Dikembangkan untuk mencapai kemandirian ekonomi umat islam (untuk umat dan dari umat)</li>
            <li>Usaha dimiliki secara bersama (Crowd Sourcing /Crowd Funding)</li>
            <li>Kegiatan usaha dilakukan secara bersama (Anggota dapat berperan sebagai pemodal, pengelola, produsen sekaligus ebagai konsumen).</li>
            <li>Dikelola dengan menerapkan sistem manajemen, teknologi modern dan budaya kerja yang amanah serta profesional.</li>
          </ol></p>
          <p style="text-align:justify;"><strong>Kenapa memilih bentuk koperasi ?</strong>   : <ol style="text-align:justify;">
            <li>Koperasi adalah bentuk badan usaha yang diakui oleh negara.</li>
            <li>Dapat menampung ribuan bahkan jutaan anggota dan ini menjadi wadah bagi semangat kebangkitan ekonomi umat islam paska gerakan 212.</li>
            <li>Dapat bergerak pada berbagai lini usaha, termasuk keuangan syariah dan investasi pada berbagai sektor industri serta jasa.</li>
            <li>Dimungkinkan penguatan modal dari anggota secara cepat.</li>
            <li>Adanya multilayers system of control, baik dari pengawas, pengurus dan manajemen.</li>
            <li>Adanya pertanggung jawaban secara berkala dalam RAT (Rapat Anggota Tahunan)</li>
          </ol></p>
          <p style="text-align:justify;"><strong>Prioritas Usaha</strong>   : <ol style="text-align:justify;">
            <li>Mobilisasi dana dan tabungan investasi anggota.</li>
            <li>Identifikasi potensi-potensi usaha.</li>
            <li>Membangun kerjasama kemitraan usaha yang saling menguntungkan dengan mengedepankan kepentingan bersama, melaluiskema Crowd Sourcing dan atau Crowd Funding.</li>
          </ol></p>
          <p style="text-align:justify;"><strong>Portofolio Usaha</strong>   : <ol style="text-align:justify;">
            <li>Ritel</li>
            <li>Perdagangan</li>
            <li>Keuangan Syariah</li>
            <li>Logistik/Transportasi</li>
            <li>Properti SYariah</li>
            <li>Agribisnis</li>
            <li>Energi</li>
            <li>Manufaktur</li>
            <li>Pendidikan</li>
            <li>dan lain -lain</li>
          </ol></p>
        </div>
      </div>
    </div>
    <!-- end divider -->
    <!-- <div class="row">
      <div class="span12">
        <h4>Talented peoples behind Flattern</h4>
      </div>
      <div class="span3">
        <img src="assets/img/dummies/team1.jpg" alt="" class="img-polaroid" />
        <div class="roles">
          <p class="lead">
            <strong>Vincent Austin Jr</strong>
          </p>
          <p>
            CEO - Founder
          </p>
        </div>
      </div>
      <div class="span3">
        <img src="assets/img/dummies/team2.jpg" alt="" class="img-polaroid" />
        <div class="roles">
          <p class="lead">
            <strong>Tommy Laugher</strong>
          </p>
          <p>
            Lead designer
          </p>
        </div>
      </div>
      <div class="span3">
        <img src="assets/img/dummies/team3.jpg" alt="" class="img-polaroid" />
        <div class="roles">
          <p class="lead">
            <strong>Gabirelle Borowski</strong>
          </p>
          <p>
            Customer support
          </p>
        </div>
      </div>
      <div class="span3">
        <img src="assets/img/dummies/team4.jpg" alt="" class="img-polaroid" />
        <div class="roles">
          <p class="lead">
            <strong>Benny Strongton</strong>
          </p>
          <p>
            Coffee maker
          </p>
        </div>
      </div>
    </div> -->
    <!-- divider -->
    <!-- <div class="row">
      <div class="span12">
        <div class="solidline">
        </div>
      </div>
    </div> -->
    <!-- end divider -->
    <!-- <div class="row">
      <div class="span6">
        <h4>More about us</h4>
        <div class="accordion" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
          1. What we do </a>
            </div>
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                <p>
                  Diam alienum oporteat ad vis, latine intellegebat cu his. Ei eros dicam commodo duo, an assum meliore eam. In sed albucius dissentiet. Sit laudem graece malorum ne, at eam omnesque expetenda pertinacia, tale meliore vim ea. Dolore legere deleniti ius
                  at, mea nibh discere perfecto ex. Mea ea iuvaret eripuit, eos no vivendo intellegat definiebas, patrioque eloquentiam eos et.
                </p>
              </div>
            </div>
          </div>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
          2. Work process </a>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>
                  Nihil suscipit posidonium eos id. An cetero fierent insolens mel, ex sit rebum falli erroribus. Ius in nemore dolorum officiis. Et vel harum dicant, vix eius persius an. Ex eam malis postea, erat nihil consulatu nam ea. Ex quem dolores euripidis eum,
                  tempor aperiam voluptaria has ad. Ea est persecuti dissentiet voluptatibus, at illum malorum minimum usu eum aeterno tritani.
                </p>
              </div>
            </div>
          </div>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
          3. Quality assurance </a>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>
                  Vel purto oportere principes ne, ut mel graeco omnesque. Habeo justo congue mei cu, eu est molestie sensibus, oratio tibique ad mei. Admodum consetetur cu eam, nec cu doming prompta inciderint, ne vim ceteros mnesarchum scriptorem. Ex eam malis postea,
                  erat nihil consulatu nam ea. Ex quem dolores euripidis eum, tempor aperiam voluptaria has ad. Et vel harum dicant vix.
                </p>
              </div>
            </div>
          </div>
          <div class="accordion-group">
            <div class="accordion-heading">
              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
          4. What we can deliver </a>
            </div>
            <div id="collapseFour" class="accordion-body collapse">
              <div class="accordion-inner">
                <p>
                  Diam alienum oporteat ad vis, latine intellegebat cu his. Ei eros dicam commodo duo, an assum meliore eam. In sed albucius dissentiet. Sit laudem graece malorum ne, at eam omnesque expetenda pertinacia, tale meliore vim ea. Dolore legere deleniti ius
                  at, mea nibh discere perfecto ex. Mea ea iuvaret eripuit, eos no vivendo intellegat definiebas, patrioque eloquentiam eos et.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="span6">
        <h4>Our expertise</h4>
        <label>Web design:</label>
        <div class="progress progress-info progress-striped active">
          <div class="bar" style="width: 90%">
          </div>
        </div>
        <label>Wordpress :</label>
        <div class="progress progress-success progress-striped active">
          <div class="bar" style="width: 60%">
          </div>
        </div>
        <label>Photoshop :</label>
        <div class="progress progress-warning progress-striped active">
          <div class="bar" style="width: 80%">
          </div>
        </div>
        <label>Ilustrator :</label>
        <div class="progress progress-danger progress-striped active">
          <div class="bar" style="width: 40%">
          </div>
        </div>
      </div>
    </div> -->
  </div>
</section>
@endsection
