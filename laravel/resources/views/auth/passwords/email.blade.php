<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>LOGIN</title>
		<link rel="shortcut icon" type="image/x-icon" href="{{url('images/dpd.jpg')}}">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
        <div class="main-wrapper">
			<div class="account-page">
				<div class="container">
					<h3 class="account-title">KSSD - CILEGON</h3>
					<div class="account-box">
						<div class="account-wrapper">
							<div class="account-logo">
								<a href="{{url('/')}}"><img src="{{url('images/Logo-Syariah-Transparan.png')}}" alt="Focus Technologies"></a>
							</div>
              @if (session('status'))
                  <div class="alert alert-success">
                      {{ session('status') }}
                  </div>
              @endif
							<form method="POST" action="{{ route('password.email') }}">
                @csrf

								<div class="form-group form-focus">
									<label class="control-label">Email</label>
                  <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
								</div>
                <div class="col-md-12">
                @if ($errors->has('email'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
                </div>
								<div class="form-group text-center">
									<button class="btn btn-success btn-block" type="submit">Kirim Link Password</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
        <script type="text/javascript" src="{{url('js/jquery-3.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/app.js')}}"></script>
    </body>
</html>
