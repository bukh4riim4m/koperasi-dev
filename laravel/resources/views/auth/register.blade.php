<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
        <title>LOGIN</title>
        <?php $web = App\Website::find(1); ?>
        <link rel="shortcut icon" type="image/x-icon" href="{{url('images/'.$web->logo)}}">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/font-awesome.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
		<!--[if lt IE 9]>
			<script src="js/html5shiv.min.js"></script>
			<script src="js/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>
      <?php $web = App\Website::find(1); ?>
        <div class="main-wrapper">
			<div class="account-page">
				<div class="container">
					<h3 class="account-title">{{$web->name}}</h3>
					<div class="account-box">
						<div class="account-wrapper">
							<div class="account-logo">

								<a href="{{url('/')}}"><img src="{{url('images/'.$web->logo)}}" alt="Focus Technologies"></a>
							</div>
							<form action="{{route('login')}}" method="post">
                @csrf
								<div class="form-group form-focus">
									<label class="control-label">Username</label>
									<input class="form-control floating" type="text" name="sequence" required>
								</div>
								<div class="form-group form-focus">
									<label class="control-label">Password</label>
									<input class="form-control floating" type="password" name="password" required>
								</div>
								<div class="form-group text-center">
									<button class="btn btn-success btn-block" type="submit">Login</button>
								</div>
								<div class="text-center">
									<a href="{{url('password/reset')}}">Lupa password?</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
        </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
        <script type="text/javascript" src="{{url('js/jquery-3.2.1.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{url('js/app.js')}}"></script>
    </body>
</html>
