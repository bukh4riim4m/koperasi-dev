@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-sm-8">
          <h4 class="page-title">Detail Anggota</h4>
        </div>
        <div class="col-sm-4 text-right m-b-30">
          <!-- <a href="edit-profile.html" class="btn btn-primary rounded"><i class="fa fa-plus"></i> Edit Profile</a> -->
        </div>
      </div>
<div class="card-box m-b-0">
<div class="row">
  <div class="col-md-12">
    <div class="profile-view">
      <div class="profile-img-wrap">
        <div class="profile-img">
          <a href=""><img class="avatar" src="{{ url('/laravel/public/images/'.$datas->foto) }}" alt=""></a>
        </div>
      </div>
      <div class="profile-basic">
        <div class="row">
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table">
                <tr>
                  <td>NIP</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Nama</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Telepon</td>
                  <td>: <a href="">{{ $user->name }}</a></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Jenis Kelamin</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Jabatan</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Satuan Kerja</td>
                  <td>: {{ $user->name }}</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-md-6">
            <div class="table-responsive">
              <table class="table">


                <tr>
                  <td>Tempat Lahir</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Tanggal Lahir</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Tanggal Pensiun</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Agama</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Status</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Golongan</td>
                  <td>: {{ $user->name }}</td>
                </tr>
                <tr>
                  <td>Sisa Cuti</td>
                  <td>: {{ $user->name }} Hari</td>
                </tr>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="card-box tab-box">
<!-- <div class="row user-tabs">
  <div class="col-lg-12 col-md-12 col-sm-12 line-tabs">
    <ul class="nav nav-tabs tabs nav-tabs-bottom"> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#myprojects">My Detail</a></li> -->
      <!-- <li class="active col-sm-3"><a data-toggle="tab" href="#tasks">Riwayat</a></li>
    </ul>
  </div>
</div> -->
</div>
    <div id="tasks" class="tab-pane fade in active">
      <div class="project-task">
        <div class="tabbable">
          <ul class="nav nav-tabs nav-tabs-top nav-justified m-b-0">
            @if($active == 'pokok') <li class="active"> @else <li> @endif
              <a href="#pokok" data-toggle="tab" aria-expanded="true">Pokok</a></li>
            @if($active == 'wajib') <li class="active"> @else <li> @endif
              <a href="#wajib" data-toggle="tab" aria-expanded="false">Wajib</a></li>
              @if($active == 'sukarela') <li class="active"> @else <li> @endif
              <a href="#sukarela" data-toggle="tab" aria-expanded="false">Sukarela</a></li>
              @if($active == 'infaq') <li class="active"> @else <li> @endif
              <a href="#infaq" data-toggle="tab" aria-expanded="false">Infaq</a></li>
              @if($active == 'wakaf') <li class="active"> @else <li> @endif
              <a href="#wakaf" data-toggle="tab" aria-expanded="false">Wakaf</a>
            </li>
            @if($active == 'lain') <li class="active"> @else <li> @endif
              <a href="#lain" data-toggle="tab" aria-expanded="false">Lainnya</a>
            </li>
          </ul>
          <div class="tab-content">
            @if($active == 'pokok') <div class="tab-pane active" id="pokok"> @else <div class="tab-pane" id="pokok"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No</th>
                            <th>Tanggal Setor</th>
                            <th>Jenis Simpanan</th>
                            <th>Nominal</th>
                            <th>Fakultas</th>
                            <th>Jurusan</th>
                            <th>Tahun Lulus</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no1=1; ?>
                          @foreach($pokok as $pdd)
                          <tr>
                            <td>{{$no1++}}.</td>
                            <td>{{$pdd->jenjang}}</td>
                            <td>{{$pdd->nama_sekolah}}</td>
                            <td>{{$pdd->lokasi}}</td>
                            <td>{{$pdd->fakultas}}</td>
                            <td>{{$pdd->jurusan}}</td>
                            <td>{{$pdd->lulus_tahun}}</td>
                            <td style="min-width:100px;">
                							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_pendidikan{{$pdd->id}}">Edit</a>
                							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_pendidikan{{$pdd->id}}">Hapus</a>
                						</td>
                          </tr>
                          @endforeach
                          @if(count($pendidikan) < 1)
                          <tr>
                            <td colspan="8" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                      <div class="col-sm-12 text-right m-b-30">
                        <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_pendidikan"><i class="fa fa-plus"></i> Tambah Pendidikan</a>                      </div>

                  </div>
                </div>
                <div id="tambah_pendidikan" class="modal custom-modal fade" role="dialog">
                  <div class="modal-dialog">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="modal-content modal-lg">
                      <div class="modal-header">
                        <h4 class="modal-title">Tambah Pendidikan</h4>
                      </div>
                      <div class="modal-body">
                        <form class="m-b-30" action="{{url('/admin/tambah-detail-pendidikan/')}}" method="post">
                          @csrf
                          <input type="hidden" name="action" value="tambah">
                          <div class="row">
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Nama Sekolah<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="nama_sekolah" required>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Lokasi <span class="text-danger">*</span></label>
                                <input class="form-control" name="lokasi" type="text" required>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Jenjang<span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="jenjang" required>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Fakultas <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="fakultas" required>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Jurusan <span class="text-danger">*</span></label>
                                <input class="form-control" type="text" name="jurusan" required>
                              </div>
                            </div>
                            <div class="col-sm-6">
                              <div class="form-group">
                                <label class="control-label">Tahun Lulus <span class="text-danger">*</span></label>
                                <input class="form-control" type="number" name="lulus_tahun" required>
                              </div>
                            </div>
                          </div>
                          <div class="m-t-20 text-center">
                            <button class="btn btn-primary">Simpan Data Pendidikan</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            @foreach($pendidikan as $pdd)
            <div id="edit_pendidikan{{$pdd->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-content modal-lg">
                  <div class="modal-header">
                    <h4 class="modal-title">Edit Pendidikan</h4>
                  </div>
                  <div class="modal-body">
                    <form class="m-b-30" action="{{url('/admin/edit-detail-pendidikan/'.$datas->id)}}" method="post">
                      @csrf
                      <input type="hidden" name="action" value="edit">
                      <input type="hidden" name="ids" value="{{$pdd->id}}">
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Nama Sekolah<span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="nama_sekolah" value="{{$pdd->nama_sekolah}}" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Lokasi <span class="text-danger">*</span></label>
                            <input class="form-control" name="lokasi" value="{{$pdd->lokasi}}" type="text" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Jenjang<span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="jenjang" value="{{$pdd->jenjang}}" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Fakultas <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="fakultas" value="{{$pdd->fakultas}}" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Jurusan <span class="text-danger">*</span></label>
                            <input class="form-control" type="text" value="{{$pdd->jurusan}}" name="jurusan" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Tahun Lulus <span class="text-danger">*</span></label>
                            <input class="form-control" type="number" name="lulus_tahun" value="{{$pdd->lulus_tahun}}" required>
                          </div>
                        </div>
                      </div>
                      <div class="m-t-20 text-center">
                        <button class="btn btn-primary">Simpan Data Pendidikan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
            @foreach($pendidikan as $pdd)
            <div id="hapus_pendidikan{{$pdd->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content modal-md">
                  <div class="modal-header">
                    <h4 class="modal-title">Hapus Pendidikan {{$pdd->jenjang}}</h4>
                  </div>
                  <form action="{{url('/admin/hapus-detail-pendidikan/'.$pdd->id)}}" method="post" id="hapus_golongan">
                    @csrf
                    <input type="hidden" name="action" value="hapus">
                    <input type="hidden" name="ids" value="{{$pdd->id}}">
                    <div class="modal-body card-box">
                      <p>Apakah yakin ingin di Hapus ???</p>
                      <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            @endforeach
            @if($active == 'pangkat') <div class="tab-pane active" id="kepangkatan"> @else <div class="tab-pane" id="kepangkatan"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Golongan</th>
                            <th>No. SK</th>
                            <th>TGL. SK</th>
                            <th>TMT. SK</th>
                            <th>Keterangan</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody><?php $no2=1; ?>
                          @foreach($kepangkatan as $pkt)
                          <tr>
                            <td>{{$no2++}}.</td>
                            <td>{{$pkt->golongan->name}}</td>
                            <td>{{$pkt->no_sk}}</td>
                            <td>{{ date('d-m-Y', strtotime($pkt->tgl_sk))}}</td>
                            <td>{{ date('d-m-Y', strtotime($pkt->tmt_sk))}}</td>
                            <td>{{$pkt->ket}}</td>
                            <td style="min-width:100px;">
                							<a href="{{url('admin/edit-kepangkatan')}}" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_kepangkat{{$pkt->id}}">Edit</a>
                							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_kepangkatan{{$pkt->id}}">Hapus</a>
                						</td>
                          </tr>
                          @endforeach
                          @if(count($kepangkatan) < 1)
                          <tr>
                            <td colspan="8" class="text-center">KOSONG</td>
                          </tr>
                          @endif
                        </tbody>
                        </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_kepangkatan"><i class="fa fa-plus"></i> Tambah Kepangkatan</a>                      </div>

                </div>
              </div>
            @foreach($kepangkatan as $editpkt)
              <div id="edit_kepangkat{{$editpkt->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Edit Kepangkatan</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/edit-detail-kepangkatan/'.$editpkt->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="action" value="edit">
                        <input type="hidden" name="ids" value="{{$editpkt->id}}">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Golongan Pangkat<span class="text-danger">*</span></label>
                              <!-- <input class="form-control" type="text" name="gopal" required> -->
                              <?php $gopal = App\Golongans::get(); ?>
                              <select class="select" name="gopal" required>
                                <option value="">Pilih Golongan Pangkat {{$datas->golongan->id}}</option>
                                @foreach($gopal as $golongan)
                                  @if($editpkt->golongan_pangkat == $golongan->id)
                                    <option value="{{$golongan->id}}" selected>{{ $golongan->name }}</option>
                                  @else
                      							<option value="{{$golongan->id}}">{{ $golongan->name }}</option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">No. SK <span class="text-danger">*</span></label>
                              <input class="form-control" name="nosk" type="text" value="{{$editpkt->no_sk}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tgl.SK<span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_sk" value="{{ date('d-m-Y', strtotime($editpkt->tgl_sk))}}" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">TMT.SK <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_sk" value="{{ date('d-m-Y', strtotime($editpkt->tmt_sk))}}" required></div>
                            </div>
                          </div>
                          <!-- <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Masa Kerja<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="masa_kerja" required>
                            </div>
                          </div> -->
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Keterangan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="ket" value="{{$editpkt->ket}}" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Kepangkatan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
@endforeach

              <div id="tambah_kepangkatan" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Tambah Kepangkatan</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/tambah-kepangkatan/'.$datas->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="action" value="tambah">
                        <input type="hidden" name="route" value="tambahpangkat">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Golongan Pangkat<span class="text-danger">*</span></label>
                              <!-- <input class="form-control" type="text" name="gopal" required> -->
                              <?php $gopal = App\Golongans::get(); ?>
                              <select class="select" name="gopal" required>
                                <option value="">Pilih Golongan Pangkat</option>
                                @foreach($gopal as $golongan)
                    							<option value="{{$golongan->id}}">{{ $golongan->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">No. SK <span class="text-danger">*</span></label>
                              <input class="form-control" name="nosk" type="text" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tgl.SK<span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_sk" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">TMT.SK <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_sk" required></div>
                            </div>
                          </div>
                          <!-- <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Masa Kerja<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="masa_kerja" required>
                            </div>
                          </div> -->
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Keterangan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="ket" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Kepangkatan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          @foreach($kepangkatan as $hapuskp)
          <div id="hapus_kepangkatan{{$hapuskp->id}}" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content modal-md">
                <div class="modal-header">
                  <h4 class="modal-title">Hapus Kepangkatan </h4>
                </div>
                <form action="{{url('/admin/hapus-detail-kepangkatan/'.$hapuskp->id)}}" method="post" id="hapus_kepangkatan">
                  @csrf
                  <input type="hidden" name="action" value="hapus">
                  <input type="hidden" name="ids" value="">
                  <div class="modal-body card-box">
                    <p>Apakah yakin ingin di Hapus ???</p>
                    <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
          @endforeach
          @if($active == 'jabatan') <div class="tab-pane active" id="jabatan"> @else <div class="tab-pane" id="jabatan"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Jabatan</th>
                            <th>Satuan Kerja</th>
                            <th>No. SK</th>
                            <th>TGL. SK</th>
                            <th>TMT. Jabatan</th>
                            <th>Tgl.Akhir Tugas</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no3=1; ?>
                          @foreach($historys as $history)
                          <tr>
                            <td>{{$no3++}}.</td>
                            <td>{{ $history->jabatan->name_jabatan}}</td>
                            <td>{{ $history->satuankerja->name_satuan}}</td>
                            <td>{{ $history->no_sk}}</td>
                            <td>{{ date('d-m-Y', strtotime($history->tgl_sk))}}</td>
                            <td>{{ date('d-m-Y', strtotime($history->tmt_jabatan))}}</td>
                            <td>{{ date('d-m-Y', strtotime($history->masa_jabatan_sampai)) }}</td>
                            <td style="min-width:100px;">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_jabatan{{$history->id}}">Edit</a>
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_jabatan{{$history->id}}">Hapus</a>
                            </td>

                          </tr>
                          @endforeach
                          @if(count($historys) < 1)
                          <tr>
                            <td colspan="8" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                        </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_jabatan"><i class="fa fa-plus"></i> Tambah Jabatan</a>                      </div>

                </div>
              </div>
              <div id="tambah_jabatan" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Tambah Jabatan</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/tambah-jabatan/'.$datas->id)}}" method="post">
                        @csrf
                        <input type="hidden" name="action" value="tambah">
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Jabatan <span class="text-danger">*</span></label>
                              <select class="select" name="jabatan" required>
                                <option value="">Jabatan</option>
                                <?php $jabat = App\Jabatans::where('type','pegawai')->get(); ?>
                                @foreach($jabat as $jab)
                                <option value="{{$jab->id}}">{{ $jab->name_jabatan }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Satuan Kerja <span class="text-danger">*</span></label>
                              <select class="select" name="satuan_kerja" required>
                                <option value="">Satuan Kerja</option>
                                <?php $satuan = App\Satuankerja::get(); ?>
                                @foreach($satuan as $satu)
                        							<option value="{{$satu->id}}">{{ $satu->name_satuan}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nomor SK <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="no_sk" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal SK <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_sk" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">TMT.Jabatan <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_jabatan" required></div>
                            </div>
                          </div>
                          <!-- <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label">Masa Jabatan <span class="text-danger">*</span></label>
                              <input class="form-control" type="number" name="masa_jabatan_dari" placeholder="Tahun" required>
                            </div>
                          </div> -->
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tgl.Akhir Jabatan <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="masa_jabatan_sampai" required></div>                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                              <input class="form-control" name="ket" type="text" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Jabatan</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            <!-- </div>
          </div> -->
          @foreach($historys as $edithistory)
          <div id="edit_jabatan{{$edithistory->id}}" class="modal custom-modal fade" role="dialog">
            <div class="modal-dialog">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <div class="modal-content modal-lg">
                <div class="modal-header">
                  <h4 class="modal-title">Form Tambah Jabatan</h4>
                </div>
                <div class="modal-body">
                  <form class="m-b-30" action="{{url('/admin/edit-detail-jabatan/'.$edithistory->id)}}" method="post">
                    @csrf
                    <input type="hidden" name="action" value="edit">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Jabatan <span class="text-danger">*</span></label>
                          <select class="select" name="jabatan" required>
                            <option value="">Jabatan</option>
                            <?php $jabat = App\Jabatans::where('type','pegawai')->get(); ?>
                            @foreach($jabat as $jab)
                            @if($edithistory->jabatan_id == $jab->id)
                            <option value="{{$jab->id}}" selected>{{ $jab->name_jabatan }}</option>
                            @else
                            <option value="{{$jab->id}}">{{ $jab->name_jabatan }}</option>
                            @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Satuan Kerja <span class="text-danger">*</span></label>
                          <select class="select" name="satuan_kerja" required>
                            <option value="">Satuan Kerja</option>
                            <?php $satuan = App\Satuankerja::get(); ?>
                            @foreach($satuan as $satu)
                              @if($edithistory->satuankerja_id ==$satu->id)
                                  <option value="{{$satu->id}}" selected>{{ $satu->name_satuan}}</option>
                              @else
                                  <option value="{{$satu->id}}">{{ $satu->name_satuan}}</option>
                              @endif
                            @endforeach
                          </select>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Nomor SK <span class="text-danger">*</span></label>
                          <input class="form-control" type="text" name="no_sk" value="{{$edithistory->no_sk}}" required>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Tanggal SK <span class="text-danger">*</span></label>
                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_sk" value="{{date('d-m-Y', strtotime($edithistory->tgl_sk))}}" required></div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">TMT.Jabatan <span class="text-danger">*</span></label>
                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_jabatan" value="{{date('d-m-Y', strtotime($edithistory->tmt_jabatan))}}" required></div>
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label class="control-label">Tgl.Akhir Tugas <span class="text-danger">*</span></label>
                          <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="masa_jabatan_sampai" value="{{date('d-m-Y', strtotime($edithistory->masa_jabatan_sampai))}}" required></div>                            </div>
                      </div>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                          <input class="form-control" name="ket" type="text" value="{{$edithistory->ket_jabatan}}" required>
                        </div>
                      </div>
                    </div>
                    <div class="m-t-20 text-center">
                      <button class="btn btn-primary">SIMPAN</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        <!-- </div>
      </div> -->
      @endforeach
      @foreach($historys as $hapusistory)
      <div id="hapus_jabatan{{$hapusistory->id}}" class="modal custom-modal fade" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content modal-md">
            <div class="modal-header">
              <h4 class="modal-title">Hapus History Jabatan</h4>
            </div>
            <form action="{{url('/admin/hapus-detail-jabatan/'.$hapusistory->id)}}" method="post" id="hapus_jabatan">
              @csrf
              <input type="hidden" name="action" value="hapus">
              <input type="hidden" name="ids" value="{{$hapusistory->id}}">
              <div class="modal-body card-box">
                <p>Apakah yakin ingin di Hapus ???</p>
                <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                  <button type="submit" class="btn btn-danger">Delete</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      @endforeach

      </div>
    </div>
    @if($active == 'diklat') <div class="tab-pane active" id="diklat"> @else <div class="tab-pane" id="diklat"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama Diklat</th>
                            <th>Penyelenggara</th>
                            <th>Tanggal Mulai</th>
                            <th>Tanggal Selesai</th>
                            <th>Keterang</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $dik =1; ?>
                          @foreach($diklats as $diklat)
                          <tr>
                            <td>{{$dik++}}.</td>
                            <td>{{$diklat->nama_diklat}}</td>
                            <td>{{$diklat->penyelenggara}}</td>
                            <td>{{date('d-m-Y', strtotime($diklat->tgl_mulai))}}</td>
                            <td>{{date('d-m-Y', strtotime($diklat->tgl_selesai))}}</td>
                            <td>{{$diklat->ket}}</td>
                            <td style="min-width:100px;">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_diklat{{$diklat->id}}">Edit</a>
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_diklat{{$diklat->id}}">Hapus</a>
                            </td>
                          </tr>
                          @endforeach
                          @if(count($diklats) < 1)
                          <tr>
                            <td colspan="7" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                        </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_diklat"><i class="fa fa-plus"></i> Tambah Diklat</a>                      </div>

                </div>
              </div>
              <div id="tambah_diklat" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Tambah Diklat</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/create-diklat/'.$datas->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nama Diklat<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="nama_diklat" required>
                              <input type="hidden" name="action" value="tambah">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Penyelenggara<span class="text-danger">*</span></label>
                              <input class="form-control" name="penyelenggara" type="text" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Mulai<span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_mulai" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Selesai <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_selesai" required></div>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                              <textarea class="form-control" type="text" name="ket" required></textarea>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Diklat</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          @foreach($diklats as $editdiklat)
            <div id="edit_diklat{{$editdiklat->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="modal-content modal-lg">
                  <div class="modal-header">
                    <h4 class="modal-title">Form Edit Diklat</h4>
                  </div>
                  <div class="modal-body">
                    <form class="m-b-30" action="{{url('/admin/edit-diklat/'.$editdiklat->id)}}" method="post">
                      @csrf
                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Nama Diklat<span class="text-danger">*</span></label>
                            <input class="form-control" type="text" name="nama_diklat" value="{{$editdiklat->nama_diklat}}"required>
                            <input type="hidden" name="action" value="edit">
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Penyelenggara<span class="text-danger">*</span></label>
                            <input class="form-control" name="penyelenggara" type="text" value="{{$editdiklat->penyelenggara}}" required>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Tanggal Mulai<span class="text-danger">*</span></label>
                            <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_mulai" value="{{date('d-m-Y', strtotime($editdiklat->tgl_mulai))}}" required></div>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <label class="control-label">Tanggal Selesai <span class="text-danger">*</span></label>
                            <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_selesai" value="{{date('d-m-Y', strtotime($editdiklat->tgl_selesai))}}" required></div>
                          </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                            <label class="control-label">Keterangan <span class="text-danger">*</span></label>
                            <textarea class="form-control" type="text" name="ket" required>{{$editdiklat->ket}}</textarea>
                          </div>
                        </div>
                      </div>
                      <div class="m-t-20 text-center">
                        <button class="btn btn-primary">Simpan Data Diklat</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>

            <div id="hapus_diklat{{$editdiklat->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content modal-md">
                  <div class="modal-header">
                    <h4 class="modal-title">Hapus Diklat</h4>
                  </div>
                  <form action="{{url('/admin/hapus-diklat/'.$editdiklat->id)}}" method="post" id="hapus_diklat">
                    @csrf
                    <input type="hidden" name="action" value="hapus">
                    <input type="hidden" name="ids" value="{{$editdiklat->id}}">
                    <div class="modal-body card-box">
                      <p>Apakah yakin ingin di Hapus ???</p>
                      <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
              @endforeach
          <!-- ////// -->
          @if($active == 'istrisuami') <div class="tab-pane active" id="family"> @else <div class="tab-pane" id="family"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Status</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $kl=1; ?>
                          @foreach($keluargas as $keluarga)
                          <tr>
                            <td>{{$kl++}}.</td>
                            <td>{{$keluarga->nama_lengkap}}</td>
                            <td>{{$keluarga->tmp_lahir}}</td>
                            <td>{{date('d-m-Y', strtotime($keluarga->tgl_lahir))}}</td>
                            <td>{{$keluarga->agama}}</td>
                            <td>{{$keluarga->pendidikan}}</td>
                            <td>{{$keluarga->pekerjaan}}</td>
                            <td>{{$keluarga->status}}</td>
                            <td>@if($keluarga->tgl_cerai_menikah=="")- @else {{date('d-m-Y', strtotime($keluarga->tgl_cerai_menikah))}} @endif</td>
                            <td style="min-width:100px;">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_keluarga{{$keluarga->id}}">Edit</a>
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_keluarga{{$keluarga->id}}">Hapus</a>
                            </td>
                          </tr>
                          @endforeach
                          @if(count($keluargas) < 1)
                          <tr>
                            <td colspan="10" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                        </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_keluarga"><i class="fa fa-plus"></i> Tambah Keluarga</a>
                    </div>
                </div>
              </div>
              <div id="tambah_keluarga" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Istri / Suami</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/create-keluarga/'.$datas->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="nama_lengkap" required>
                              <input type="hidden" name="action" value="tambah">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                              <input class="form-control" name="tmp_lahir" type="text" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Agama <span class="text-danger">*</span></label>
                              <select class="select" name="agama" required>
                                <option value="">Pilih Agama</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Protestan">Protestan</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                              </select>
                          </div>
                        </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pendidikan Terakhir<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pendidikan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pekerjaan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pekerjaan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Status Perkawinan<span class="text-danger">*</span></label>
                              <select class="select" name="status" required>
                                <option value="">Pilih Status</option>
                                <option value="Istri">Istri</option>
                                <option value="Suami">Suami</option>
                                <option value="Cerai">Cerai</option>
                                <option value="Meninggal">Meninggal</option>
                              </select>
                          </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Cerai / Meninggal <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_cerai_menikah"></div>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Keluarga</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              @foreach($keluargas as $editkeluarga)
              <div id="edit_keluarga{{$editkeluarga->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Edit Istri / Suami</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/edit-keluarga/'.$editkeluarga->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="nama_lengkap" value="{{$editkeluarga->nama_lengkap}}" required>
                              <input type="hidden" name="action" value="edit">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                              <input class="form-control" name="tmp_lahir" type="text" value="{{$editkeluarga->tmp_lahir}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{date('d-m-Y', strtotime($editkeluarga->tgl_lahir))}}" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Agama <span class="text-danger">*</span></label>
                              <select class="select" name="agama" required>
                                <?php $agamas = [
                                  'Islam','Kristen','Protestan','Hindu','Budha'
                                ]; ?>
                                <option value="">Pilih Agama</option>
                                @foreach($agamas as $agama)
                                  @if($agama == $editkeluarga->agama)
                                    <option value="{{$agama}}" selected>{{$agama}}</option>
                                  @else
                                    <option value="{{$agama}}">{{$agama}}</option>
                                  @endif
                                @endforeach
                              </select>
                          </div>
                        </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pendidikan Terakhir<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pendidikan" value="{{$editkeluarga->pendidikan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pekerjaan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pekerjaan" value="{{$editkeluarga->pekerjaan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Status Perkawinan<span class="text-danger">*</span></label>
                              <?php $kawins = [
                                'Istri','Suami','Cerai','Meninggal'
                              ]; ?>
                              <select class="select" name="status" required>
                                <option value="">Pilih Status</option>
                                @foreach($kawins as $kawin)
                                  @if($kawin == $editkeluarga->status)
                                    <option value="{{$kawin}}" selected>{{$kawin}}</option>
                                  @else
                                    <option value="{{$kawin}}">{{$kawin}}</option>
                                  @endif
                                @endforeach
                              </select>
                          </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Cerai / Meninggal <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_cerai_menikah" value="@if($editkeluarga->tgl_cerai_menikah !==null){{date('d-m-Y', strtotime($editkeluarga->tgl_cerai_menikah))}} @endif"></div>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Keluarga</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div id="hapus_keluarga{{$editkeluarga->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content modal-md">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data Istri/Suami</h4>
                    </div>
                    <form action="{{url('/admin/hapus-keluarga/'.$editkeluarga->id)}}" method="post" id="hapus_diklat">
                      @csrf
                      <input type="hidden" name="action" value="hapus">
                      <input type="hidden" name="ids" value="{{$editkeluarga->id}}">
                      <div class="modal-body card-box">
                        <p>Apakah yakin ingin di Hapus ???</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                          <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          <!-- ////// -->
          @if($active == 'anak') <div class="tab-pane active" id="anak"> @else <div class="tab-pane" id="anak"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Jenis Kelamin</th>
                            <th>Tempat Lahir</th>
                            <th>Tanggal Lahir</th>
                            <th>Agama</th>
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                            <th>Status Perkawinan</th>
                            <th>Status Anak</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $ank=1; ?>
                          @foreach($anaks as $anak)
                          <tr>
                            <td>{{$ank++}}.</td>
                            <td>{{$anak->nama}}</td>
                            <td>{{$anak->jenkel}}</td>
                            <td>{{$anak->tmp_lahir}}</td>
                            <td>{{$anak->tgl_lahir}}</td>
                            <td>{{$anak->agama}}</td>
                            <td>{{$anak->pendidikan}}</td>
                            <td>{{$anak->pekerjaan}}</td>
                            <td>{{$anak->status_kawin}}</td>
                            <td>{{$anak->status_anak}}</td>
                            <td style="min-width:100px;">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anak{{$anak->id}}">Edit</a>
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_anak{{$anak->id}}">Hapus</a>
                            </td>
                          </tr>
                          @endforeach
                          @if(count($anaks) < 1)
                          <tr>
                            <td colspan="11" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                        </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_anak"><i class="fa fa-plus"></i> Tambah Keluarga</a>                      </div>

                </div>
              </div>
              <div id="tambah_anak" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Tambah Anak</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/create-anak/'.$datas->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="nama" required>
                              <input type="hidden" name="action" value="tambah">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                              <select class="select" name="jenkel" required>
                                <?php $jenkel = [
                                  'Laki-Laki','Perempuan'
                                ]; ?>
                                <option value="">Pilih Jenis Kelamin</option>
                                @foreach($jenkel as $jkl)
                                    <option value="{{$jkl}}">{{$jkl}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                              <input class="form-control" name="tmp_lahir" type="text" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Agama <span class="text-danger">*</span></label>
                              <select class="select" name="agama" required>
                                <?php $agamas = [
                                  'Islam','Kristen','Protestan','Hindu','Budha'
                                ]; ?>
                                <option value="">Pilih Agama</option>
                                @foreach($agamas as $agama)
                                    <option value="{{$agama}}">{{$agama}}</option>
                                @endforeach
                              </select>
                          </div>
                        </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pendidikan Terakhir <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pendidikan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pekerjaan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pekerjaan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Status Kawin<span class="text-danger">*</span></label>
                              <select class="select" name="status_kawin" required>
                                <?php $statuskawin = [
                                  'Belum Menikah','Sudah Menikah'
                                ]; ?>
                                <option value="">Pilih Status Kawin</option>
                                @foreach($statuskawin as $statusk)
                                    <option value="{{$statusk}}">{{$statusk}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Status Anak<span class="text-danger">*</span></label>
                              <select class="select" name="status_anak" required>
                                <?php $anakss = [
                                  'Anak Kandung','Anak Angkat'
                                ]; ?>
                                <option value="">Pilih Status Anak</option>
                                @foreach($anakss as $anak)
                                    <option value="{{$anak}}">{{$anak}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Anak</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              @foreach($anaks as $editanak)
              <div id="edit_anak{{$editanak->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Edit Anak</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/edit-anak/'.$editanak->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="nama" value="{{$editanak->nama}}" required>
                              <input type="hidden" name="action" value="edit">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                              <select class="select" name="jenkel" required>
                                <?php $jenkel = [
                                  'Laki-Laki','Perempuan'
                                ]; ?>
                                <option value="">Pilih Jenis Kelamin</option>
                                @foreach($jenkel as $jkl)
                                  @if($jkl == $editanak->jenkel)
                                    <option value="{{$jkl}}" selected>{{$jkl}}</option>
                                  @else
                                    <option value="{{$jkl}}">{{$jkl}}</option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                              <input class="form-control" name="tmp_lahir" type="text" value="{{$editanak->tmp_lahir}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                              <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{$editanak->tgl_lahir}}" required></div>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Agama <span class="text-danger">*</span></label>
                              <select class="select" name="agama" required>
                                <?php $agamas = [
                                  'Islam','Kristen','Protestan','Hindu','Budha'
                                ]; ?>
                                <option value="">Pilih Agama</option>
                                @foreach($agamas as $agama)
                                  @if($agama == $editanak->agama)
                                    <option value="{{$agama}}" selected>{{$agama}}</option>
                                  @else
                                    <option value="{{$agama}}">{{$agama}}</option>
                                  @endif
                                @endforeach
                              </select>
                          </div>
                        </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pendidikan Terakhir <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pendidikan" value="{{$editanak->pendidikan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Pekerjaan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="pekerjaan" value="{{$editanak->pekerjaan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Status Kawin<span class="text-danger">*</span></label>
                              <select class="select" name="status_kawin" required>
                                <?php $statuskawin = [
                                  'Belum Menikah','Sudah Menikah'
                                ]; ?>
                                <option value="">Pilih Status Kawin</option>
                                @foreach($statuskawin as $statusk)
                                  @if($statusk == $editanak->status_kawin)
                                    <option value="{{$statusk}}" selected>{{$statusk}}</option>
                                  @else
                                    <option value="{{$statusk}}">{{$statusk}}</option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-12">
                            <div class="form-group">
                              <label class="control-label">Status Anak<span class="text-danger">*</span></label>
                              <select class="select" name="status_anak" required>
                                <?php $anakss = [
                                  'Anak Kandung','Anak Angkat'
                                ]; ?>
                                <option value="">Pilih Status Anak</option>
                                @foreach($anakss as $anak)
                                  @if($anak == $editanak->status_anak)
                                    <option value="{{$anak}}" selected>{{$anak}}</option>
                                  @else
                                    <option value="{{$anak}}">{{$anak}}</option>
                                  @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Anak</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div id="hapus_anak{{$editanak->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content modal-md">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data Anak</h4>
                    </div>
                    <form action="{{url('/admin/hapus-anak/'.$editanak->id)}}" method="post" id="hapus_diklat">
                      @csrf
                      <input type="hidden" name="action" value="hapus">
                      <input type="hidden" name="ids" value="{{$editanak->id}}">
                      <div class="modal-body card-box">
                        <p>Apakah yakin ingin di Hapus ???</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                          <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          @if($active == 'alamat') <div class="tab-pane active" id="alamat"> @else <div class="tab-pane" id="alamat"> @endif
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="task-list-body">
                    <ul id="task-list">

                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Alamat/Jalan</th>
                            <th>RT/RW</th>
                            <th>Kelurahan</th>
                            <th>Kecamatan</th>
                            <th>Kabupaten</th>
                            <th>Propinsi</th>
                            <th>Kode Pos</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $al=1; ?>
                          @foreach($alamats as $alamat)
                          <tr>
                            <td>{{$al++}}.</td>
                            <td>{{$alamat->jalan}}</td>
                            <td>{{$alamat->rt}}/{{$alamat->rw}}</td>
                            <td>{{$alamat->kelurahan}}</td>
                            <td>{{$alamat->kecamatan}}</td>
                            <td>{{$alamat->kabupaten}}</td>
                            <td>{{$alamat->propinsi}}</td>
                            <td>{{$alamat->kode_pos}}</td>
                            <td style="min-width:100px;">
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_alamat{{$alamat->id}}">Edit</a>
                              <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_alamat{{$alamat->id}}">Hapus</a>
                            </td>
                          </tr>
                          @endforeach
                          @if(count($alamats) < 1)
                          <tr>
                            <td colspan="9" class="text-center"> Data Kosong</td>
                          </tr>
                          @endif
                        </tbody>
                      </table>
                      </div>
                    </ul>
                    <div class="col-sm-12 text-right m-b-30">
                      <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#tambah_alamat"><i class="fa fa-plus"></i> Tambah Alamat</a>                      </div>

                </div>
              </div>
              <div id="tambah_alamat" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Tambah Alamat</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/create-alamat/'.$datas->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Alamat / Jalan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="jalan" required>
                              <input type="hidden" name="action" value="tambah">
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label">RT <span class="text-danger">*</span></label>
                              <?php $rts = ['001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020'] ?>
                              <select class="form-control" name="rt" required>
                                <option value="">Pilih RT</option>
                                @foreach($rts as $rt)
                                <option value="{{$rt}}">{{$rt}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label">RW<span class="text-danger">*</span></label>
                              <?php $rws = ['001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020'] ?>
                              <select class="form-control" name="rw" required>
                                <option value="">Pilih RW</option>
                                @foreach($rws as $rw)
                                <option value="{{$rw}}">{{$rw}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kelurahan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kelurahan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kecamatan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kecamatan" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kabupaten <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kabupaten" required>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label">Propinsi <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="propinsi" required>
                            </div>
                          </div>
                          <div class="col-sm-3">
                            <div class="form-group">
                              <label class="control-label">Kode Pos <span class="text-danger">*</span></label>
                              <input class="form-control" type="number" name="kode_pos" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Alamat</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              @foreach($alamats as $editalamat)
              <div id="edit_alamat{{$editalamat->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <div class="modal-content modal-lg">
                    <div class="modal-header">
                      <h4 class="modal-title">Form Tambah Alamat</h4>
                    </div>
                    <div class="modal-body">
                      <form class="m-b-30" action="{{url('/admin/edit-alamat/'.$editalamat->id)}}" method="post">
                        @csrf
                        <div class="row">
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Alamat / Jalan<span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="jalan" value="{{$editalamat->jalan}}" required>
                              <input type="hidden" name="action" value="edit">
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">RT <span class="text-danger">*</span></label>
                              <?php $rts = ['001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020'] ?>
                              <select class="form-control" name="rt" required>
                                <option value="">Pilih RT</option>
                                @foreach($rts as $rt)
                                @if($rt == $editalamat->rt)
                                <option value="{{$rt}}" selected>{{$rt}}</option>
                                @else
                                <option value="{{$rt}}">{{$rt}}</option>
                                @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">RW<span class="text-danger">*</span></label>
                              <?php $rws = ['001','002','003','004','005','006','007','008','009','010','011','012','013','014','015','016','017','018','019','020'] ?>
                              <select class="form-control" name="rw" required>
                                <option value="">Pilih RW</option>
                                @foreach($rws as $rw)
                                @if($rw == $editalamat->rw)
                                <option value="{{$rw}}" selected>{{$rw}}</option>
                                @else
                                <option value="{{$rw}}">{{$rw}}</option>
                                @endif
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kelurahan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kelurahan" value="{{$editalamat->kelurahan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kecamatan <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kecamatan" value="{{$editalamat->kecamatan}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kabupaten <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="kabupaten" value="{{$editalamat->kabupaten}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Propinsi <span class="text-danger">*</span></label>
                              <input class="form-control" type="text" name="propinsi" value="{{$editalamat->propinsi}}" required>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-group">
                              <label class="control-label">Kode Pos <span class="text-danger">*</span></label>
                              <input class="form-control" type="number" name="kode_pos" value="{{$editalamat->kode_pos}}" required>
                            </div>
                          </div>
                        </div>
                        <div class="m-t-20 text-center">
                          <button class="btn btn-primary">Simpan Data Alamat</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <div id="hapus_alamat{{$editalamat->id}}" class="modal custom-modal fade" role="dialog">
                <div class="modal-dialog">
                  <div class="modal-content modal-md">
                    <div class="modal-header">
                      <h4 class="modal-title">Hapus Data Alamat</h4>
                    </div>
                    <form action="{{url('/admin/hapus-alamat/'.$editalamat->id)}}" method="post">
                      @csrf
                      <input type="hidden" name="action" value="hapus">
                      <input type="hidden" name="ids" value="{{$editalamat->id}}">
                      <div class="modal-body card-box">
                        <p>Apakah yakin ingin di Hapus ???</p>
                        <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                          <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


</div>
</div>

@endsection
