@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center">

              <?php

              $dataPoints = array(
              	array("label"=> "Pokok", "y"=> $pokoks[0]->totalpokok),
              	array("label"=> "Wajib", "y"=> $wajins[0]->totalwajib),
              	array("label"=> "Sukarela", "y"=> $sukarelas[0]->totalsukarela),
              	array("label"=> "Investasi", "y"=> $investasis[0]->totalinvestasi),
              	array("label"=> "Wakaf", "y"=> $wakafs[0]->totalwakaf),
              	array("label"=> "Infaq", "y"=> $infaqs[0]->totalinfak),
                array("label"=> "SHU", "y"=> $shus[0]->totalshu),
              	array("label"=> "Lain-Lain", "y"=> $lains[0]->totallain)
              );

              ?>
              <script>
              window.onload = function () {

              var chart = new CanvasJS.Chart("chartContainer", {
              	animationEnabled: true,
              	exportEnabled: true,
              	title:{
              		text: "Statistik Simpanan"
              	},
              	subtitles: [{
              		text: "Periode"
              	}],
              	data: [{
              		type: "pie",
              		showInLegend: "true",
              		legendText: "{label}",
              		indexLabelFontSize: 16,
              		indexLabel: "{label} - #percent%",
              		yValueFormatString: "Rp #,##0",
              		dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
              	}]
              });
              chart.render();

              }
              </script>
              </head>
              <body>
              <div id="chartContainer" style="height: 370px; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
@endsection
