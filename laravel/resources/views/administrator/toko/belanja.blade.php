@extends('layouts.admin.app')
@section('content')
            <div class="page-wrapper">
                <div class="content container-fluid">
					<div class="row">
						<div class="col-xs-12">
							<h4 class="page-title">Shopping</h4>
						</div>

					</div>
					<div class="row filter-row">
            <form class="m-b-30" action="{{route('belanja')}}" method="post">
              @csrf
              <input type="hidden" name="action" value="cari">
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Kode Produks</label>
  								<input type="text" class="form-control floating" name="kode" value="{{$kode}}"/>
  							</div>
  						</div>
              <div class="col-sm-4 col-xs-6">
  							<div class="form-group form-focus">
  								<label class="control-label">Nama Produk</label>
  								<input type="text" class="form-control floating" name="name" value="{{$name}}"/>
  							</div>
  						</div>
  						<div class="col-sm-4 col-xs-12">
  							<button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
  						</div>
            </form>
          </div>
					<div class="row staff-grid-row">
            @foreach($produks as $produk)
						<div class="col-md-4 col-sm-4 col-xs-6 col-lg-3">
							<div class="profile-widget">
								<div class="profile-imges">
									<a href="#"  data-toggle="modal" data-target="#lihat"><img src="{{url('laravel/public/gambars/'.$produk->gambar)}}" width="100%"></a>
								</div>
                <p><h6 class="user-name m-t-10 m-b-0 text-left">Kode : {{$produk->kode}}</h6></p>
								<h6 class="user-name m-t-10 m-b-0 text-left">{{$produk->name}}</h6>
								<h6 class="user-name m-t-10 m-b-0 text-left">Rp. {{number_format($produk->harga)}},-</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Berat : {{$produk->berat}} Gram</h6>
                <h6 class="user-name m-t-10 m-b-0 text-left">Stok : {{$produk->stok}}</h6>
                <div class="small text-muted"></div><hr>
                <form class="" action="{{route('belanja')}}" method="post" id="beli{{$produk->id}}">
                  @csrf
                  <input type="hidden" name="action" value="beli">
                  <input type="hidden" name="ids" value="{{$produk->id}}">
                  <a href="#" onclick="event.preventDefault();
                                document.getElementById('beli{{$produk->id}}').submit();" class="btn btn-primary btn-sm m-t-10">BELI</a>
                  <a href="#" data-toggle="modal" data-target="#detail{{$produk->id}}" class="btn btn-default btn-sm m-t-10">DETAIL</a>
                </form>
							</div>
						</div>
            @endforeach
            @foreach($produks as $edituser)
            <div id="detail{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
      				<div class="modal-dialog">
      					<div class="modal-content modal-md">
      						<div class="modal-header">
      							<h4 class="modal-title">Detail Produk</h4>
      						</div>
      						<div class="modal-body card-box col-md-12">
                      <div class="col-md-6">
                        <p><img src="{{url('laravel/public/gambars/'.$edituser->gambar)}}" width="100%"></p>
                      </div>
                      <div class="col-md-6">
                        <p><strong>Kode :</strong> {{$edituser->kode}}</p>
                        <p><strong>Nama :</strong> {{$edituser->name}}</p>
                        <p><strong>Harga :</strong> {{$edituser->harga}}</p>
                        <p><strong>Stok :</strong> {{$edituser->stok}}</p>
                        <p><strong>Keterangan :</strong> </p>
                        <p>{{$edituser->keterangan}}</p>
                      </div>

                      <div class="m-t-20"><hr>
                        <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-primary">BELI</button>
                      </div>
      						</div>

      					</div>
      				</div>
      			</div>
            <div id="delete{{$edituser->id}}" class="modal custom-modal fade" role="dialog">
              <div class="modal-dialog">
                <div class="modal-content modal-md">
                  <div class="modal-header">
                    <h4 class="modal-title">Hapus Produk</h4>
                  </div>
                  <form action="{{url('/administrator/produk')}}" method="post">
                    @csrf
                    <input type="hidden" name="action" value="hapus">
                    <input type="hidden" name="ids" value="{{$edituser->id}}">
                    <div class="modal-body card-box">
                      <p>Apakah yakin ingin di Hapus "{{$edituser->kode}}" ???</p>
                      <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                        <button type="submit" class="btn btn-danger">Delete</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            @endforeach
					</div>
      </div>

      <div id="tambah" class="modal custom-modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content modal-md">
						<div class="modal-header">
							<h4 class="modal-title">UPLOAD PRODUK</h4>
						</div>
						<div class="modal-body card-box">
              <form class="" action="{{url('/administrator/produk')}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group form-focus">
  								<label class="control-label">Kode Produk</label>
  								<input type="text" class="form-control floating" name="kode" value="" required/>
  							</div>
                <div class="form-group form-focus">
  								<label class="control-label">Nama Produk</label>
  								<input type="text" class="form-control floating" name="name" value="" required/>
  							</div>
                <div class="form-group form-focus">
  								<label class="control-label">Harga Produk</label>
  								<input type="number" class="form-control floating" name="harga" value="" required/>
  							</div>
                <div class="form-group form-focus">
  								<label class="control-label">Stok</label>
  								<input type="number" class="form-control floating" name="stok" value="" required/>
  							</div>
                <input type="hidden" name="action" value="tambah" required/>
                <div class="form-group">
  								<input type="file" class="form-control" name="gambar" required/>
                  <span class="help-block">Rekomendasi Gambar Sama Sisi</span>
  							</div>
                <div class="form-group form-focus">
  								<label class="control-label">Keterangan</label>
                  <textarea class="form-control floating" name="keterangan" rows="8" cols="80" required></textarea>
  							</div>
  							<div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
  								<button type="submit" class="btn btn-success">Upload</button>
  							</div>
              </form>
						</div>
					</div>
				</div>
			</div>

    </div>
		<div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
