@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">VOUCHER LISTRIK</h4>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-md-4 col-xs-12">
          <form class="" action="{{route('proses-ppob')}}" method="post">
          <div class="form-group">
            <select class="form-control" name="provider" id="provider" required>
              <option value="">Pilih Provider</option>
              <option value="PLN">PLN</option>
            </select>
          </div>
          <script type="text/javascript">
            $("select[name='provider']").change(function(){
                $(document).ajaxStart(function(){
                    $("#wait").css("display", "block");
                });
                $(document).ajaxComplete(function(){
                    $("#wait").css("display", "none");
                });
                $("button").click(function(){
                    $("#txt").load("demo_ajax_load.asp");
                });
              // alert('change');
                var provider = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('select-provider') ?>",
                    method: 'POST',
                    data: {provider:provider, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#paket").html("");
                      $("#paket").append(data);
                      $("#divharga").hide();
                      $("#buttom").hide();
                    }
                });
            });
          </script>
          <div class="form-group">
            <select class="form-control" name="paket" id="paket">
              <option value="">Pilih Nominal Pulsa</option>
            </select>
          </div>
          <script type="text/javascript">
            $("select[name='paket']").change(function(){
                $(document).ajaxStart(function(){
                    $("#wait").css("display", "block");
                });
                $(document).ajaxComplete(function(){
                    $("#wait").css("display", "none");
                });
                $("button").click(function(){
                    $("#txt").load("demo_ajax_load.asp");
                });
              // alert('change');
                var paket = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('select-paket') ?>",
                    method: 'POST',
                    data: {paket:paket, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#harga").html("");
                      $("#harga").append(data);
                      $("#divharga").show();
                      $("#buttom").show();
                    }
                });
            });
          </script>
          <div class="form-group" id="divharga">
            <label id="harga" class="form-control" name="harga">Rp.</label>
          </div>
          <script>
            $("#divharga").hide();
          </script>
          <div class="form-group" id="buttom">
            <input type="submit" class="form-control btn btn-primary" name="btn" value="P R O S E S">
          </div>
          <script>
            $("#buttom").hide();
          </script>
        </form>
        </div>
        <div class="col-sm-8 col-md-8 col-xs-12">
          <hr>
          <div class="col-xs-12">
            <h4 class="page-title text-center"> MENU LAIN</h4>
          </div>
            @foreach($menus as $menu)
            <div class="col-sm-6 col-md-6 col-xs-6">
                <a href="{{url('administrator/'.$menu->route)}}"><button type="button" name="button"  class="btn btn-success btn-sm m-t-10 form-control">{{$menu->menu}}</button></a>
            </div>
            @endforeach
          </div>
        <center> <div id="wait" style="display:none;width:50%;border:0px solid black;position:absolute;top:50%;padding:5px;"><img src="{{url('images/load.gif')}}" width="100%" /></div></center>
      </div>
    </div>
</div>
@endsection
