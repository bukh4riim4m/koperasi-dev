@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-title">Laporan Barang</h4>
        </div>
      </div>
      <div class="row filter-row">
        <form class="m-b-30" action="{{route('administrator-laporan-barang')}}" method="post">
          @csrf
          <input type="hidden" name="action" value="cari">
          <div class="col-sm-3 col-xs-6">
            <div class="form-group form-focus">
              <label class="control-label">Dari</label>
              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
            </div>
          </div>
          <div class="col-sm-3 col-xs-6">
            <div class="form-group form-focus">
              <label class="control-label">Sampai</label>
              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="until" value="{{$until}}"></div>
            </div>
          </div>
          <div class="col-sm-4 col-md-3 col-xs-12">
            <div class="form-group form-focus select-focus">
              <label class="control-label">Status</label>
              <select class="select floating" name="status">
                <option value="">-- Semua --</option>
                @foreach($statuses as $status)
                  @if($stts == $status->id)
                    <option value="{{$status->id}}" selected> {{$status->name}} </option>
                  @else
                    <option value="{{$status->id}}"> {{$status->name}} </option>
                  @endif
                @endforeach
              </select>
            </div>
          </div>
          <div class="col-sm-3 col-xs-12">
            <button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
          </div>
        </form>
      </div>
      <br>
      <div class="staff-grid-row">
        <div class="table-responsive">
          <table class="table table-striped custom-table">
            <thead>
              <tr>
                <th>No</th>
                <th>Tgl. Pesanan</th>
                <th>Kode Barang</th>
                <th>Ongkir</th>
                <th>Harga</th>
                <th>Modal</th>
                <th>Status</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; ?>
              @foreach($datas as $data)
              <tr>
                <td>{{$no++}}</td>
                <td>{{date('d-m-Y', strtotime($data->tanggal))}}</td>
                <td>{{$data->kode}}</td>
                <td>Rp {{number_format($data->ongkir)}}</td>
                <td>Rp {{number_format($data->total_harga)}}</td>
                <td>Rp {{number_format($data->modal)}}</td>
                <td>@if($data->status == 2) <span class="label label-warning-border"> {{$data->statusId->name}} </span> @elseif($data->status ==3 || $data->status ==4) <span class="label label-success-border"> {{$data->statusId->name}} </span> @else <span class="label label-danger-border"> {{$data->statusId->name}} </span> @endif</td>
                <td>@if($data->status == 2 || $data->status == 5) <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#detail{{$data->id}}">Detail</a> @endif</td>
              </tr>
              @endforeach
              @if(count($datas) < 1)
              <tr>
                <td colspan="8" align="center"> Data kosong </td>
              </tr>
              @endif
            </tbody>
          </table>
        </div>
      </div>
  </div>
  @foreach($datas as $detail)
  <div id="detail{{$detail->id}}" class="modal custom-modal fade" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content modal-lg">
        <div class="modal-header">
          <h4 class="modal-title"> DETAIL BELANJA</h4>
        </div>
          <div class="modal-body card-box">
            <div class="row">
              <div class="col-md-4">
                <img src="{{url('laravel/public/gambars/'.$detail->tokoId->gambar)}}" alt="" width="100%">
              </div>
              <div class="col-md-8">

                <h3>{{$detail->tokoId->name}}</h3>
                <br>
                <table class="table">
                  <tr>
                    <td>Nomor Pemesanan</td>
                    <td>: {{$detail->no_pemesanan}}</td>
                  </tr>
                  <tr>
                    <td>Kode Barang</td>
                    <td>: {{$detail->kode}}</td>
                  </tr>
                  <tr>
                    <td>Harga Barang</td>
                    <td>: Rp {{number_format($detail->total_harga-$detail->ongkir)}}</td>
                  </tr>

                  <tr>
                    <td>Jumlah Barang</td>
                    <td>: {{$detail->jumlah}}</td>
                  </tr>
                  <tr>
                    <td>Berat Barang</td>
                    <td>: {{$detail->berat}} Gram / {{$detail->berat/1000}} Kg</td>
                  </tr>
                  <tr>
                    <td>Ongkos Kirim</td>
                    <td>: {{strtoupper($detail->melalui)}}, Rp {{number_format($detail->ongkir)}}</td>
                  </tr>
                  <tr>
                    <td>Total Transfer </td>
                    <td>: Rp {{number_format($detail->total_harga+$detail->kode_unik)}} (Termasuk Ongkir)</td>
                  </tr>
                  <tr>
                    <td>Alamat Kirim </td>
                    <td>: {{$detail->alamat}}</td>
                  </tr>
                  <tr>
                    <td>Status Pemesanan </td>
                    <td>: @if($detail->status == 1) <span class="label label-warning-border"> {{$detail->statusId->name}} </span> @elseif($detail->status ==2 || $detail->status ==3 || $detail->status ==4) <span class="label label-success-border"> {{$detail->statusId->name}} </span> @else <span class="label label-danger-border"> {{$detail->statusId->name}} </span> @endif</td>
                  </tr>
                  @if($detail->status == 5)
                  <tr>
                    <td>Alasan Pembatalan</td>
                    <td>: {{$detail->alasan}}</td>
                  </tr>
                  @endif
                </table>
              </div>
            </div>
            <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
            </div>
          </div>
      </div>
    </div>
  </div>
  @endforeach
</div>
@endsection
