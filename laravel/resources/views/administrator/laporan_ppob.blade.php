@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-6">
		<h4 class="page-title">Laporan PPOB</h4>
	</div>
  <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#download"><i class="fa fa-download"></i> Download</a>
  </div>
</div>

<div class="row filter-row">
  <form class="m-b-30" action="{{route('administrator-laporan-ppob')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="until" value="{{$until}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-12">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = ['Proses','Berhasil','Gagal']; ?>
        <select class="select floating" name="status">
          <option value="">-- Semua --</option>
          @foreach($statuses as $key=>$status)
          @if($status == $stts)
          <option value="{{$status}}" selected>{{$status}}</option>
          @else
          <option value="{{$status}}">{{$status}}</option>
          @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <button type="submit" class="btn btn-success btn-block"> TAMPILKAN </button>
    </div>
  </form>
</div>
<br>
<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>No & tgl Transaksi</th>
            <td>No Anggota</td>
            <th>No HP/Meteran</th>
            <th>Paket</th>
            <th>Jual</th>
            <th>Modal</th>
            <th>Untung</th>
            <th>Status</th>
            <th style="max-width:70px;">Action</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1;
          $tot_jual=0;$tot_modal=0;$tot_untung=0;?>
          @foreach($datas as $data)
          <?php $tot_jual+= $data->nominal;
          $tot_modal+= $data->nta;
          $tot_untung+=$data->nominal-$data->nta;?>
					<tr class="holiday-completed">
						<td>{{$no++}}</td>
						<td>{{$data->no_trx}} <br> {{date('d-m-Y H:i',strtotime($data->created_at))}} </td>
            <td>{{$data->no_anggota}}</td>
            <td>{{$data->nomorhp}}</td>
            <td>{{$data->paket}}</td>
            <td>Rp {{number_format($data->nominal,0,",",".")}}</td>
            <td>Rp {{number_format($data->nta,0,",",".")}}</td>
            <td>Rp {{number_format($data->nominal-$data->nta,0,",",".")}}</td>
            <td>@if($data->status=='Berhasil') <span class="label label-success-border">{{$data->status}}</span> @elseif($data->status=='Proses') <span class="label label-warning-border">{{$data->status}} </span> @else <span class="label label-danger-border">{{$data->status}} </span> @endif</td>
            <td style="max-width:70px;">
              <form class="" action="" method="post" id="cek-status{{$data->id}}">
                @csrf
                <input type="hidden" name="ids" value="{{$data->id}}">
              </form>
              @if($data->status =='Proses')
							<!-- <a href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('cek-status{{$data->id}}').submit();" class="btn btn-primary btn-sm rounded" >Refresh</a> -->
              @elseif($data->status =='Berhasil')
              <a href="javascript:void(0);" data-toggle="modal" data-target="#sn{{$data->id}}" class="btn btn-primary btn-sm rounded" >SN</a>
              @endif
            </td>
					</tr>
        @endforeach
        @if(count($datas) < 1)
        <tr>
          <td colspan="10" align="center"> Data kosong </td>
        </tr>
        @endif
				</tbody>

			</table>
		</div>
    @if(count($datas) > 0)
    <div class="col-md-4 row">
      <table class="table table-striped custom-table">
        <tr>
          <td>Total Transaksi</td><td>:</td> <td class="text-right"> {{count($datas)}}</td>
        </tr>
        <tr>
          <td>Total Jual</td> <td>:</td><td class="text-right"> Rp {{number_format($tot_jual,0,",",".")}}</td>
        </tr>
        <tr>
          <td>Total Modal</td> <td>:</td><td class="text-right"> Rp {{number_format($tot_modal,0,",",".")}}</td>
        </tr>
        <tr>
          <td>Total Untung</td> <td>:</td><td class="text-right"> Rp {{number_format($tot_untung,0,",",".")}}</td>
        </tr>
      </table>
    </div>
    @endif
	</div>
</div>

</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
