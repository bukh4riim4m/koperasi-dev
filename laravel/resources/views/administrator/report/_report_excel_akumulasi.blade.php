<tbody>
  <tr>
    <td>No</td>
    <td>No. Anggota</td>
    <td>No transaksi</td>
    <td>Tgl. transaksi</td>
    <td>Nominal</td>
    <td>Gerai</td>
  </tr>
  <?php $no=1;
  $total=0;?>
  @foreach($akumulasis as $akumulas)
  <?php $total+= $akumulas->nominal;?>
  <tr>
    <td>{{$no++}}</td>
    <td>{{$akumulas->no_kssd}}</td>
    <td>{{$akumulas->no_trx}}</td>
    <td>{{$akumulas->tgl_trx}}</td>
    <td>{{$akumulas->nominal}}</td>
    <td>{{$akumulas->gerai}}</td>
  </tr>
  @endforeach
  <tr>
    <td colspan="4">Total</td>
    <td>{{$total}}</td>
  </tr>
</tbody>
