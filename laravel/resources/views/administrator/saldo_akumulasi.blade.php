@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-6">
		<h4 class="page-title">Saldo Akumulasi</h4>
	</div>
  <div class="col-xs-6">
    <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah jenissimpanan</a> -->
  </div>
</div>

<div class="row filter-row">
  <form class="" action="{{url('/administrator/saldo-akumulasi-belanja')}}" method="post">
    @csrf
<input type="hidden" name="action" value="cari">
	<div class="col-sm-4 col-xs-6">
		<div class="form-group form-focus">
			<label class="control-label">Nomor Anggota</label>
			<input type="text" name="no_anggota" value="{{$nomor}}" class="form-control floating">
		</div>
	</div>
  <div class="col-sm-3 col-md-4 col-xs-6">
    <div class="form-group form-focus select-focus">
      <label class="control-label">Tahun</label>
      <?php $tahuns = App\Tahun::get(); ?>
      <select class="select floating" name="tahun">
        @foreach($tahuns as $thn)
          @if($tahun == $thn->name)
            <option value="{{$thn->name}}" selected> {{$thn->name}} </option>
          @else
            <option value="{{$thn->name}}"> {{$thn->name}} </option>
          @endif
        @endforeach
      </select>
    </div>
  </div>
	<div class="col-sm-4 col-xs-12">
		<!-- <a href="#" class="btn btn-success btn-block"> Search </a> -->
    <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN">
	</div>
</form>
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
    @if($nomor)
    <div class="card-box m-b-0">
    <div class="row">
      <div class="col-md-12">
        <div class="profile-view">
          <div class="profile-img-wrap">
            <div class="profile-img">
              <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.$users->fotodiri) }}" alt=""></a>
            </div>
          </div>
          <div class="profile-basic">
            <div class="row">
              <div class="col-md-12">
                <div class="table-responsive">
                  <table class="table">
                    <tr>
                      <td>Nomor Anggota<span class="pull-right">:</span></td>
                      <td>@if($users->no_anggota==null) @else {{$users->no_anggota}} @endif</td>
                    </tr>
                    <tr>
                      <td>Nama Lengkap<span class="pull-right">:</span></td>
                      <td>@if($users->name==null) @else {{$users->name}} @endif</td>
                    </tr>
                    <tr>
                      <td>Nomor Telpon<span class="pull-right">:</span></td>
                      <td>@if($users->telp==null) @else {{$users->telp}} @endif</td>
                    </tr>
                    <tr>
                      <td>Periode Tahun<span class="pull-right">:</span></td>
                      <td>01-Jan-{{$tahun}} s/d 31-Des-{{$tahun}}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>

		<div class="table-responsive"><hr>
			<table class="table table-striped custom-table">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
            <th>No.Anggota</th>
						<th>Tanggal Transaksi</th>
            <th>No. Transaksi</th>
            <th>Nominal</th>
            <th>Akumulasi</th>
            <th>Gerai</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody><?php $no=1; $saldo =0;?>
          @foreach($datas as $data)
					<tr class="holiday-completed">
            <?php $saldo+= $data->nominal;?>
						<td>{{$no++}}</td>
						<td>{{$data->no_kssd}}</td>
            <td>{{$data->tgl_trx}}</td>
						<td>{{$data->no_trx}}</td>
            <td>Rp. {{number_format($data->nominal,0,",",".")}}</td>
            <td>Rp. {{number_format($saldo,0,",",".")}}</td>
						<td>{{$data->gerai}}</td>
						<td style="min-width:150px;">
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas{{$data->id}}">Edit</a>
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_komunitas{{$data->id}}">Hapus</a>
						</td>
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
    <div class="row col-md-6 table-responsive">
      <table class="table table-striped custom-table" width="40px">
        <thead>
          <tr>
            <th>Total Akumulasi</th>
            <th>: Rp. {{number_format($saldo)}},-</th>
          </tr>
        </thead>
      </table>
    </div>

	</div>
</div>

<div id="add_golongan_pegawai" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Form Jenis Simpanan</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/data-jenis-simpanan')}}" method="post">
          <input type="hidden" name="action" value="tambah">
            @csrf
          <div class="form-group">
            <label>Nama Jenis Simpanan <span class="text-danger">*</span></label>
            <input class="input-sm form-control" required="" type="text" name="name">
          </div>
          <div class="m-t-20 text-center">
            <input class="btn btn-primary" type="submit" value="SIMPAN"/>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@foreach($datas as $dta)
<div id="edit_komunitas{{$dta->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Edit Saldo Akumulasi</h4>
      </div>
      <div class="modal-body">
        <form action="{{url('/administrator/saldo-akumulasi-belanja')}}" method="post">
          @csrf
          <div class="form-group">
            <div class="form-group form-focus">
              <label class="control-label">Tanggal Transaksi</label>
              <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="tgl_trx" value="{{date('d-m-Y', strtotime($dta->tgl_trx))}}" required></div>
            </div>
            <input type="hidden" name="ids" value="{{$dta->id}}">
            <input type="hidden" name="action" value="edit">
            <input type="hidden" name="tahun" value="{{$tahun}}">
          </div>
          <div class="form-group">
            <label>No.Anggota <span class="text-danger">*</span></label>
            <input class="input-sm form-control" name="no_anggota" value="{{$dta->no_kssd}}" type="text" required>
          </div>
          <div class="form-group">
            <label>Nominal <span class="text-danger">*</span></label>
            <input class="input-sm form-control" name="nominal" value="{{$dta->nominal}}" type="text" required>
          </div>
          <div class="form-group">
            <label>Gerai <span class="text-danger">*</span></label>
            <input class="input-sm form-control" name="gerai" value="{{$dta->gerai}}" type="text" required>
          </div>
          <div class="m-t-20 text-center">
            <button class="btn btn-success">SIMPAN</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="hapus_komunitas{{$dta->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content modal-md">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Saldo Akumulasi</h4>
      </div>
      <form action="{{url('/administrator/saldo-akumulasi-belanja')}}" method="post" id="hapus_golongan">
        <input type="hidden" name="ids" value="{{$dta->id}}">
        <input type="hidden" name="action" value="hapus">
        <input type="hidden" name="tahun" value="{{$tahun}}">
        <input type="hidden" name="no_anggota" value="{{$dta->no_kssd}}">
        @csrf
        <div class="modal-body card-box">
          <p>Apakah yakin ingin di Hapus : </p>
          <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Kembali</a>
            <button type="submit" class="btn btn-danger">Hapus</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endforeach
@endif
  </div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
