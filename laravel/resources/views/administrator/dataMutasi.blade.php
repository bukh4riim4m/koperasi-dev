@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Mutasi Anggota</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{('/administrator/data-mutasi')}}" method="post">
    @csrf
    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value="{{$anggota}}"/>
      </div>
    </div>
    <div class="col-sm-3 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Dari</label>
				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
			</div>
		</div>
    <div class="col-sm-3 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Sampai</label>
				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$until}}"></div>
			</div>
		</div>
    <div class="col-sm-3 col-xs-6">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="Search"/>
    </div>
  </form>
</div><a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a>
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th><strong>No.</strong></th>
          <th><strong>tanggal Setor</strong></th>
          <th><strong>Jenis Simpanan</strong></th>
          <th class="text-right"><strong>Kredit</strong></th>
          <th class="text-right"><strong>Debet</strong></th>
          <th class="text-right"><strong>Saldo</strong></th>
          <th><strong>Keterangan</strong></th>
          <th class="text-right"><strong>Action</strong></th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($simpan as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td>{{date('d-m-Y', strtotime($us->tanggalSetor))}}</td>
          <td>{{$us->simpanan_id->name}}</td>
          @if($us->mutasi =='Tambah')
          <td class="text-right">Rp {{number_format($us->nominal,0,",",".")}}</td>
          <td class="text-right">Rp 0</td>
          @else
          <td class="text-right">Rp 0</td>
          <td class="text-right">Rp {{number_format($us->nominal,0,",",".")}}</td>
          @endif
          <td class="text-right">Rp {{number_format($us->saldo,0,",",".")}}</td>
          <td>{{$us->ket}}</td>
          <td style="min-width:120px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_employee">Edit</a>
          </td>
        </tr>
        @endforeach
        <!-- <tr>
          <td colspan="9" class="text-center">KOSONG</td>
        </tr> -->
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>


    <div id="add_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Anggota</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tempat Lahir<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="tmp_lahir" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir"></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KTP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="nik" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NPWP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="npwp" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <input class="form-control" type="email" name="email" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Agama <span class="text-danger">*</span></label>
                    <select class="select" name="agama" required>
                      <option value="">Pilih Agama</option>
                      <option value="islam">Islam</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendidikan Terakhir <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="pendidikan" required>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status Dalam Keluarga<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="statuskeluarga" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ibu<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="nama_ibu" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ayah<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="nama_ayah" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <select class="select" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                      <option value="Laki-laki">LAKI-LAKI</option>
                      <option value="Perempuan">PEREMPUAN</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Alamat Anggota <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="alamat" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Komunitas <span class="text-danger">*</span></label>
                    <select class="select" name="komunitas" required>
                      <option value="">Pilih Komunitas</option>
                      <?php $komunitas = App\Komunitas::where('aktif', 1)->get(); ?>
                      @foreach($komunitas as $komu)
                      <option value="{{$komu->id}}">{{$komu->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Propinsi <span class="text-danger">*</span></label>
                    <select class="select" name="propinsi" required>
                      <option value="">Pilih Komunitas</option>
                      <?php $propinsi = App\Propinsi::where('aktif', 1)->get(); ?>
                      @foreach($propinsi as $pro)
                      <option value="{{$pro->id}}">{{$pro->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendapatan <span class="text-danger">*</span></label>
                    <select class="select" name="pendapatan" required>
                      <option value="">Pilih Pendapatan</option>
                      <?php $pendapatan = App\Pendapatan::where('aktif', 1)->get(); ?>
                      @foreach($pendapatan as $pend)
                      <option value="{{$pend->id}}">{{$pend->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pekerjaan <span class="text-danger">*</span></label>
                    <select class="select" name="pekerjaan" required>
                      <option value="">Pilih Pekerjaan</option>
                      <?php $pekerjaan = App\Pekerjaan::where('aktif', 1)->get(); ?>
                      @foreach($pekerjaan as $pek)
                      <option value="{{$pek->id}}">{{$pek->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Dokumen KTP<span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotoktp" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Diri<span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotodiri" required>
                  </div>
                </div>
              </div>
              <hr>
            <center><h3>SIMPANAN</h3></center>
            <hr>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Pokok<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="pokok" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Wajib<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="wajib" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Sukarela<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="sukarela" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Infaq<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="infaq" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Wakaf<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="wakaf" required>
                  </div>
                </div>
              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">Simpan Data Pegawai</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="edit_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Pegawai</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/admin/edit-data-pegawai/')}}" method="post" enctype="multipart/form-data">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NIP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="nip" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <input class="form-control" type="email" name="email" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Alamat <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="alamat" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tempat Lahir <span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="tmp_lahir" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value=""></div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <?php $jns_klm = [
                      'Laki-Laki',
                      'Perempuan',
                    ]; ?>
                    <select class="select" name="jenkel" required>
                      <option value="">Pilih Jenis Kelamin</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Agama <span class="text-danger">*</span></label>
                    <?php $agamas = [
                      'Islam',
                      'Kristen',
                      'Protestan',
                      'Hindu',
                      'Budha',
                    ]; ?>
                    <select class="select" name="agama" required>
                      <option value="">Pilih Agama</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status Kawin <span class="text-danger">*</span></label>
                    <?php $status_kwn = [
                      'Sudah Menikah',
                      'Belum Menikah',
                    ]; ?>
                    <select class="select" name="stts_kawin" required>
                      <option value="">Pilih Status Kawin</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jabatan <span class="text-danger">*</span></label>
                    <select class="select" name="jabatan" required>
                      <option value="">Pilih Jabatan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Satuan Kerja <span class="text-danger">*</span></label>
                    <select class="select" name="satuan_kerja_id" required>
                      <option value="">Pilih Satuan Kerja</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Masuk <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tmt_masuk" value="" required></div>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Golongan <span class="text-danger">*</span></label>
                    <select class="select" name="golongan" required>
                      <option value="">Pilih Golongan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status <span class="text-danger">*</span></label>
                    <select class="select" name="stts_pegawai" required>
                      <?php $status = [
                        'CPNS',
                        'PNS Tetap',
                        'Honorer',
                      ]; ?>
                      <option value="">Pilih Status</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto <span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="foto">
                  </div>
                </div>
                <div class="col-sm-12">
                  <div class="form-group">
                    <label class="control-label">Hak Akses <span class="text-danger">*</span></label>
                    <select class="select" name="type" required>
                      <?php $akses = [
                        'admin',
                        'kadinas',
                        'pegawai'
                      ]; ?>
                      <option value="">Pilih Hak Akses</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jabatan Atasan<span class="text-danger">*</span></label>
                    <select class="select" name="jabatan_penilai_id" required>
                      <option value="">Pilih Jabatan</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Satuan Kerja Atasan<span class="text-danger">*</span></label>
                    <select class="select" name="satuan_penilai_id" required>
                      <option value="">Pilih Satuan Kerja</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">Save Changes</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="delete_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Pegawai</h4>
          </div>
          <form action="{{url('/admin/hapus-data-pegawai/')}}" method="post">
            <div class="modal-body card-box">
              <p>Apakah yakin ingin di Hapus ???</p>
              <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-danger">Delete</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
