@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Data Anggota</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a>
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{url('/administrator/data-anggota')}}" method="post">
    @csrf
    <input type="hidden" name="action" value="cari">
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_anggota" value=""/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Nama Anggota</label>
        <input type="text" class="form-control floating" name="name" value=""/>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
    <div class="col-sm-4 col-xs-12"><br>
    <a href="#" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                  document.getElementById('exportdata').submit();"/></a>
                </div>
  </form>


</div>
<div class="row">
  <form class="" action="{{url('/administrator/export-data-anggota')}}" method="post" id="exportdata">
    @csrf
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No.Anggota</th>
          <th>Nama</th>
          <th>No. Telp</th>
          <th>Saldo Simpanan</th>
          <th>Saldo Transaksi</th>
          <th style="min-width:100px;" class="text-right">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1; ?>
        @foreach($user as $us)
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$us->no_anggota}}</td>
          <td>{{$us->name}}</td>
          <td>{{$us->telp}}</td>
          <td>Rp {{number_format($us->saldo,0,",",".")}}</td>
          <td>Rp {{number_format($us->saldotransaksi,0,",",".")}}</td>
          <td style="max-width:80px;" class="text-right">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_anggota{{$us->id}}">Edit</a>
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#detail{{$us->id}}">Detail</a>
          </td>
        </tr>
        @endforeach
        @if(count($user) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>

    </table>{!! $user->render() !!}


    <!-- <label class="col-lg-3 control-label">Light Logo</label> -->
    <!-- <form class="" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
      @csrf
      <input type="hidden" name="action" value="csv">
  		<div class="col-lg-3"><br>
        <span class="control-label">UPLOAD CSV</span>
          <input class="form-control rounded" type="file" name="upload">
  		</div>
      <div class="col-lg-9"><br>
  		</div>
      <div class="col-lg-2"><br>
          <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
  		</div>
    </form> -->
  </div>
  <div class="row col-md-6 table-responsive">
    <table class="table table-striped custom-table" width="40px">
      <thead>
        <tr>
          <th>Total Saldo Simpanan</th>
          <th>: Rp {{number_format($totalsimpanan,0,",",".")}}</th>
        </tr>
        <tr>
          <th>Total Saldo Transaksi</th>
          <th>: Rp {{number_format($totalsaldotransaksi,0,",",".")}}</th>
        </tr>
      </thead>
    </table>
  </div>
</div>
</div>
    </div>


    <div id="add_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Tambah Anggota</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data">
              <input type="hidden" name="action" value="tambah">
              @csrf
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Anggota<span class="text-danger">*</span></label>
                    <?php $noanggota = App\User::orderBy('no_anggota', 'DESC')->where('type', '<>', 'karyawan')->first();
                    $nomor = (int)$noanggota->no_anggota+1;
                    $sequence = substr($nomor, -6);
                    $anggota = date('Ym').$sequence; ?>
                    <input type="hidden" name="no_anggota" value="{{$anggota}}">
                    <label class="form-control" type="text" name="no_anggota" value="" disabled>{{$anggota}}</label>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KTP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="nik">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email <span class="text-danger">*</span></label>
                    <input class="form-control" type="email" name="email">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="telp">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tempat Lahir<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="tmp_lahir">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir <span class="text-danger">*</span></label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir"></div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NPWP<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="npwp">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Agama <span class="text-danger">*</span></label>
                    <select class="select" name="agama">
                      <option value="">Pilih Agama</option>
                      <option value="islam">Islam</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendidikan Terakhir <span class="text-danger">*</span></label>
                    <select class="select" name="pendidikan">
                      <option value="">Pilih Pendidikan</option>
                      <?php $pendidikans = App\Pendidikan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($pendidikans as $pendidikan)
                      <option value="{{$pendidikan->id}}">{{$pendidikan->name}}</option>
                      @endforeach
                    </select>
                    <!-- <input type="text" class="form-control" name="pendidikan"> -->
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status Dalam Keluarga<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="statuskeluarga">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ibu<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="nama_ibu">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ayah<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="nama_ayah">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin <span class="text-danger">*</span></label>
                    <select class="select" name="jenkel">
                      <option value="">Pilih Jenis Kelamin</option>
                      <option value="Laki-laki">LAKI-LAKI</option>
                      <option value="Perempuan">PEREMPUAN</option>
                    </select>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Alamat Anggota <span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="alamat">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kelurahan <a href="#" data-toggle="modal" data-target="#tambah_kelurahan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kelurahan">
                      <option value="">Pilih Kelurahan</option>
                      <?php $kelurahans = App\Kelurahan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kelurahans as $kelurah)
                      <option value="{{$kelurah->id}}">{{$kelurah->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kecamatan <a href="#" data-toggle="modal" data-target="#tambah_kecamatan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kecamatan">
                      <option value="">Pilih Kecamatan</option>
                      <?php $kecamatans = App\Kecamatan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kecamatans as $kecama)
                      <option value="{{$kecama->id}}">{{$kecama->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kabupaten <a href="#" data-toggle="modal" data-target="#tambah_kabupaten"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kabupaten">
                      <option value="">Pilih Kabupaten</option>
                      <?php $kabupatens = App\Kabupaten::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kabupatens as $kabu)
                      <option value="{{$kabu->id}}">{{$kabu->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Propinsi <a href="#" data-toggle="modal" data-target="#tambah_propinsi"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="propinsi">
                      <option value="">Pilih Propinsi</option>
                      <?php $propinsi = App\Propinsi::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($propinsi as $pro)
                      <option value="{{$pro->id}}">{{$pro->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Komunitas <a href="#" data-toggle="modal" data-target="#tambah_komunitas"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="komunitas">
                      <option value="">Pilih Komunitas</option>
                      <?php $komunitas = App\Komunitas::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($komunitas as $komu)
                      <option value="{{$komu->id}}">{{$komu->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendapatan <a href="#" data-toggle="modal" data-target="#tambah_pendapatan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="pendapatan">
                      <option value="">Pilih Pendapatan</option>
                      <?php $pendapatan = App\Pendapatan::where('aktif', 1)->get(); ?>
                      @foreach($pendapatan as $pend)
                      <option value="{{$pend->id}}">{{$pend->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pekerjaan <a href="#" data-toggle="modal" data-target="#tambah_pekerjaan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="pekerjaan">
                      <option value="">Pilih Pekerjaan</option>
                      <?php $pekerjaan = App\Pekerjaan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($pekerjaan as $pek)
                      <option value="{{$pek->id}}">{{$pek->name}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Dokumen KTP<span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotoktp">
                  </div>
                </div> -->
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Diri<span class="text-danger">*</span></label>
                    <input class="form-control" type="file" name="fotodiri">
                  </div>
                </div>
              </div>
              <!-- <hr>
            <center><h3>SIMPANAN</h3></center>
            <hr>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Pokok<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="pokok" value="" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Wajib<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="wajib" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Sukarela<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="sukarela" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Infaq<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="infaq" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Simpanan Wakaf<span class="text-danger">*</span></label>
                    <input type="text" class="form-control" name="wakaf" required>
                  </div>
                </div>
              </div> -->
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- DETAIL -->
    <div id="tambah_kelurahan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Kelurahan</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-kelurahan')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Kelurahan <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <div id="tambah_kecamatan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Kecamatan</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-kecamatan')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Kecamatan <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="tambah_kabupaten" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Kabupaten</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-kabupaten')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Kabupaten <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="tambah_propinsi" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Propinsi</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-propinsi')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Propinsi <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="tambah_komunitas" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Komunitas</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-komunitas')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Komunitas <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="tambah_pendapatan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Pendapatan</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-pendapatan')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Pendapatan <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div id="tambah_pekerjaan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Form Pekerjaan</h4>
          </div>
          <div class="modal-body">
            <form action="{{url('/administrator/data-pekerjaan')}}" method="post">
              <input type="hidden" name="action" value="tambah">
                @csrf
              <div class="form-group">
                <label>Nama Pekerjaan <span class="text-danger">*</span></label>
                <input class="input-sm form-control" required="" type="text" name="name">
              </div>
              <div class="m-t-20 text-center">
                <input class="btn btn-primary" type="submit" value="SIMPAN"/>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @foreach($user as $use)
    <div id="detail{{$use->id}}" class="modal custom-modal fade" role="dialog">
  <div class="modal-dialog">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-lg">
      <div class="modal-header">
        <h4 class="modal-title">Detail Data Anggota</h4>
      </div>
      <div class="modal-body">
          <table class="table">
            <tr>
              <td colspan="2" align="center"><img src="{{url('/laravel/public/foto/'.$use->fotodiri)}}" width="150px"/></td>
            </tr>
            <tr>
              <td>Nomor Anggota</td>
              <td>: {{$use->no_anggota}}</td>
            </tr>
            <tr>
              <td>Nomor KS212</td>
              <td>: {{$use->no_ks212}}</td>
            </tr>
            <tr>
              <td>Nama Lengkap</td>
              <td>: {{$use->name}}</td>
            </tr>
            <tr>
              <td>Email</td>
              <td>: {{$use->email}}</td>
            </tr>
            <tr>
              <td>Nomor Telpon</td>
              <td>: {{$use->telp}}</td>
            </tr>
            <tr>
              <td>No.NPWP</td>
              <td>: {{$use->npwp}}</td>
            </tr>
            <tr>
              <td>Tempat Lahir</td>
              <td>: {{$use->tpt_lahir}}</td>
            </tr>
            <tr>
              <td>Tanggal Lahir</td>
              <td>: {{date('d-m-Y', strtotime($use->tgl_lahir))}}</td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td>: {{$use->jenkel}}</td>
            </tr>
            <tr>
              <td>Agama</td>
              <td>: {{$use->agama}}</td>
            </tr>
            <tr>
              <td>Pendidikan</td>
              <td>:  @if($use->pendidikan > 0){{$use->pendidikan_id->name}} @else  @endif</td>
            </tr>
            <tr>
              <td>Status dalam keluarga</td>
              <td>: {{$use->statuskeluarga}}</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>: {{$use->alamat}}</td>
            </tr>
            <tr>
              <td>Kelurahan</td>
              <td>: @if($use->kelurahan > 0){{ $use->kelurahan_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Kecamatan </td>
              <td>: @if($use->kecamatan > 0){{ $use->kecamatan_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Kabupaten/Kota</td>
              <td>: @if($use->kabupaten > 0){{ $use->kabupaten_id->name }} @else  @endif</td>
            </tr>
            <tr>
              <td>Propinsi </td>
              <td>: @if($use->propinsi > 0){{ $use->propinsi_id->name }} @else @endif </td>
            </tr>
            <tr>
              <td><strong>SALDO SIMPANAN</strong> </td>
              <td><strong>: Rp. {{ number_format($use->saldo) }},-</strong></td>
            </tr>
            <tr>
              <td><strong>SALDO TRANSAKSI</strong> </td>
              <td><strong>: Rp. {{ number_format($use->saldotransaksi) }},-</strong></td>
            </tr>
            <tr>
              <td colspan="2"></td>
            </tr>
          </table>
      </div>
    </div>
  </div>
</div>
<!-- END DETAIL -->

<!-- EDIT -->
    <div id="edit_anggota{{$use->id}}" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h4 class="modal-title">Edit Data Pegawai</h4>
          </div>
          <div class="modal-body">
            <form class="m-b-30" action="{{url('/administrator/data-anggota')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" name="action" value="edit">
              <input type="hidden" name="ids" value="{{$use->id}}">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No.Anggota<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="no_anggota" value="{{$use->no_anggota}}" disabled>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Lengkap<span class="text-danger">*</span></label>
                    <input class="form-control" type="text" name="name" value="{{$use->name}}" required>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KS212</label>
                    <input class="form-control" type="text" name="no_ks212" value="{{$use->no_ks212}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">No. KTP</label>
                    <input class="form-control" type="text" name="nik" value="{{$use->nik}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Email </label>
                    <input class="form-control" type="email" name="email" value="{{$use->email}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nomor Telpon </label>
                    <input class="form-control" type="text" name="telp" value="{{$use->telp}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tempat Lahir</label>
                    <input class="form-control" type="text" name="tpt_lahir" value="{{$use->tpt_lahir}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Tanggal Lahir </label>
                    <div class="cal-icon"><input class="form-control datetimepicker" type="text" name="tgl_lahir" value="{{date('d-m-Y', strtotime($use->tgl_lahir))}}"></div>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">NPWP</label>
                    <input class="form-control" type="text" name="npwp" value="{{$use->npwp}}">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Agama </label>
                    <select class="select" name="agama">
                      <option value="islam">Islam</option>
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendidikan Terakhir </label>
                    <select class="select" name="pendidikan">
                      <?php $pendidikans = App\Pendidikan::where('aktif', 1)->get();?>
                      <option value="">Pilih Pendidikan Terakhir</option>
                      @foreach($pendidikans as $pendidikan)
                      @if($use->pendidikan == $pendidikan->id)
                      <option value="{{$pendidikan->id}}" selected>{{$pendidikan->name}}</option>
                      @else
                      <option value="{{$pendidikan->id}}">{{$pendidikan->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Status Dalam Keluarga</label>
                    <input type="text" class="form-control" name="statuskeluarga" value="{{$use->statuskeluarga}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ibu</label>
                    <input type="text" class="form-control" name="nama_ibu" value="{{$use->nama_ibu}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Nama Ayah</label>
                    <input type="text" class="form-control" name="nama_ayah" value="{{$use->nama_ayah}}">
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Jenis Kelamin </label>
                    <select class="select" name="jenkel">
                      <?php $jenkel = ['Laki-laki','Perempuan'] ;?>
                      <option value="">Pilih Jenis Kelamin</option>
                      @foreach($jenkel as $jen)
                      @if($use->jenkel == $jen)
                      <option value="{{$jen}}" selected>{{$jen}}</option>
                      @else
                      <option value="{{$jen}}">{{$jen}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Alamat Anggota </label>
                    <input class="form-control" type="text" name="alamat" value="{{$use->alamat}}">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kelurahan <a href="#" data-toggle="modal" data-target="#tambah_kelurahan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kelurahan">
                      <option value="">Pilih Kelurahan</option>
                      <?php $kelurahans = App\Kelurahan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kelurahans as $kelurah)
                      @if($kelurah->id==$use->kelurahan)
                      <option value="{{$kelurah->id}}" selected>{{$kelurah->name}}</option>
                      @else
                      <option value="{{$kelurah->id}}">{{$kelurah->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kecamatan <a href="#" data-toggle="modal" data-target="#tambah_kecamatan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kecamatan">
                      <option value="">Pilih Kecamatan</option>
                      <?php $kecamatans = App\Kecamatan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kecamatans as $kecama)
                      @if($kecama->id==$use->kecamatan)
                      <option value="{{$kecama->id}}" selected>{{$kecama->name}}</option>
                      @else
                      <option value="{{$kecama->id}}">{{$kecama->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Kabupaten <a href="#" data-toggle="modal" data-target="#tambah_kabupaten"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="kabupaten">
                      <option value="">Pilih Kabupaten</option>
                      <?php $kabupatens = App\Kabupaten::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($kabupatens as $kabu)
                      @if($kabu->id==$use->kabupaten)
                      <option value="{{$kabu->id}}" selected>{{$kabu->name}}</option>
                      @else
                      <option value="{{$kabu->id}}">{{$kabu->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Propinsi <a href="#" data-toggle="modal" data-target="#tambah_propinsi"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="propinsi">
                      <option value="">Pilih Propinsi</option>
                      <?php $propinsi = App\Propinsi::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($propinsi as $pro)
                      @if($pro->id==$use->propinsi)
                      <option value="{{$pro->id}}" selected>{{$pro->name}}</option>
                      @else
                      <option value="{{$pro->id}}">{{$pro->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Komunitas <a href="#" data-toggle="modal" data-target="#tambah_komunitas"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="komunitas">
                      <option value="">Pilih Komunitas</option>
                      <?php $komunitas = App\Komunitas::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($komunitas as $komu)
                      @if($komu->id==$use->komunitas)
                      <option value="{{$komu->id}}" selected>{{$komu->name}}</option>
                      @else
                      <option value="{{$komu->id}}">{{$komu->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pendapatan <a href="#" data-toggle="modal" data-target="#tambah_pendapatan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="pendapatan">
                      <option value="">Pilih Pendapatan</option>
                      <?php $pendapatan = App\Pendapatan::where('aktif', 1)->get(); ?>
                      @foreach($pendapatan as $pend)
                      @if($pend->id == $use->pendapatan)
                      <option value="{{$pend->id}}" selected>{{$pend->name}}</option>
                      @else
                      <option value="{{$pend->id}}">{{$pend->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Pekerjaan <a href="#" data-toggle="modal" data-target="#tambah_pekerjaan"> &nbsp;&nbsp;<i class="fa fa-plus"></i></a> </label>
                    <select class="select" name="pekerjaan">
                      <option value="">Pilih Pekerjaan</option>
                      <?php $pekerjaan = App\Pekerjaan::where('aktif', 1)->orderBy('name', 'ASC')->get(); ?>
                      @foreach($pekerjaan as $pek)
                      @if($pek->id == $use->pekerjaan)
                      <option value="{{$pek->id}}" selected>{{$pek->name}}</option>
                      @else
                      <option value="{{$pek->id}}">{{$pek->name}}</option>
                      @endif
                      @endforeach
                    </select>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Saldo</label>
                    <input class="form-control" type="text" value="{{$use->saldo}}" name="saldo">
                  </div>
                </div>
                <!-- <div class="col-sm-6">
                  <div class="form-group">
                    <label class="control-label">Foto Diri</label>
                    <input class="form-control" type="file" name="fotodiri">
                  </div>
                </div> -->
              </div>
              <div class="m-t-20 text-center">
                <button class="btn btn-primary">SIMPAN</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    @endforeach
<!-- END EDIT -->
<!-- DELETE -->
    <div id="delete_employee" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title">Hapus Data Pegawai</h4>
          </div>
          <form action="{{url('/admin/hapus-data-pegawai/')}}" method="post">
            <div class="modal-body card-box">
              <p>Apakah yakin ingin di Hapus ???</p>
              <div class="m-t-20"> <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-danger">Delete</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- END DELETE -->
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
