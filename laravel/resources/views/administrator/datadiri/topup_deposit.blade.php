@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-xs-4">
          <h4 class="page-title">TOPUP DEPOSIT</h4>
        </div>
        <div class="col-xs-8 text-right m-b-30">
          <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#panduan"></i>PANDUAN TOPUP</a>
          <div class="view-icons">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8 col-md-6 col-xs-12">
          <form class="" action="{{url('/administrator/topup-deposit')}}" method="post">
            <input type="hidden" name="action" value="topup">
            @csrf
          <div class="form-group">
            <select class="form-control" name="bank" id="bank" required>
              <option value="">Pilih Bank Tujuan</option>
              <?php $banks = App\Bank::where('aktif', 1)->get(); ?>
              @foreach($banks as $bank)
                <option value="{{$bank->bank}}">{{$bank->bank}}</option>
              @endforeach
            </select>
          </div>
          <script type="text/javascript">
            $("select[name='bank']").change(function(){
                $(document).ajaxStart(function(){
                    $("#wait").css("display", "block");
                });
                $(document).ajaxComplete(function(){
                    $("#wait").css("display", "none");
                });
                $("button").click(function(){
                    $("#txt").load("demo_ajax_load.asp");
                });
              // alert('change');
                var bank = $(this).val();
                var token = $("input[name='_token']").val();
                $.ajax({
                    url: "<?php echo route('select-bank') ?>",
                    method: 'POST',
                    data: {bank:bank, _token:token},
                    success: function(data) {
                      console.log(data);
                      $("#rekening").html("");
                      $("#rekening").append(data);
                      $("#divrekening").show();
                    }
                });
            });
          </script>
          <div class="form-group" id="divrekening">
            <label id="rekening" class="form-control" name="rekening"></label>
          </div>
          <script>
            $("#divrekening").hide();
          </script>
          <script language="javascript"> function convertToRupiah(angka){ var rupiah = '';
                      var angkarev = angka.toString().split('').reverse().join(''); for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah +=
                      angkarev.substr(i,3)+'.'; return rupiah.split('',rupiah.length-1).reverse().join('');
                      }
                      function rupiah(){ var nominal = document.getElementById("nominal").value; var rupiah = convertToRupiah(nominal);
                      document.getElementById("nominal").value = rupiah; }
                      </script>
                      <script> function convertToRupiah (objek) {
                       separator = ".";
                       a = objek.value;
                       b = a.replace(/[^\d]/g,"");
                       c = "";
                       panjang = b.length;
                       j = 0; for (i = panjang; i > 0; i--) {
                       j = j + 1; if (((j % 3) == 1) && (j != 1)) {
                       c = b.substr(i-1,1) + separator + c; } else {
                       c = b.substr(i-1,1) + c; } } objek.value = c; }
                       </script>
          <div class="form-group">
              <input type="text" name="nominal" class="form-control" value="" onkeyup="convertToRupiah(this)" placeholder="Nominal Transfer" id="nominal" required>
          </div>
          <div class="form-group" id="buttom">
            <input type="submit" class="form-control btn btn-primary" name="btn" value="P R O S E S">
          </div>
        </form>
        </div>
        <div class="col-sm-8 col-md-6 col-xs-12">
          <div class="row">
            <div class="col-xs-12">
              <table width="100%" class="table table-striped custom-table">
                <tr>
                  <td>
                    <h4 class="page-title text-center"> MENU LAIN</h4>
                  </div>
                    @foreach($menus as $menu)
                    <div class="col-sm-6 col-md-6 col-xs-6">
                        <a href="{{url('administrator/'.$menu->route)}}"><button type="button" name="button"  class="btn btn-success btn-sm m-t-10 form-control">{{$menu->menu}}</button></a>
                    </div>
                    @endforeach
                  </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
        <center> <div id="wait" style="display:none;width:50%;border:0px solid black;position:absolute;top:50%;padding:5px;"><img src="{{url('images/load.gif')}}" width="100%" /></div></center>
    </div>
    <div class="col-sm-8 col-md-12 col-xs-12">
      <div class="table-responsive">
        <table class="table table-striped custom-table">
          <thead>
          <tr>
            <th>No.</th>
            <th>Tgl Transaksi</th>
            <th>No. Transaksi</th>
            <th>Bank</th>
            <th>Nominal</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
          <?php $no=1; ?>
          @foreach($datas as $data)
          <tr>
            <td>{{$no++}}.</td>
            <td>{{date('d-m-Y', strtotime($data->tgl_trx))}}</td>
            <td>{{$data->no_trx}}</td>
            <td>{{$data->bank}}</td>
            <td>Rp {{number_format($data->nominal,0,",",".")}}</td>
            <td>@if($data->status ==1)<span class="label label-warning-border">{{$data->statusId->status}}</span>@elseif($data->status ==2)<span class="label label-success-border">{{$data->statusId->status}}</span>@else <span class="label label-danger-border">{{$data->statusId->status}}</span>@endif</td>
          </tr>
          @endforeach
        </tbody>
        </table>
      </div>

    </div>
    <div id="panduan" class="modal custom-modal fade" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content modal-md">
          <div class="modal-header">
            <h4 class="modal-title"> PANDUAN TOPUP DEPOSIT</h4>
          </div>
            @csrf
            <div class="modal-body card-box">
              <table width="100%" class="table table-striped">
                <tr>
                  <td valign="top"><p>1.</p></td><?php $digit = substr(Auth::user()->no_anggota, -4); ?>
                  <td><p style="text-align:justify;">Lakukan transfer minimal Rp 50,000, di tambahkan 4 digit terakhir nomor anggota anda.
                  <br>Contoh : Nomor anggota anda adalah {{Auth::user()->no_anggota}}  maka nominal transfernya di tambahkan <strong>{{$digit}}</strong></p></td>
                </tr>
                <tr>
                  <td valign="top"><p>2.</p></td>
                  <td><p style="text-align:justify;">Lakukan transfer Kerekening kami dibawah<br>
                    @foreach($banks as $b)
                  - {{$b->bank}} : {{$b->no_rekening}}<br>
                  @endforeach
                </tr>
                <tr>
                  <td valign="top"><p>3.</p></td>
                  <td><p style="text-align:justify;">Setelah Transfer, Lakukan pengisian Form Topup Deposit diatas sesuai dengan yang di transfer.</p></td>
                </tr>
                <tr>
                  <td valign="top"><p>4.</p></td>
                  <td><p style="text-align:justify;">Tunggu 5 - 10 Menit, saldo akan otomatis bertambah.</p></td>
                </tr>
              </table>
              <div class="m-t-20"> <a href="#" class="btn btn-danger" data-dismiss="modal">Close</a>
              </div>
            </div>
        </div>
      </div>
    </div>
</div>
@endsection
