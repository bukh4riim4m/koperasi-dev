@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Buku Saldo Transaksi</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_employee"><i class="fa fa-plus"></i> Tambah Anggota</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>
<div class="row filter-row">
  <form class="form" action="{{('/administrator/buku-saldo-transaksi')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf
    <div class="col-sm-4 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Periode Dari</label>
				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}" required/></div>
			</div>
		</div>
    <div class="col-sm-4 col-xs-6">
			<div class="form-group form-focus">
				<label class="control-label">Sampai</label>
				<div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$until}}" required/></div>
			</div>
		</div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">
<div class="card-box m-b-0">
  <div class="row">
    <div class="col-md-12">
      <div class="profile-view">
        <div class="profile-img-wrap">
          <div class="profile-img">
            <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.$users->fotodiri) }}" alt=""></a>
          </div>
        </div>
        <div class="profile-basic">
          <div class="row">
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">
                  <tr>
                    <td>Nomor Anggota</td>
                    <td>: @if($users->no_anggota==null) @else {{$users->no_anggota}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nama Lengkap</td>
                    <td>: @if($users->name==null) @else {{$users->name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>: @if($users->email==null) @else {{$users->email}} @endif</td>
                  </tr>
                  <tr>
                    <td>Nomor Telpon</td>
                    <td>: @if($users->telp==null) @else {{$users->telp}} @endif</td>
                  </tr>
                  <tr>
                    <td>No.NPWP</td>
                    <td>: @if($users->npwp==null) @else {{$users->npwp}} @endif</td>
                  </tr>
                  <tr>
                    <td>Tempat Lahir	</td>
                    <td>: @if($users->tpt_lahir==null) @else {{$users->tpt_lahir}} @endif</td>
                  </tr>
                  <tr>
                    <td>Tanggal Lahir	</td>
                    <td>: @if($users->tgl_lahir==null) @else {{date('d-m-Y', strtotime($users->tgl_lahir))}} @endif</td>
                  </tr>
                </table>
              </div>
            </div>
            <div class="col-md-6">
              <div class="table-responsive">
                <table class="table">


                  <tr>
                    <td>Jenis Kelamin	</td>
                    <td>: @if($users->jenkel==null) @else {{$users->jenkel}} @endif</td>
                  </tr>
                  <tr>
                    <td>Agama</td>
                    <td>: @if($users->agama==null) @else {{$users->agama}} @endif</td>
                  </tr>
                  <tr>
                    <td>Pendidikan</td>
                    <td>: @if($users->pendidikan==null) @else {{$users->pendidikan_id->name}} @endif</td>
                  </tr>
                  <tr>
                    <td>Status dalam keluarga	</td>
                    <td>: @if($users->statuskeluarga==null) @else {{$users->statuskeluarga}} @endif</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>: @if($users->alamat==null) @else {{$users->alamat}} @endif</td>
                  </tr>
                  <tr>
                    <td>Komunitas</td>
                    <td>: @if($users->komunitas==null) @else {{$users->komunitas_id->name}} @endif</td>
                  </tr>
                  <tr>
                    <td><strong>SALDO TRANSAKSI</strong></td>
                    <td><strong>: Rp {{number_format($users->saldotransaksi,0,",",".")}}</strong></td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>

  <div class="card-box tab-box">
</div>
    <div id="tasks" class="tab-pane fade in active">
      <div class="project-task">
        <div class="tabbable">
          <div class="tab-content">
          <div class="tab-pane active" id="pokok">
              <div class="task-wrapper">
                <div class="task-list-container">
                  <div class="">
                    <ul id="task-list">
                      <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                          <tr>
                            <th>No.</th>
                            <th>Tanggal Transaksi</th>
                            <th>Nomor Transaksi</th>
                            <th>Kredit</th>
                            <th>Debet</th>
                            <th>Saldo</th>
                            <th>Keterangan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php $no=1;$masuk=0;$keluar=0;$saldos=0;$saldosawal=0;$saldosakhir=0;$maxs=array();$maks=array();?>
                          @foreach($datasppob as $key => $datas)
                          <?php if ($datas->mutasi =='Kredit') {
    $masuk+=$datas->nominal;
    if ($key ==0) {
        $saldosawal = $datas->saldo - $datas->nominal;
    }
} else {
    $keluar+=$datas->nominal;
    if ($key ==0) {
        $saldosawal = $datas->saldo + $datas->nominal;
    }
};
                          $maxs[]=$datas->id;
                          $jum_id = max($maxs);
                          $saldosakhir = $datas->saldo;?>

                          <tr>
                            <td>{{$no++}}</td>
                            <td>{{date('d-m-Y', strtotime($datas->tgl_trx))}}</td>
                            <td>{{$datas->no_trx}}</td>
                            @if($datas->mutasi =='Kredit')
                            <td>-</td>
                            <td>Rp {{number_format($datas->nominal,0,",",".")}}</td>
                            @else
                            <td>Rp {{number_format($datas->nominal,0,",",".")}}</td>
                            <td>-</td>
                            @endif
                            <td>Rp {{number_format($datas->saldo,0,",",".")}}</td>
                            <td>{{$datas->keterangan}}</td>
                          </tr>
                          @endforeach
                        </tbody>
                      </table>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="row col-md-6 table-responsive">
                <table class="table table-striped custom-table" width="40px">
                  <thead>
                    <tr>
                      <th><strong>Saldo Awal</strong></th>
                      <th><strong>: Rp {{number_format($saldosawal,0,",",".")}}</strong></th>
                    </tr>
                    <tr>
                      <th>Total Debet</th>
                      <th>: Rp {{number_format($masuk,0,",",".")}}</th>
                    </tr>
                    <tr>
                      <th>Total Kredit</th>
                      <th>: Rp {{number_format($keluar,0,",",".")}}</th>
                    </tr>
                    <tr>
                      <th><strong><font color="green">Saldo Akhir</font></strong></th>
                      <th><strong><font color="green">: Rp {{number_format($saldosakhir,0,",",".")}}</font></strong></th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>

      </div>
    </div>
  </div>
</div>


</div>
</div>
@endsection
