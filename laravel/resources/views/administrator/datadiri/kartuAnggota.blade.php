@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-12">
  <h4 class="page-title">Kartu Saya</h4>
</div>
<!-- <div class="col-xs-8 text-right m-b-30"> -->
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#edit_profil"><i class="fa fa-edit"></i> Edit Profil</a> -->
  <!-- <div class="view-icons"> -->
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  <!-- </div>
</div> -->
</div>

<div class="card-box m-b-0">
<div class="row">
  <div class="col-md-12">
    <div class="profile-view">

      <!-- <div class="profile-img-wrap">
        <div class="profile-img">
          <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.Auth::user()->fotodiri) }}" alt=""></a>
        </div>
      </div> -->

      <div class="profile-basic">
        <div class="row">
          <div class="col-md-12">
            <div class="">

              <div class="col-md-12 col-sm-12 col-xs-12 col-lg-9">
                <div class="profile-widget">
                  <div class="profile-imges">
                    <a href="#"  data-toggle="modal" data-target="#lihat{{Auth::user()->id}}">@if(Auth::user()->fotodiri !==null)<img src="{{url('laravel/public/foto/'.Auth::user()->fotodiri)}}" width="20%" style="margin-bottom:-53%;margin-left:-23%;margin-right:50%"><center><img src="{{url('/laravel/public/kartu/new.png')}}" width="100%" id="img1" style="margin-top:-17px;"/></center>@else <img src="{{url('/laravel/public/kartu/new.png')}}" width="100%" id="img1" style="margin-top:1px;"/>@endif</a>
                  </div>
                  <h4 class="user-name m-t-10 m-b-0 text-ellipsis">{{Auth::user()->no_anggota}}</h4>
                  <h5 class="user-name m-t-10 m-b-0 text-ellipsis">{{Auth::user()->name}}</h5>
                  <div class="small text-muted"></div>
                  <a href="{{url('/administrator/Download-kartu/'.Auth::user()->sequence)}}" class="btn btn-primary btn-sm m-t-10">Download</a>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
</div>

@endsection
