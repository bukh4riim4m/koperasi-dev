@extends('layouts.admin.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="row">
            <div class="col-sm-12 text-center">

              <?php
              $dataPoints=array();
              $total=1;
              foreach ($kabupatens as $kabupaten) {
                $users = \DB::select("SELECT SUM(users.aktif) as totalkabupaten FROM users WHERE users.aktif ='1' AND users.kabupaten = $kabupaten->id AND users.kabupaten <> ''");

                 array_push($dataPoints,array("label"=> "$kabupaten->name", "y"=> $users[0]->totalkabupaten));
                 $total+=count($kabupatens);
              }

              // $dataPoints = array(
              //   array("label"=> "Pokok", "y"=> $pokoks[0]->totalpokok),
              //   array("label"=> "Wajib", "y"=> $wajins[0]->totalwajib),
              //   array("label"=> "Sukarela", "y"=> $sukarelas[0]->totalsukarela),
              //   array("label"=> "Investasi", "y"=> $investasis[0]->totalinvestasi),
              //   array("label"=> "Wakaf", "y"=> $wakafs[0]->totalwakaf),
              //   array("label"=> "Infaq", "y"=> $infaqs[0]->totalinfak),
              //   array("label"=> "SHU", "y"=> $shus[0]->totalshu),
              //   array("label"=> "Lain-Lain", "y"=> $lains[0]->totallain)
              // );

              ?>
              <script>
              window.onload = function() {


              var chart = new CanvasJS.Chart("chartContainer", {
                animationEnabled: true,
                title: {
                  text: "Statistik Kabupaten"
                },
                subtitles: [{
                  text: ""
                }],
                data: [{
                  type: "pie",
                  showInLegend: "true",
                  legendText: "{label}",
                  indexLabelFontSize: 16,
                  indexLabel: "{label} - #percent%",
                  yValueFormatString: "#,##0 Orang",
                  dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                }]
              });
              chart.render();

              }
              </script>
              <div id="chartContainer" style="height: 100%; width: 100%;"></div>
              <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
              </div>
            </div>
          </div>
      </div>
    </div>
</div>
@endsection
