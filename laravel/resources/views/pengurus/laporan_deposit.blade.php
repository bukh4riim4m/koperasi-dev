@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Laporan Deposit</h4>
</div>
<div class="col-xs-8">
  <a href="#" class="btn btn-primary pull-right rounded" onclick="event.preventDefault();
                document.getElementById('download').submit();"> Download</a>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{route('laporan-deposit-anggota')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{date('d-m-Y', strtotime($dari))}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{date('d-m-Y', strtotime($sampai))}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-3 col-xs-12">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Status</label>
        <?php $statuses = App\Statusdeposit::where('id', '<>', 1)->get(); ?>
        <select class="select floating" name="status">
          <option value="">-- Semua --</option>
          @foreach($statuses as $status)
            @if($stts == $status->id)
              <option value="{{$status->id}}" selected> {{$status->status}} </option>
            @else
              <option value="{{$status->id}}"> {{$status->status}} </option>
            @endif
          @endforeach
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{route('laporan-deposit-anggota')}}" method="post" id="download">
    @csrf
    <input type="hidden" name="dari" value="{{$dari}}"/>
    <input type="hidden" name="sampai" value="{{$sampai}}"/>
    <input type="hidden" name="status" value="{{$stts}}"/>
    <input type="hidden" name="action" value="cari"/>
    <input type="hidden" name="export" value="download">
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No Anggota</th>
          <th>Tgl. Transaksi</th>
          <th>No.Transaksi</th>
          <th>Bank</th>
          <th>Nominal</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody><?php $no=1; ?>
        @foreach($deposits as $deposit)
        <tr>
          <td>{{$no++}}</td>
          <td>{{$deposit->no_anggota}}</td>
          <td>{{date('d-m-Y', strtotime($deposit->tgl_trx))}}</td>
          <td>{{$deposit->no_trx}}</td>
          <td>{{$deposit->bank}}</td>
          <td>{{number_format($deposit->nominal,0,",",".")}}</td>
          <td>@if($deposit->status ==1)<span class="label label-warning-border">{{$deposit->statusId->status}}</span>@elseif($deposit->status ==2)<span class="label label-success-border">{{$deposit->statusId->status}}</span>@else <span class="label label-danger-border">{{$deposit->statusId->status}}</span>@endif</td>
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
