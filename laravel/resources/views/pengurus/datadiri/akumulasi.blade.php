@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Akumulasi Belanja</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <div class="view-icons">
  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{('/pengurus/my-akumulasi-belanja')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-4 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="dari" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-4 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="sampai" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{('#')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="nip" value=""/>
    <input type="hidden" name="hakakses" value=""/>
    <input type="hidden" name="jabatan" value=""/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table datatable">
      <thead>
        <tr>
          <th>No.</th>
          <th>No. Anggota</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>Nominal</th>
          <th>Gerai</th>
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $total=0;?>
        @foreach($akumulasis as $akumulasi)
        <?php $total+= $akumulasi->nominal;?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$akumulasi->no_kssd}}</td>
          <td>{{$akumulasi->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($akumulasi->tgl_trx))}}</td>
          <td>Rp {{number_format($akumulasi->nominal,0,",",".")}}</td>
          <td>{{$akumulasi->gerai}}</td>
        </tr>
        @endforeach
        @if(count($akumulasis) < 1)
        <tr>
          <td colspan="6" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Akumulasi</td>
    <td>: Rp {{number_format($total,0,",",".")}}</td>
  </tr>
</table>
</div>
</div>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>
@endsection
