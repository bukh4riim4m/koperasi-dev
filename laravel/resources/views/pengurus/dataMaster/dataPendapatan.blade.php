@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-12">
		<h4 class="page-title">Data Pendapatan</h4>
	</div>
  <!-- <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah Pendapatan</a>
  </div> -->
</div>

<div class="row filter-row">
</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>Pendapatan</th>
						<!-- <th>Action</th> -->
					</tr>
				</thead>
				<tbody>
          <?php $no=1; ?>
          @foreach($pendapatans as $kel)
					<tr class="holiday-completed">
						<td>{{$no++}}.</td>
						<td>{{$kel->name}}</td>
						<!-- <td style="min-width:150px;">
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas{{$kel->id}}">Edit</a>
							<a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_komunitas{{$kel->id}}">Hapus</a>
						</td> -->
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
	</div>
</div>

  </div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
