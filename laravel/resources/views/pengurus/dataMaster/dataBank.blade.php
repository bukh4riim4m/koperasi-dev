@extends('layouts.pengurus.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">


<div class="row">
	<div class="col-xs-12">
		<h4 class="page-title">Data Rekening</h4>
	</div>
  <!-- <div class="col-xs-6">
    <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i> Tambah Rekening</a>
  </div> -->
</div>

<div class="row filter-row">

</div>

<div class="row">
	<div class="col-sx-6 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped custom-table">
			<!-- <a href="{{url('#')}}" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_golongan_pegawai"><i class="fa fa-plus"></i>Tambah Golongan</a> -->

				<thead>
					<tr>
						<th>No.</th>
						<th>Bank</th>
            <th>Nomor Rekening</th>
            <th>Atas Nama</th>
					</tr>
				</thead>
				<tbody>
          <?php $no=1; ?>
          @foreach($rekenings as $rekening)
					<tr class="holiday-completed">
						<td>{{$no++}}.</td>
						<td>{{$rekening->bank}}</td>
            <td>{{$rekening->no_rekening}}</td>
            <td>{{$rekening->atas_nama}}</td>
					</tr>
          @endforeach
				</tbody>

			</table>
		</div>
	</div>
</div>

</div>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
</div>
</div>
@endsection
