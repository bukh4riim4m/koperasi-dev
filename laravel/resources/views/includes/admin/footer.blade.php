<script type='text/javascript'>
//<![CDATA[
//Script Redirect CTRL + U
//https://mastamvan.blogspot.com/ ganti dengan url blog kalian
function redirectCU(e) {
  if (e.ctrlKey && e.which == 85) {
    alert('Tidak diperbolehkan CTRL+U');
    return false;
  }
}
document.onkeydown = redirectCU;

// //Script Redirect Klik Kanan
function redirectKK(e) {
  if (e.which == 3) {
    alert('Tidak diperbolehkan Klik Kanan');
    return false;
  }
}
document.oncontextmenu = redirectKK;
//]]>
</script>
<div class="sidebar-overlay" data-reff="#sidebar"></div>
    <script type="text/javascript" src="{{url('js/jquery-3.2.1.min.js')}}"></script>
    <!-- <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script> -->
<script type="text/javascript" src="{{url('js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/jquery.slimscroll.js')}}"></script>
<script type="text/javascript" src="{{url('js/select2.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/bootstrap-datetimepicker.min.js')}}"></script>
<script type="text/javascript" src="{{url('js/app.js')}}"></script>
<script type="text/javascript" src="{{url('plugins/summernote/dist/summernote.min.js')}}"></script>
<!-- <script src="//code.jquery.com/jquery.js"></script> -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script>
    $('#flash-overlay-modal').modal();
</script>
<script>
$(document).ready(function(){
$('.summernote').summernote({
height: 200,                 // set editor height
minHeight: null,             // set minimum height of editor
maxHeight: null,             // set maximum height of editor
focus: false                 // set focus to editable area after initializing summernote
});
});
</script>
<!-- ==== -->
{{--<script type="text/javascript" src="{{url('plugins/morris/morris.min.js')}}"></script>--}}
<script type="text/javascript" src="{{url('plugins/raphael/raphael-min.js')}}"></script>
<script>
    var data = [
    { y: '2014', a: 50, b: 90},
    { y: '2015', a: 65,  b: 75},
    { y: '2016', a: 50,  b: 50},
    { y: '2017', a: 75,  b: 60},
    { y: '2018', a: 80,  b: 65},
    { y: '2019', a: 90,  b: 70},
    { y: '2020', a: 100, b: 75},
    { y: '2021', a: 115, b: 75},
    { y: '2022', a: 120, b: 85},
    { y: '2023', a: 145, b: 85},
    { y: '2024', a: 160, b: 95}
  ],
  config = {
    data: data,
    xkey: 'y',
    ykeys: ['a', 'b'],
    labels: ['Total Income', 'Total Outcome'],
    fillOpacity: 0.6,
    hideHover: 'auto',
    behaveLikeLine: true,
    resize: true,
    pointFillColors:['#ffffff'],
    pointStrokeColors: ['black'],
    gridLineColor: '#eef0f2',
    lineColors:['gray','orange']
  };
config.element = 'area-chart';
Morris.Area(config);
config.element = 'line-chart';
Morris.Line(config);
config.element = 'bar-chart';
Morris.Bar(config);
config.element = 'stacked';
config.stacked = true;
Morris.Bar(config);
Morris.Donut({
  element: 'pie-chart',
  data: [
  {label: "Employees", value: 30},
  {label: "Clients", value: 15},
  {label: "Projects", value: 45},
  {label: "Tasks", value: 10}
  ]
});
</script>
</body>
</html>
