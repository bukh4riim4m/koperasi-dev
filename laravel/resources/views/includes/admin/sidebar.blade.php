<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  @if($dashboard =='dashboard')<li class="active">@else <li>@endif
    <a href="{{url('/administrator/home')}}">DASHBOARD ADMIN</a>
  </li>
  @if($dashboard =='dataMaster')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA MASTER</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/data-pendidikan')}}">&nbsp;&nbsp;&nbsp;Data Pendidikan</a></li>
      <li><a href="{{url('/administrator/data-komunitas')}}">&nbsp;&nbsp;&nbsp;Data Komunitas</a></li>
      <li><a href="{{url('/administrator/data-kelurahan')}}">&nbsp;&nbsp;&nbsp;Data Kelurahan</a></li>
      <li><a href="{{url('/administrator/data-kecamatan')}}">&nbsp;&nbsp;&nbsp;Data Kecamatan</a></li>
      <li><a href="{{url('/administrator/data-kabupaten')}}">&nbsp;&nbsp;&nbsp;Data Kabupaten</a></li>
      <li><a href="{{url('/administrator/data-propinsi')}}">&nbsp;&nbsp;&nbsp;Data Propinsi</a></li>
      <li><a href="{{url('/administrator/data-pendapatan')}}">&nbsp;&nbsp;&nbsp;Data Pendapatan</a></li>
      <li><a href="{{url('/administrator/data-pekerjaan')}}">&nbsp;&nbsp;&nbsp;Data Pekerjaan</a></li>
      <li><a href="{{url('/administrator/data-jenis-simpanan')}}">&nbsp;&nbsp;&nbsp;Jenis Simpanan</a></li>
      <li><a href="{{url('/administrator/data-rekening')}}">&nbsp;&nbsp;&nbsp;Data Rekening</a></li>
    </ul>
  </li>
  @if($dashboard =='dataPengguna')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA PENGGUNA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/data-anggota')}}">&nbsp;&nbsp;&nbsp;Data Anggota</a></li>
      <li><a href="{{url('/administrator/data-group')}}">&nbsp;&nbsp;&nbsp;Data Group</a></li>
      <li><a href="{{url('/administrator/data-karyawan')}}">&nbsp;&nbsp;&nbsp;Data Karyawan</a></li>
      <!-- <li><a href="{{url('/administrator/simpanan-jatuh-tempo')}}">&nbsp;&nbsp;&nbsp;Data Jatuh Tempo</a></li> -->
    </ul>
  </li>
  @if($dashboard =='dataSimpanan')<li class="active">@else <li>@endif
    <a href="{{url('/administrator/data-simpanan')}}">SIMPANAN ANGGOTA</a>
  </li>
  @if($dashboard =='Simpananwajib')<li class="active">@else <li>@endif
    <a href="{{url('/administrator/status-simpanan-wajib')}}">STATUS SIMPANAN WAJIB</a>
  </li>

  @if($dashboard =='akumulasi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <li><a href="{{url('/administrator/saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li>
    </ul>
  </li>
  @if($dashboard =='kartuAnggota')<li class="active">@else <li>@endif
    <a href="{{url('/administrator/kartu-anggota')}}">KARTU ANGGOTA</a>
  </li>
  @if($dashboard =='tokoOnline')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>PRODUK ONLINE</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('harga-ppob')}}">&nbsp;&nbsp;&nbsp;Produk PPOB</a></li>
      <li><a href="{{url('/administrator/produk')}}">&nbsp;&nbsp;&nbsp;Produk Barang</a></li>
    </ul>
  </li>
  <?php $pesanans = App\Pesanan::whereBetWeen('status',[2,3])->where('aktif', 1)->get(); ?>
  @if($dashboard =='pemesanan')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>PEMESANAN PRODUK</span> &nbsp;@if(count($pesanans) > 0)<span class="noti-dot active"></span>@endif <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <?php $dipesan = App\Pesanan::where('status',2)->where('aktif', 1)->sum('aktif'); ?>
      <li><a href="{{route('administrator-permintaan-barang')}}">&nbsp;&nbsp;&nbsp;Barang dipesan @if($dipesan > 0)<span class="badge bg-primary pull-right">{{ $dipesan }}</span>@endif</a></li>
      <?php $kirim = App\Pesanan::where('status',3)->where('aktif', 1)->sum('aktif'); ?>
      <li><a href="{{route('administrator-kirim-barang')}}">&nbsp;&nbsp;&nbsp;Barang dikirim @if($kirim > 0)<span class="badge bg-primary pull-right">{{ $kirim }}</span>@endif</a></li>

    </ul>
  </li>
  @if($dashboard =='laporan')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>LAPORAN PENJUALAN</span> &nbsp;<span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('administrator-laporan-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('administrator-laporan-barang')}}">&nbsp;&nbsp;&nbsp;Laporan Barang</a></li>
      <!-- <li><a href="{{route('administrator-laporan-penjualan')}}">&nbsp;&nbsp;&nbsp;Laporan Penjualan</a></li> -->
    </ul>
  </li>
  <?php $depo = App\Deposit::where('status', 1)->where('aktif', 1)->get(); ?>
  @if($dashboard =='Konfirmasideposit')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>KONFIRMASI DEPOSIT</span>&nbsp;@if(count($depo) > 0)<span class="noti-dot active"></span>@endif <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('konfirmasi-deposit')}}">&nbsp;&nbsp;&nbsp;Konfirmasi Deposit @if(count($depo) > 0)<span class="badge bg-primary pull-right">{{ count($depo) }}</span>@endif</a></li>
      <li><a href="{{route('laporan-deposit')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li>
    </ul>
  </li>
  @if($dashboard =='bukuSaldo')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>BUKU SALDO ANGGOTA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('buku-saldo-transaksi-anggota')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>
  @if($dashboard =='datasuplayer')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>SUPLAYER</span><span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-suplayer')}}">&nbsp;&nbsp;&nbsp;Topup Saldo Suplayer</a></li>
      <li><a href="{{route('buku-saldo-suplayer')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Suplayer</a></li>
      <!-- <li><a href="#">&nbsp;&nbsp;&nbsp;Data Transaksi Suplayer</a></li> -->
      <!-- <li><a href="#">&nbsp;&nbsp;&nbsp;Laporan Suplayer</a></li> -->
    </ul>
  </li>

  @if($dashboard =='statistik')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/statistik-akumulasi')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a href="{{url('/administrator/statistik-simpanan')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a href="{{url('/administrator/statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a href="{{url('/administrator/statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a href="{{url('/administrator/statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a href="{{url('/administrator/statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  {{--@if($dashboard =='transaksi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{route('admin-transaksi-pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{route('admin-voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{route('admin-paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data</a></li>
      <li><a href="{{route('admin-paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{route('admin-transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <li><a href="{{route('admin-saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <li><a href="{{url('/administrator/data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Data Transaksi PPOB</a></li>
    </ul>
  </li>--}}
  {{--@if($dashboard =='deposit')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DEPOSIT</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{url('/administrator/my-buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('my-buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
      <!-- <li><a href="{{route('data.topup')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit</a></li> -->
    </ul>--}}
  @if($dashboard =='dataSaya')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA SAYA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/administrator/profil')}}">&nbsp;&nbsp;&nbsp;Profil</a></li>
      <!-- <li><a href="{{url('/administrator/alamat-kirim')}}">&nbsp;&nbsp;&nbsp;Alamat Kirim</a></li> -->
      <!-- <li><a href="{{url('/administrator/simpanan')}}">&nbsp;&nbsp;&nbsp;Simpanan</a></li> -->
      <li><a href="{{url('/administrator/my-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Akumulasi Belanja</a></li>
      <li><a href="{{url('/administrator/kartu')}}">&nbsp;&nbsp;&nbsp;Kartu Saya</a></li>
      <li><a href="{{url('/administrator/ganti-password')}}">&nbsp;&nbsp;&nbsp;Ganti Password</a></li>
    </ul>
  </li>
  {{--@if($dashboard =='pengaturan')<li class="active">@else <li>@endif
    <a href="{{route('administrator.pengaturan')}}">PENGATURAN</a>
  </li>--}}
  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
</div>
    </div>
</div>
