<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
<div id="sidebar-menu" class="sidebar-menu">
<ul>
  @if($dashboard =='dashboard')<li class="active">@else <li>@endif
  <!-- <li> -->
    <a href="{{url('/pengurus/home')}}">DASHBOARD PENGURUS</a>
  </li>
  @if($dashboard =='dataMaster')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA MASTER</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pengurus.data-pendidikan')}}">&nbsp;&nbsp;&nbsp;Data Pendidikan</a></li>
      <li><a href="{{route('pengurus.data-komunitas')}}">&nbsp;&nbsp;&nbsp;Data Komunitas</a></li>
      <li><a href="{{route('pengurus.data-kelurahan')}}">&nbsp;&nbsp;&nbsp;Data Kelurahan</a></li>
      <li><a href="{{route('pengurus.data-kecamatan')}}">&nbsp;&nbsp;&nbsp;Data Kecamatan</a></li>
      <li><a href="{{route('pengurus.data-kabupaten')}}">&nbsp;&nbsp;&nbsp;Data Kabupaten</a></li>
      <li><a href="{{route('pengurus.data-propinsi')}}">&nbsp;&nbsp;&nbsp;Data Propinsi</a></li>
      <li><a href="{{route('pengurus.data-pendapatan')}}">&nbsp;&nbsp;&nbsp;Data Pendapatan</a></li>
      <li><a href="{{route('pengurus.data-pekerjaan')}}">&nbsp;&nbsp;&nbsp;Data Pekerjaan</a></li>
      <li><a href="{{route('pengurus.data-jenis-simpanan')}}">&nbsp;&nbsp;&nbsp;Jenis Simpanan</a></li>
      <li><a href="{{route('pengurus.data-rekening')}}">&nbsp;&nbsp;&nbsp;Data Rekening</a></li>
    </ul>
  </li>
  @if($dashboard =='dataPengguna')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA PENGGUNA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/data-anggota')}}">&nbsp;&nbsp;&nbsp;Data Anggota</a></li>
      <li><a href="{{url('/pengurus/data-group')}}">&nbsp;&nbsp;&nbsp;Data Group</a></li>
      <li><a href="{{url('/pengurus/data-karyawan')}}">&nbsp;&nbsp;&nbsp;Data Karyawan</a></li>
      <!-- <li><a href="{{url('/administrator/simpanan-jatuh-tempo')}}">&nbsp;&nbsp;&nbsp;Data Jatuh Tempo</a></li> -->
    </ul>
  </li>
  @if($dashboard =='dataSimpanan')<li class="active">@else <li>@endif
    <a href="{{url('/pengurus/data-simpanan')}}">SIMPANAN ANGGOTA</a>
  </li>
  @if($dashboard =='Simpananwajib')<li class="active">@else <li>@endif
    <a href="{{url('/pengurus/status-simpanan-wajib')}}">STATUS SIMPANAN WAJIB</a>
  </li>
  @if($dashboard =='akumulasi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>AKUMULASI BELANJA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Laporan Akumulasi</a></li>
      <li><a href="{{url('/pengurus/saldo-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Saldo Akumulasi</a></li>
    </ul>
  </li>
  @if($dashboard =='kartuAnggota')<li class="active">@else <li>@endif
    <a href="{{url('/pengurus/kartu-anggota')}}">KARTU ANGGOTA</a>
  </li>
  @if($dashboard =='tokoOnline')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>PRODUK ONLINE</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pengurus-harga-ppob')}}">&nbsp;&nbsp;&nbsp;Produk PPOB</a></li>
      <li><a href="{{route('pengurus-harga-barang')}}">&nbsp;&nbsp;&nbsp;Produk Barang</a></li>
    </ul>
  </li>
  @if($dashboard =='laporan')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>LAPORAN PENJUALAN</span> &nbsp;<span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('pengurus-laporan-ppob')}}">&nbsp;&nbsp;&nbsp;Laporan PPOB</a></li>
      <li><a href="{{route('pengurus-laporan-barang')}}">&nbsp;&nbsp;&nbsp;Laporan Barang</a></li>
    </ul>
  </li>
  @if($dashboard =='transaksi')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>TRANSAKSI</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">

      <li><a href="{{route('pengurus-belanja')}}">&nbsp;&nbsp;&nbsp;Belanja Barang</a></li>
      <li><a href="{{url('/pengurus/pulsa')}}">&nbsp;&nbsp;&nbsp;Pulsa HP</a></li>
      <li><a href="{{url('/pengurus/paket-data-internet')}}">&nbsp;&nbsp;&nbsp;Paket Data Internet</a></li>
      <li><a href="{{url('/pengurus/voucher-listrik')}}">&nbsp;&nbsp;&nbsp;Voucher Listrik</a></li>
      <li><a href="{{url('/pengurus/saldo-ojek')}}">&nbsp;&nbsp;&nbsp;Saldo Ojek</a></li>
      <li><a href="{{url('/pengurus/paket-sms')}}">&nbsp;&nbsp;&nbsp;Paket SMS/Telp</a></li>
      <li><a href="{{url('/pengurus/transfer-pulsa')}}">&nbsp;&nbsp;&nbsp;Transfer Pulsa</a></li>
      <!-- <li><a href="#">&nbsp;&nbsp;&nbsp;Tagihan Listrik</a></li> -->
      <li><a href="{{url('pengurus/data-transaksi-ppob')}}">&nbsp;&nbsp;&nbsp;Data Transaksi PPOB</a></li>
      <li><a href="{{route('pengurus-data-transaksi-belanja')}}">&nbsp;&nbsp;&nbsp;Data Transaksi Belanja</a></li>
    </ul>
  </li>
@if($dashboard =='deposit')<li class="active submenu">@else <li class="submenu">@endif
  <a href="#"><span>SUPLAYER</span> <span class="menu-arrow"></span></a>
  <ul class="list-unstyled" style="display: none;">
    <li><a href="{{route('laporan-topup-suplayer')}}">&nbsp;&nbsp;&nbsp;Laporan Deposit Suplayer</a></li>
    <li><a href="{{route('pengurus-buku-saldo-suplayer')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Suplayer</a></li>
  </ul>
  @if($dashboard =='bukuSaldo')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>BUKU SALDO ANGGOTA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{route('buku-saldo-transaksi-pengurus')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>




  @if($dashboard =='statistik')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>STATISTIK</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/statistik-akumulasi')}}">&nbsp;&nbsp;&nbsp;Statistik Penjualan</a></li>
      <li><a href="{{url('/pengurus/statistik-jenis-kelamin')}}">&nbsp;&nbsp;&nbsp;Statistik Jenis Kelamin</a></li>
      <li><a href="{{url('/pengurus/statistik-simpanan')}}">&nbsp;&nbsp;&nbsp;Statistik Simpanan</a></li>
      <li><a href="{{url('/pengurus/statistik-kelurahan')}}">&nbsp;&nbsp;&nbsp;Statistik Kelurahan</a></li>
      <li><a href="{{url('/pengurus/statistik-kecamatan')}}">&nbsp;&nbsp;&nbsp;Statistik Kecamatan</a></li>
      <li><a href="{{url('/pengurus/statistik-kabupaten')}}">&nbsp;&nbsp;&nbsp;Statistik Kabupaten</a></li>
    </ul>
  </li>
  @if($dashboard =='mySaldo')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>BUKU SALDO</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{route('topup-deposit-pengurus')}}">&nbsp;&nbsp;&nbsp;Topup Saldo</a></li>
      <li><a href="{{url('/pengurus/my-buku-saldo')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Simpanan</a></li>
      <li><a href="{{url('/pengurus/buku-saldo-transaksi')}}">&nbsp;&nbsp;&nbsp;Buku Saldo Transaksi</a></li>
    </ul>
  @if($dashboard =='dataSaya')<li class="active submenu">@else <li class="submenu">@endif
    <a href="#"><span>DATA SAYA</span> <span class="menu-arrow"></span></a>
    <ul class="list-unstyled" style="display: none;">
      <li><a href="{{url('/pengurus/profil')}}">&nbsp;&nbsp;&nbsp;Profil</a></li>
      <li><a href="{{url('/pengurus/simpanan')}}">&nbsp;&nbsp;&nbsp;Simpanan</a></li>
      <li><a href="{{url('/pengurus/my-akumulasi-belanja')}}">&nbsp;&nbsp;&nbsp;Akumulasi Belanja</a></li>
      <li><a href="{{url('/pengurus/kartu')}}">&nbsp;&nbsp;&nbsp;Kartu Saya</a></li>
      <li><a href="{{url('/pengurus/ganti-password')}}">&nbsp;&nbsp;&nbsp;Ganti Password</a></li>
    </ul>
  </li>
  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">KELUAR</a></li>
</ul>
</div>
    </div>
</div>
