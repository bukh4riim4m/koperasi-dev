@extends('layouts.umum.pages')
@section('content')
<section id="content">
  <div class="container">
    <h4><center><strong>Dewan Syari'ah</strong></center></h4><hr>
    <div class="row">
      <div class="span3">
        <center><img src="laravel/public/organisasi/Ust.-H.-Ardawi-Muhsin-Lc.png" height="80px"><br>
        <strong>Ust. H. Ardawi Muhsin, Lc</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Ust.-H.-Inas-Nasrullah-Lc-MA.png"><br>
        <strong>Ust. H. Inas Nasrullah, Lc, MA</strong><hr></center>
      </div>
    </div>

    <h4><center><strong>Dewan Pengawas</strong></center></h4><hr>
    <div class="row">
      <div class="span3">
        <center><img src="laravel\public\organisasi\H.-Sugeng-Pujoko-SE-MM.png" height="80px"><br>
        <strong>H. Sugeng Pujoko, SE, MM</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Ir.-H.-Muhammad-Balbed-MM.png"><br>
        <strong>Ir. H. Muhammad Balbed, MM</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\H.-Muhammad-Shodiq.png"><br>
        <strong>Ir. H Muhammad Shodiq</strong><hr></center>
      </div>
    </div>

    <h4><center><strong>Dewan Pengurus</strong></center></h4><hr>
    <div class="row">
      <div class="span3">
        <center><img src="laravel\public\organisasi\H.-Aang-Burhanudin-SE-MM.png" height="80px"><br>
        <strong>H. Aang Burhanudin, SE, MM<br>
Ketua</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Dr.-Firmanullah-Firdaus-SE-M.kom.png"><br>
        <strong>DR. H. Firmanullah Firdaus, SE, M.Kom<br>
Ketua I </strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Ir.-H.-Didan-Failatunis-MM.png"><br>
        <strong>Ir. H. Didan Failatunis, MM<br>
Ketua II </strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\H.-Jojo-Jawahir-SE-MM.png"><br>
        <strong>H. Jojo Jawahir, SE, MM<br>
Bendahara Umum</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Deni-Romdani-A.md.png" height="80px"><br>
        <strong>Ny. Deni Romdani, Amd<br>
Bendahara I</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Betty-Subiarti-Perwita-ST.png"><br>
        <strong>Betty Subiarti Perwita, ST<br>
Bendahara II </strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Roby-Adriansya-S.IP.png"><br>
        <strong>Roby Adriansya, S.IP<br>
Sekretaris Umum </strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Fatahilah-S.Pdi.png"><br>
        <strong>Fatahillah, S.Pdi<br>
Sekretaris I</strong><hr></center>
      </div>
    </div>

    <h4><center><strong>Divisi-Divisi</strong></center></h4><hr>
    <div class="row">
      <div class="span3">
        <center><img src="laravel\public\organisasi\Jamal-AN-M.Ak.png" height="80px"><br>
        <strong>Jamal AN, M.Ak<br>Divisi Keuangan</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Harry-Supriyadi.png"><br>
        <strong>Harry Supriyadi<br>Divisi Humas & Infokom</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Bayu-Montana.png"><br>
        <strong>Bayu Montana<br>Divisi Pertanian</strong><hr></center>
      </div>
      <div class="span3">
        <center><img src="laravel\public\organisasi\Ny.-Beby-Yulia.png"><br>
        <strong>Ny. Beby Yulia<br>Divisi Perdagangan</strong><hr></center>
      </div>
    </div>
  </div>
</section>
@endsection
