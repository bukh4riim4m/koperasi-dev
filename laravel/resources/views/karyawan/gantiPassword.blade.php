@extends('layouts.karyawan.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Ganti Password</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#edit_profil"><i class="fa fa-edit"></i> Edit Profil</a> -->
  <div class="view-icons">
    <!-- <a href="{{url('/admin/data-pegawai')}}" class="grid-view btn btn-link"><i class="fa fa-th"></i></a> -->

  </div>
</div>
</div>

<div class="card-box m-b-0">
<div class="row">
  <div class="col-md-12">
    <div class="profile-view">

      <div class="profile-img-wrap">
        <div class="profile-img">
          <a href=""><img class="avatar" src="{{ url('/laravel/public/foto/'.Auth::user()->fotodiri) }}" alt=""></a>
        </div>
      </div>

      <div class="profile-basic">
        <div class="row">
          <div class="col-md-12">
            <div class="">
              <form action="{{url('/karyawan/ganti-password')}}" method="post">
                  @csrf
                <div class="form-group">
                  <label>Username</label>
                  <input class="input-sm form-control" disabled value="{{Auth::user()->sequence}}" type="text" name="propinsi">
                </div>
                <div class="form-group">
                  <label>Password Lama<span class="text-danger">*</span></label>
                  <input class="input-sm form-control" value="" type="text" name="password_lm" minlength="6" required>
                </div>
                <div class="form-group">
                  <label>Password Baru<span class="text-danger">*</span></label>
                  <input class="input-sm form-control" value="" type="text" name="password_br" minlength="6" required>
                </div>
                <div class="m-t-20 text-center">
                  <input class="btn btn-primary" type="submit" value="SIMPAN"/>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
</div>
</div>
</div>

@endsection
