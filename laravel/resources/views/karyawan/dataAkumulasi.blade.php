@extends('layouts.karyawan.app')
@section('content')
<div class="page-wrapper">
    <div class="content container-fluid">

<div class="row">
<div class="col-xs-4">
  <h4 class="page-title">Akumulasi Belanja</h4>
</div>
<div class="col-xs-8 text-right m-b-30">
  <!-- <a href="#" class="btn btn-primary pull-right rounded" data-toggle="modal" data-target="#add_akumulasi"><i class="fa fa-plus"></i> Tambah Akumulasi</a> -->
  <div class="view-icons">
  </div>
</div>
</div>
<div class="row filter-row">
  <form class="form" action="{{url('karyawan/akumulasi-belanja')}}" method="post">
    <input type="hidden" name="action" value="cari">
    @csrf

    <div class="col-sm-3 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">No. Anggota</label>
        <input type="text" class="form-control floating" name="no_kssd" value="{{$no_kssd}}" minlength="6"/>
      </div>
    </div>
    <div class="col-sm-4 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Dari Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="from" value="{{$from}}"></div>
      </div>
    </div>
    <div class="col-sm-4 col-md-2 col-xs-6">
      <div class="form-group form-focus">
        <label class="control-label">Sampai Tgl</label>
        <div class="cal-icon"><input class="form-control floating datetimepicker" type="text" name="to" value="{{$to}}"></div>
      </div>
    </div>
    <div class="col-sm-3 col-md-2 col-xs-6">
      <div class="form-group form-focus select-focus">
        <label class="control-label">Type</label>
        <select class="select floating" name="type">
          <option value=""> Semua Type </option>
          <option value="Anggota"> Anggota</option>
          <option value="Non"> Non Anggota </option>
        </select>
      </div>
    </div>
    <div class="col-sm-3 col-xs-12">
      <input type="submit" class="btn btn-success btn-block" name="btn" value="TAMPILKAN"/>
    </div>
  </form>
  <div class="col-sm-3 col-xs-12"><br>
  <a href="{{url('/karyawan/export-data-simpanan')}}" class="pull-right"><img width="40px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
                document.getElementById('export').submit();"/></a>
              </div>
</div>
<!-- <a href="{{url('/admin/data-pegawai')}}" class="pull-right"><img width="30px" src="{{url('/images/excel.png')}}" onclick="event.preventDefault();
              document.getElementById('export').submit();"/></a> -->
<div class="row">
  <form class="" action="{{url('/karyawan/akumulasi-belanja')}}" method="post" id="export">
    @csrf
    <input type="hidden" name="action" value="export">
    <input type="hidden" name="no_kssd" value="{{$no_kssd}}"/>
    <input type="hidden" name="from" value="{{$from}}"/>
    <input type="hidden" name="to" value="{{$to}}"/>
    <input type="hidden" name="export" value="1"/>
  </form>

<div class="col-md-12">

  <div class="table-responsive">

    <table class="table table-striped custom-table">
      <thead>
        <tr>
          <th>No.</th>
          <th>No. Anggota</th>
          <th>No.Transaksi</th>
          <th>Tgl. Transaksi</th>
          <th>Nominal</th>
          <th>Gerai</th>
          <th>Type</th>
          <!-- <th style="max-width:110px;" class="text-center">Action</th> -->
        </tr>
      </thead>
      <tbody>
        <?php $no=1;
        $total=0;?>
        @foreach($akumulasis as $akumulasi)
        <?php $total+= $akumulasi->nominal;?>
        <tr>
          <td>{{$no++}}.</td>
          <td>{{$akumulasi->no_kssd}}</td>
          <td>{{$akumulasi->no_trx}}</td>
          <td>{{date('d-m-Y', strtotime($akumulasi->tgl_trx))}}</td>
          <td>Rp {{number_format($akumulasi->nominal,0,",",".")}}</td>
          <td>{{$akumulasi->gerai}}</td>
          <td>@if($akumulasi->type =='Non')Non Anggota @else {{$akumulasi->type}} @endif</td>
          <!-- <td style="max-width:110px;" align="center">
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#edit_komunitas{{$akumulasi->id}}">Edit</a>
            <a href="#" class="btn btn-primary btn-sm rounded" data-toggle="modal" data-target="#hapus_komunitas{{$akumulasi->id}}">Hapus</a>
          </td> -->
        </tr>
        @endforeach
        @if(count($akumulasis) < 1)
        <tr>
          <td colspan="8" class="text-center">KOSONG</td>
        </tr>
        @endif
      </tbody>
    </table>
  </div>
</div>

<div class="col-md-5"><hr>
<table class="table custom-table datatable" width="40px">
  <tr>
    <td>Total Akumulasi</td>
    <td>: Rp {{number_format($total,0,",",".")}}</td>
  </tr>
</table>

</div>

</div>
<!-- <label class="col-lg-3 control-label">Light Logo</label> -->
<form class="" action="{{url('karyawan/akumulasi-belanja')}}" method="post" enctype="multipart/form-data" method="post" id="upload-csv">
  @csrf
  <input type="hidden" name="action" value="csv">
  <div class="col-lg-3"><br>
    <span class="control-label">UPLOAD CSV</span>
      <input class="form-control rounded" type="file" name="upload">
  </div>
  <div class="col-lg-9"><br>
  </div>
  <div class="col-lg-2"><br>
      <input class="form-control rounded btn-success" type="submit" name="csv" value="Upload">
  </div>
</form>
    </div>
      </div>
  <div class="sidebar-overlay" data-reff="#sidebar"></div>


@endsection
